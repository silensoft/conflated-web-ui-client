// @flow

import type { Element } from 'react';
import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import type { ContextRouter } from 'react-router';
import { Icon } from 'semantic-ui-react';
import type { PageStateNamespace } from '../../../common/components/page/model/state/namespace/PageStateNamespace';
import { link, linkText, navigation, selected } from './NavigationView.module.scss';

type OwnProps = $Exact<{
  selectPage: PageStateNamespace => void
}>;

type Props = $Exact<{ ...OwnProps, ...ContextRouter }>;

const NavigationView = ({ location: { pathname }, selectPage }: Props): Element<any> => (
  <nav className={navigation}>
    <Link className={link} to="/dashboards" onClick={() => selectPage('Dashboards')}>
      <Icon name="dashboard" color="teal" />
      <span className={`${linkText} ${pathname === '/' || pathname === '/dashboards' ? selected : ''}`}>
        DASHBOARDS
      </span>
    </Link>
    <Link className={link} to="/reports" onClick={() => selectPage('Reports')}>
      <Icon name="file" color="teal" />
      <span className={`${linkText} ${pathname === '/reports' ? selected : ''}`}>REPORTS</span>
    </Link>
    <Link className={link} to="/alerts" onClick={() => selectPage('Alerts')}>
      <Icon name="alarm" color="teal" />
      <span className={`${linkText} ${pathname === '/alerts' ? selected : ''}`}>ALERTS</span>
    </Link>
    <Link className={link} to="/goals" onClick={() => selectPage('Goals')}>
      <Icon name="target" color="teal" />
      <span className={`${linkText} ${pathname === '/goals' ? selected : ''}`}>GOALS</span>
    </Link>
    <Link className={link} to="/data-explorer" onClick={() => selectPage('Data Explorer')}>
      <Icon name="database" color="teal" />
      <span className={`${linkText} ${pathname === '/data-explorer' ? selected : ''}`}>DATA EXPLORER</span>
    </Link>
  </nav>
);

export default withRouter(NavigationView);
