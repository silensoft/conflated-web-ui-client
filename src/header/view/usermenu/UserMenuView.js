// @flow

import type { Element } from 'react';
import React from 'react';
import { Icon } from 'semantic-ui-react';
import { userIcon, userMenu } from './UserMenuView.module.scss';
import { headerIcon } from '../HeaderView.module.scss';

const UserMenuView = (): Element<any> => (
  <section className={userMenu}>
    <span className={`${headerIcon} ${userIcon}`} />
    <span>PKSILEN</span>
    <Icon name="dropdown" />
  </section>
);

export default UserMenuView;
