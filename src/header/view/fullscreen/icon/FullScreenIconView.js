// @flow

import type { Element } from 'react';
import React from 'react';
import { Popup } from 'semantic-ui-react';
import { headerIcon } from '../../HeaderView.module.scss';
import { fullScreen, fullScreenIcon } from './FullScreenIconView.module.scss';

type Props = $Exact<{
  requestFullScreenMode: () => void
}>;

const FullScreenIconView = ({ requestFullScreenMode }: Props): Element<any> => (
  <section className={fullScreen}>
    <Popup
      trigger={<span className={`${headerIcon} ${fullScreenIcon}`} onClick={requestFullScreenMode} />}
      content="Switch to full screen mode"
      inverted
    />
  </section>
);

export default FullScreenIconView;
