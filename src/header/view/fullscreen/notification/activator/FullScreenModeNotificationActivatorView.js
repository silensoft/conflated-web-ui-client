// @flow

import type { Element } from 'react';
import React from 'react';
import classNames from 'classnames';
import { fullScreenModeNotificationActivator, hidden } from './FullScreenModeNotificationActivatorView.module.scss';

type Props = $Exact<{
  isFullScreenModeActive: boolean,
  showFullScreenModeNotification: () => void
}>;

const FullScreenModeNotificationActivatorView = ({
  isFullScreenModeActive,
  showFullScreenModeNotification
}: Props): Element<any> => {
  const className = classNames(fullScreenModeNotificationActivator, { [hidden]: !isFullScreenModeActive });

  return (
    <div
      className={className}
      onMouseOver={showFullScreenModeNotification}
      onFocus={showFullScreenModeNotification}
    />
  );
};

export default FullScreenModeNotificationActivatorView;
