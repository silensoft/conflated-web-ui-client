// @flow

import type { Element } from 'react';
import React, { useCallback, useEffect } from 'react';
import { connect } from 'react-redux';
import type { Dispatch } from 'oo-redux-utils';
import classNames from 'classnames';
import { appHeader, appTitle, hidden } from './HeaderView.module.scss';
import type { AppState } from '../../store/AppState';
import HeaderControllerFactory from '../controller/HeaderControllerFactory';
import NavigationView from './navigation/NavigationView';
import UserMenuView from './usermenu/UserMenuView';
import FullScreenIconView from './fullscreen/icon/FullScreenIconView';
import FullScreenModeNotificationView from './fullscreen/notification/FullScreenModeNotificationView';
import FullScreenModeNotificationActivatorView from './fullscreen/notification/activator/FullScreenModeNotificationActivatorView';

const mapAppStateToComponentProps = (appState: AppState) => appState.headerState;
const createController = (dispatch: Dispatch) => new HeaderControllerFactory(dispatch).createController();

type MappedState = $Call<typeof mapAppStateToComponentProps, AppState>;
type Controller = $Call<typeof createController, Dispatch>;
type Props = $Exact<{ ...MappedState, ...Controller }>;

// noinspection OverlyComplexFunctionJS
const HeaderView = ({
  dismissFullScreenModeNotification,
  exitFullScreenMode,
  isFullScreenModeActive,
  selectPage,
  shouldShowFullScreenModeNotification,
  showDashboardsHeader,
  showFullScreenModeNotification,
  switchToFullScreenMode
}: Props): Element<any> => {
  const handleFullScreenModeExit = useCallback(() => {
    document.exitFullscreen();
    exitFullScreenMode();
  }, []);

  const onKeyDown = useCallback((keyboardEvent: KeyboardEvent) => {
    if (keyboardEvent.code === 'Escape') {
      handleFullScreenModeExit();
    }
  }, []);

  useEffect((): (() => void) => {
    document.addEventListener('keydown', onKeyDown);
    return () => document.removeEventListener('keydown', onKeyDown);
  }, []);

  const onFullScreenChange = useCallback(() => {
    if (!document.fullscreenElement) {
      exitFullScreenMode();
    }
  }, []);

  useEffect((): (() => void) => {
    document.addEventListener('fullscreenchange', onFullScreenChange);
    return () => document.removeEventListener('fullscreenchange', onFullScreenChange);
  }, []);

  const requestFullScreenMode = useCallback(() => {
    const { documentElement } = document;

    if (documentElement && documentElement.requestFullscreen) {
      documentElement.requestFullscreen();
    }

    switchToFullScreenMode();
  });

  const className = classNames(appHeader, { [hidden]: isFullScreenModeActive });

  return (
    <header className={className} onMouseOver={showDashboardsHeader} onFocus={showDashboardsHeader}>
      <h1 className={appTitle}>Conflated</h1>
      <NavigationView selectPage={selectPage} />
      <UserMenuView />
      <FullScreenIconView requestFullScreenMode={requestFullScreenMode} />
      <FullScreenModeNotificationView
        exitFullScreenMode={handleFullScreenModeExit}
        dismissFullScreenModeNotification={dismissFullScreenModeNotification}
        shouldShowFullScreenModeNotification={shouldShowFullScreenModeNotification}
      />
      <FullScreenModeNotificationActivatorView
        isFullScreenModeActive={isFullScreenModeActive}
        showFullScreenModeNotification={showFullScreenModeNotification}
      />
    </header>
  );
};

export default connect<Props, $Exact<{}>, _, _, _, _>(
  mapAppStateToComponentProps,
  createController
)(HeaderView);
