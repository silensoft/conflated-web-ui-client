import React from 'react';
import { shallow as renderShallow } from 'enzyme';
import Header from './HeaderView';

test('Header should render correctly', () => {
  const renderedHeader = renderShallow(<Header />);
  expect(renderedHeader).toMatchSnapshot();
});
