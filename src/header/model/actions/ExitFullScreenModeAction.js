// @flow

import AbstractHeaderAction from './AbstractHeaderAction';
import type { HeaderState } from '../state/HeaderState';

export default class ExitFullScreenModeAction extends AbstractHeaderAction {
  performActionAndReturnNewState(currentState: HeaderState): HeaderState {
    return {
      ...currentState,
      isFullScreenModeActive: false,
      isFullScreenModeNotificationDismissed: false,
      shouldShowFullScreenModeNotification: false
    };
  }
}
