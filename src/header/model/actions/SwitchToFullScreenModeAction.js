// @flow

import AbstractHeaderAction from './AbstractHeaderAction';
import type { HeaderState } from '../state/HeaderState';

export default class SwitchToFullScreenModeAction extends AbstractHeaderAction {
  performActionAndReturnNewState(currentState: HeaderState): HeaderState {
    return {
      ...currentState,
      isFullScreenModeActive: true
    };
  }
}
