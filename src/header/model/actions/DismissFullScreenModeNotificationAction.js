// @flow

import AbstractHeaderAction from './AbstractHeaderAction';
import type { HeaderState } from '../state/HeaderState';

export default class DismissFullScreenModeNotificationAction extends AbstractHeaderAction {
  performActionAndReturnNewState(currentState: HeaderState): HeaderState {
    return {
      ...currentState,
      isFullScreenModeNotificationDismissed: true,
      shouldShowFullScreenModeNotification: false
    };
  }
}
