// @flow

import type { DragType } from './types/DragType';
import type { PageStateNamespace } from '../../../common/components/page/model/state/namespace/PageStateNamespace';

export type HeaderState = $Exact<{
  +currentPage: PageStateNamespace,
  +isFullScreenModeActive: boolean,
  +isFullScreenModeNotificationDismissed: boolean,
  +shouldShowFullScreenModeNotification: boolean,
  +lastDragType?: DragType
}>;
