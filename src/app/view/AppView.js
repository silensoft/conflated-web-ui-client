// @flow

import type { Element } from 'react';
import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from '../../store/store';
import DashboardsPageView from '../../dashboardspage/view/DashboardsPageView';
import TriggersPageView from '../../common/components/triggerspage/view/TriggersPageView';
import DataExplorerPageView from '../../dataexplorerpage/view/DataExplorerPageView';
import HeaderView from '../../header/view/HeaderView';

const AppView = (): Element<any> => (
  <Provider store={store}>
    <Router>
      <div>
        <HeaderView />
        <Route exact path="/" component={DashboardsPageView} />
        <Route path="/dashboards" component={DashboardsPageView} />
        <Route path="/data-explorer" component={DataExplorerPageView} />
        <Route path="/alerts" render={() => <TriggersPageView pageStateNamespace="alertsPage" />} />
        <Route path="/goals" render={() => <TriggersPageView pageStateNamespace="goalsPage" />} />
      </div>
    </Router>
  </Provider>
);

export default AppView;
