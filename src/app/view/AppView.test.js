import React from 'react';
import { shallow as renderShallow } from 'enzyme';
import AppView from './AppView';

test('App should render correctly', () => {
  const renderedApp = renderShallow(<AppView />);
  expect(renderedApp).toMatchSnapshot();
});
