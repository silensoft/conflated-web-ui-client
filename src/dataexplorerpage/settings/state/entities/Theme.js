// @flow

export type Theme = {
  name: string,
  colors: string[]
};
