// @flow

import type { Theme } from './entities/Theme';

export type DataExplorerSettingsState = {
  +theme: Theme
};
