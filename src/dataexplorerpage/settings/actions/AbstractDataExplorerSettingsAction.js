// @flow

import { AbstractAction } from 'oo-redux-utils';
import type { DataExplorerSettingsState } from '../state/DataExplorerSettingsState';

export default class AbstractDataExplorerSettingsAction extends AbstractAction<DataExplorerSettingsState> {
}
