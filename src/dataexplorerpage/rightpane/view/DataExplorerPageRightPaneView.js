// @flow

import _ from 'lodash';
import type { Element } from 'react';
import React, { useCallback, useEffect } from 'react';
import { connect } from 'react-redux';
import type { Dispatch } from 'oo-redux-utils';
import FilterSelectorView from '../../../common/components/filterselector/view/FilterSelectorView';
import SortBySelectorView from '../../../common/components/sortbyselector/view/SortBySelectorView';
import DataPointsCountSelectorView from '../../../common/components/datapointscountselector/view/DataPointsCountSelectorView';
import type { AppState } from '../../../store/AppState';
import DataExplorerPageRightPaneControllerFactory from '../controller/DataExplorerPageRightPaneControllerFactory';
import DataExplorerPageRightPaneViewUtils from './DataExplorerPageRightPaneViewUtils';
import PagePaneView from '../../../common/view/pagepane/PagePaneView';
import DataExplorerPageActionIconsView from '../actionicons/view/DataExplorerPageActionIconsView';

const mapAppStateToComponentProps = (appState: AppState) => ({
  isFullScreenModeActive: appState.headerState.isFullScreenModeActive,

  shouldShowDataExplorerPageRightPane:
    appState.common.pageStates.dataExplorerPage.shouldShowPagePane.rightPane,

  shouldShowDataExplorerPageRightPanePermanently:
    appState.common.pageStates.dataExplorerPage.shouldShowPagePanePermanently.rightPane,

  dataExplorerPageRightPaneGutterOffset:
    appState.common.pageStates.dataExplorerPage.pagePaneGutterOffset.rightPane,

  isFilterSelectorOpen: appState.common.selectorStates.dataExplorerPageFilterSelector.isSelectorOpen,
  isSortBySelectorOpen: appState.common.selectorStates.dataExplorerPageSortBySelector.isSelectorOpen,
  isDataPointsCountSelectorOpen:
    appState.common.selectorStates.dataExplorerPageDataPointsCountSelector.isSelectorOpen
});

const createController = (dispatch: Dispatch) =>
  new DataExplorerPageRightPaneControllerFactory(dispatch).createController();

type MappedState = $Call<typeof mapAppStateToComponentProps, AppState>;
type Controller = $Call<typeof createController, Dispatch>;
type Props = $Exact<{ ...MappedState, ...Controller }>;

const DataExplorerPageRightPaneView = ({
  hideDataExplorerPageRightPane,
  isDataPointsCountSelectorOpen,
  isFilterSelectorOpen,
  isFullScreenModeActive,
  isSortBySelectorOpen,
  dataExplorerPageRightPaneGutterOffset,
  shouldShowDataExplorerPageRightPane,
  shouldShowDataExplorerPageRightPanePermanently
}: Props): Element<any> => {
  const updateSelectorContentHeights = useCallback(
    () =>
      _.before(2, () =>
        DataExplorerPageRightPaneViewUtils.updateSelectorContentHeights({
          isDataPointsCountSelectorOpen,
          isFilterSelectorOpen,
          isSortBySelectorOpen
        })
      )(),
    [isDataPointsCountSelectorOpen, isFilterSelectorOpen, isSortBySelectorOpen]
  );

  useEffect(() => updateSelectorContentHeights());
  useEffect(
    () => {
      setTimeout(() => updateSelectorContentHeights(), 1000);
    },
    [isFullScreenModeActive]
  );

  return (
    <PagePaneView
      id="dataExplorerPageRightPane"
      isFullScreenModeActive={isFullScreenModeActive}
      hidePagePane={hideDataExplorerPageRightPane}
      pane="rightPane"
      paneDefaultWidthCssVarName="data-explorer-page-right-pane-default-width"
      paneGutterOffset={dataExplorerPageRightPaneGutterOffset}
      shouldShowPagePane={shouldShowDataExplorerPageRightPane}
      shouldShowPagePanePermanently={shouldShowDataExplorerPageRightPanePermanently}
    >
      <DataExplorerPageActionIconsView />
      <FilterSelectorView pageStateNamespace="dataExplorerPage" />
      <SortBySelectorView pageStateNamespace="dataExplorerPage" />
      <DataPointsCountSelectorView pageStateNamespace="dataExplorerPage" />
    </PagePaneView>
  );
};

export default connect<Props, $Exact<{}>, _, _, _, _>(
  mapAppStateToComponentProps,
  createController
)(DataExplorerPageRightPaneView);
