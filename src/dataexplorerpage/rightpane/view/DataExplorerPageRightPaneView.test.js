import React from 'react';
import { shallow as renderShallow } from 'enzyme';
import DataExplorerRightPane from './DataExplorerPageRightPaneView';

test('DataExplorerRightPane should render correctly', () => {
  const renderedDataExplorerRightPane = renderShallow(<DataExplorerRightPane />);
  expect(renderedDataExplorerRightPane).toMatchSnapshot();
});
