// @flow

import React from 'react';
import type { Element } from 'react';
import { connect } from 'react-redux';
import type { Dispatch } from 'oo-redux-utils';
import { Icon, Popup } from 'semantic-ui-react';
import { actionIcons, actionIcon } from './DataExplorerPageActionIconsView.module.scss';
import DataExplorerActionIconsControllerFactory from '../controller/DataExplorerActionIconsControllerFactory';

const mapAppStateToComponentProps = () => ({});
const createController = (dispatch: Dispatch) =>
  new DataExplorerActionIconsControllerFactory(dispatch).createController();

type Controller = $Call<typeof createController, Dispatch>;
type Props = Controller;

const DataExplorerPageActionIconsView = ({ openSaveAsDashboardOrReportTemplateDialog }: Props): Element<any> => (
  <section id="dataExplorerPageActionIcons" className={actionIcons}>
    <Popup
      trigger={<Icon className={actionIcon} name="refresh" size="large" />}
      content="Refresh chart data"
      inverted
    />
    <Popup
      trigger={
        <Icon
          className={actionIcon}
          name="save"
          size="large"
          onClick={openSaveAsDashboardOrReportTemplateDialog}
        />
      }
      content="Save charts as new dashboard"
      inverted
    />
    <Popup
      trigger={<Icon className={actionIcon} name="trash alternate" size="large" />}
      content="Clear charts"
      inverted
    />
    <Popup
      trigger={<Icon className={actionIcon} name="external share" size="large" />}
      content="Export"
      inverted
    />
    <Popup
      trigger={<Icon className={actionIcon} name="setting" size="large" />}
      content="Settings"
      inverted
    />
  </section>
);

export default connect<Props, $Exact<{}>, _, _, _, _>(
  mapAppStateToComponentProps,
  createController
)(DataExplorerPageActionIconsView);
