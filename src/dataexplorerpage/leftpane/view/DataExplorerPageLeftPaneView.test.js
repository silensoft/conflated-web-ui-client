import React from 'react';
import { shallow as renderShallow } from 'enzyme';
import DataExplorerLeftPane from './DataExplorerPageLeftPaneView';

test('DataExplorerLeftPane should render correctly', () => {
  const renderedDataExplorerLeftPane = renderShallow(<DataExplorerLeftPane />);
  expect(renderedDataExplorerLeftPane).toMatchSnapshot();
});
