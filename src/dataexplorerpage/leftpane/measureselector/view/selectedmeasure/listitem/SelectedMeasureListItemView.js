// @flow

import type { Element } from 'react';
import React from 'react';
import { Icon, List } from 'semantic-ui-react';
import { measureName, removeIcon, selectedListItem } from './SelectedMeasureListItemView.module.scss';
import type { SelectedMeasure } from '../../../../../../common/components/chartarea/chart/model/state/selectedmeasure/SelectedMeasure';
import type { Theme } from '../../../../../settings/state/entities/Theme';
import type { AggregationFunction } from '../../../../../../common/components/chartarea/chart/model/state/selectedmeasure/types/AggregationFunction';
import type { MeasureVisualizationType } from '../../../../../../common/components/chartarea/chart/model/state/selectedmeasure/types/MeasureVisualizationType';
import type { Chart } from '../../../../../../common/components/chartarea/chart/model/state/Chart';
import VisualizationColorPickerView from '../../../../../../common/view/visualizationcolorpicker/VisualizationColorPickerView';
import AggregationFunctionPickerView from '../../../../../../common/view/aggregationfunctionpicker/AggregationFunctionPickerView';
import SelectedMeasureVisualizationTypeDropdownView from '../visualizationtype/dropdown/SelectedMeasureVisualizationTypeDropdownView';

type Props = $Exact<{
  changeAggregationFunction: AggregationFunction => void,
  changeVisualizationColor: string => void,
  changeVisualizationType: MeasureVisualizationType => void,
  chart: Chart,
  removeSelectedMeasure: SelectedMeasure => void,
  selectedMeasure: SelectedMeasure,
  theme: Theme
}>;

const SelectedMeasureListItemView = ({
  changeAggregationFunction,
  changeVisualizationColor,
  changeVisualizationType,
  chart,
  removeSelectedMeasure,
  selectedMeasure,
  theme
}: Props): Element<any> => {
  let visualizationColorPicker;
  if (chart.supportsSelectedMeasureVisualizationColor()) {
    visualizationColorPicker = (
      <VisualizationColorPickerView
        changeVisualizationColor={changeVisualizationColor}
        currentColor={selectedMeasure.visualizationColor}
        theme={theme}
      />
    );
  }

  return (
    <List.Item className={selectedListItem} key={selectedMeasure.measure.name}>
      {visualizationColorPicker}
      <SelectedMeasureVisualizationTypeDropdownView
        changeVisualizationType={changeVisualizationType}
        chart={chart}
        selectedMeasure={selectedMeasure}
      />
      <AggregationFunctionPickerView
        aggregationFunctions={chart.getSupportedAggregationFunctions()}
        changeAggregationFunction={changeAggregationFunction}
        selectedAggregationFunction={selectedMeasure.aggregationFunction}
      />
      <div className={measureName}>{selectedMeasure.measure.name}</div>
      <Icon className={removeIcon} name="close" onClick={() => removeSelectedMeasure(selectedMeasure)} />
    </List.Item>
  );
};

export default SelectedMeasureListItemView;
