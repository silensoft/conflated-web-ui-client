// @flow

import React, { useMemo } from 'react';
import type { Element } from 'react';
import { Dropdown, Popup } from 'semantic-ui-react';
import type { SelectedMeasure } from '../../../../../../../common/components/chartarea/chart/model/state/selectedmeasure/SelectedMeasure';
import type { Chart } from '../../../../../../../common/components/chartarea/chart/model/state/Chart';
import type { MeasureVisualizationType } from '../../../../../../../common/components/chartarea/chart/model/state/selectedmeasure/types/MeasureVisualizationType';
import SelectedMeasureVisualizationTypeIconFactory from '../iconfactory/SelectedMeasureVisualizationTypeIconFactory';
import { candlestickVisualizationTypeSelector } from '../../listitem/SelectedMeasureListItemView.module.scss';

type Props = $Exact<{
  changeVisualizationType: MeasureVisualizationType => void,
  chart: Chart,
  selectedMeasure: SelectedMeasure
}>;

const SelectedMeasureVisualizationTypeDropdownView = ({
  changeVisualizationType,
  chart,
  selectedMeasure
}: Props): Element<any> | null => {
  const visualizationTypeDropdownMenuItems = useMemo(
    () =>
      chart
        .getSupportedMeasureVisualizationTypes(selectedMeasure)
        .map((visualizationType: MeasureVisualizationType) => (
          <Dropdown.Item
            key={visualizationType}
            text={visualizationType}
            value={visualizationType}
            onClick={() => changeVisualizationType(visualizationType)}
          />
        )),
    [chart, selectedMeasure]
  );

  const visualizationTypeIcon = SelectedMeasureVisualizationTypeIconFactory.createMeasureVisualizationTypeIcon(
    selectedMeasure.visualizationType,
    chart.selectedDimensions.length === 0
  );

  if (chart.chartType === 'candlestick') {
    return (
      <Dropdown
        className={candlestickVisualizationTypeSelector}
        text={selectedMeasure.visualizationType.toUpperCase().charAt(0)}
      >
        <Dropdown.Menu>{visualizationTypeDropdownMenuItems}</Dropdown.Menu>
      </Dropdown>
    );
  } else if (visualizationTypeDropdownMenuItems.length > 0) {
    return (
      <Popup
        trigger={
          <Dropdown icon={visualizationTypeIcon}>
            <Dropdown.Menu>{visualizationTypeDropdownMenuItems}</Dropdown.Menu>
          </Dropdown>
        }
        content="Change visualization type"
        inverted
      />
    );
  }

  return null;
};

export default SelectedMeasureVisualizationTypeDropdownView;
