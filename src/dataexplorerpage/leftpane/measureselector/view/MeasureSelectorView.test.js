import React from 'react';
import { shallow as renderShallow } from 'enzyme/build';
import MeasureSelector from './MeasureSelectorView';

test('MeasureSelector should render correctly', () => {
  const renderedMeasureSelector = renderShallow(<MeasureSelector />);
  expect(renderedMeasureSelector).toMatchSnapshot();
});
