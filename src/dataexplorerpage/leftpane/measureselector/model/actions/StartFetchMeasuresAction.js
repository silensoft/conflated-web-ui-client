// @flow

import { Inject } from 'noicejs';
import type { DispatchAction } from 'oo-redux-utils';
import AbstractMeasureSelectorAction from './AbstractMeasureSelectorAction';
import MeasureService from '../service/MeasureService';
import type { MeasureSelectorState } from '../state/MeasureSelectorState';
import type { DataSource } from '../../../../../common/model/state/datasource/DataSource';
import type { Measure } from '../state/entities/Measure';
import AbstractMeasureSelectorDispatchingAction from './AbstractMeasureSelectorDispatchingAction';

type ConstructorArgs = {
  measureService: MeasureService,
  dispatchAction: DispatchAction,
  dataSource: DataSource
};

export default
@Inject('measureService')
class StartFetchMeasuresAction extends AbstractMeasureSelectorDispatchingAction {
  +measureService: MeasureService;

  +dataSource: DataSource;

  constructor({ measureService, dispatchAction, dataSource }: ConstructorArgs) {
    super(dispatchAction);
    this.measureService = measureService;
    this.dataSource = dataSource;
  }

  performActionAndReturnNewState(currentState: MeasureSelectorState): MeasureSelectorState {
    this.measureService
      .fetchMeasures(this.dataSource)
      .then((measures: Measure[]) => this.dispatchAction(new FinishFetchMeasuresAction(measures)));

    return {
      ...currentState,
      isFetchingMeasures: true
    };
  }
}

class FinishFetchMeasuresAction extends AbstractMeasureSelectorAction {
  +measures: Measure[];

  constructor(measures: Measure[]) {
    super();
    this.measures = measures;
  }

  performActionAndReturnNewState(currentState: MeasureSelectorState): MeasureSelectorState {
    return {
      ...currentState,
      measures: this.measures,
      isFetchingMeasures: false
    };
  }
}
