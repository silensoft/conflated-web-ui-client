// @flow

import type { Measure } from './entities/Measure';

export type MeasureSelectorState = $Exact<{
  +measures: Measure[],
  +isFetchingMeasures: boolean
}>;
