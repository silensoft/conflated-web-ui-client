// @flow

import type { DataSource } from '../../../../../common/model/state/datasource/DataSource';
import type { Measure } from '../state/entities/Measure';

export default class MeasureService {
  // noinspection JSMethodCanBeStatic
  // eslint-disable-next-line no-unused-vars
  fetchMeasures(dataSource: DataSource): Promise<Measure[]> {
    throw new TypeError('Abstract method error');
  }
}
