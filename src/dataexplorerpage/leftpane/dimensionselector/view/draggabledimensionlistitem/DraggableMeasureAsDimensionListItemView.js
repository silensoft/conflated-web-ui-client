/* eslint-disable react/destructuring-assignment */
// @flow

import type { Element } from 'react';
import React from 'react';
import type { Measure } from '../../../measureselector/model/state/entities/Measure';
import type { ListItemViewProps } from '../../../../../common/view/listitems/listitem/ListItemView';
import ListItemView from '../../../../../common/view/listitems/listitem/ListItemView';

const DraggableMeasureAsDimensionListItemView = (props: ListItemViewProps<Measure>): Element<any> => (
  <ListItemView
    draggable
    dragEventDataKey="measureName"
    iconName="dashboard"
    {...props}
  />
);

export default DraggableMeasureAsDimensionListItemView;
