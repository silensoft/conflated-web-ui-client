/* eslint-disable react/destructuring-assignment */
// @flow

import type { Element } from 'react';
import React from 'react';
import type { Dimension } from '../../model/state/entities/Dimension';
import type { ListItemViewProps } from '../../../../../common/view/listitems/listitem/ListItemView';
import ListItemView from '../../../../../common/view/listitems/listitem/ListItemView';

const DraggableDimensionListItemView = (props: ListItemViewProps<Dimension>): Element<any> => (
  <ListItemView
    draggable
    dragEventDataKey="dimensionName"
    iconName={`${props.item.isTimestamp ? 'calendar outline alternate' : 'cube'}`}
    {...props}
  />
);

export default DraggableDimensionListItemView;
