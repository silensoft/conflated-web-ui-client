// @flow

import type { Element } from 'react';
import React from 'react';
import { Icon, List } from 'semantic-ui-react';
import {
  dimensionName,
  listItem,
  removeIcon,
  visualizationType
} from './SelectedDimensionListItem.module.scss';
import type { Theme } from '../../../../settings/state/entities/Theme';
import type { SelectedDimension } from '../../../../../common/components/chartarea/chart/model/state/selecteddimension/SelectedDimension';
import VisualizationColorPickerView from '../../../../../common/view/visualizationcolorpicker/VisualizationColorPickerView';

type Props = $Exact<{
  changeVisualizationColor: string => void,
  removeSelectedDimension: SelectedDimension => void,
  selectedDimension: SelectedDimension,
  shouldShowVisualizationColorPicker: boolean,
  theme: Theme
}>;

const SelectedDimensionListItem = ({
  changeVisualizationColor,
  removeSelectedDimension,
  selectedDimension,
  shouldShowVisualizationColorPicker,
  theme
}: Props): Element<any> => {
  let visualizationColorPicker;
  if (shouldShowVisualizationColorPicker) {
    visualizationColorPicker = (
      <VisualizationColorPickerView
        changeVisualizationColor={changeVisualizationColor}
        currentColor={selectedDimension.visualizationColor}
        theme={theme}
      />
    );
  }

  return (
    <List.Item className={listItem}>
      {visualizationColorPicker}
      <div className={dimensionName}>{selectedDimension.dimension.name}</div>
      <div className={visualizationType}>{selectedDimension.visualizationType}</div>
      <Icon className={removeIcon} name="close" onClick={() => removeSelectedDimension(selectedDimension)} />
    </List.Item>
  );
};

export default SelectedDimensionListItem;
