import React from 'react';
import { shallow as renderShallow } from 'enzyme/build';
import DimensionSelector from './DimensionSelectorView';

test('DimensionSelector should render correctly', () => {
  const renderedDimensionSelector = renderShallow(<DimensionSelector />);
  expect(renderedDimensionSelector).toMatchSnapshot();
});
