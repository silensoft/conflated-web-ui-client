// @flow

import type { Element } from 'react';
import React from 'react';
import { List } from 'semantic-ui-react';
import type { DimensionVisualizationType } from '../../../../../common/components/chartarea/chart/model/state/selecteddimension/types/DimensionVisualizationType';

export default class DimensionDropZoneListItemViewFactory {
  className: string;

  enterDropZone: (event: SyntheticDragEvent<HTMLDivElement>) => void;

  leaveDropZone: (event: SyntheticDragEvent<HTMLDivElement>) => void;

  dropDimension: (
    event: SyntheticDragEvent<HTMLDivElement>,
    visualizationType: DimensionVisualizationType
  ) => void;

  constructor(
    className: string,
    enterDropZone: (event: SyntheticDragEvent<HTMLDivElement>) => void,
    leaveDropZone: (event: SyntheticDragEvent<HTMLDivElement>) => void,
    dropDimension: (
      event: SyntheticDragEvent<HTMLDivElement>,
      visualizationType: DimensionVisualizationType
    ) => void
  ) {
    this.className = className;
    this.enterDropZone = enterDropZone;
    this.leaveDropZone = leaveDropZone;
    this.dropDimension = dropDimension;
  }

  createDimensionDropZoneListItem(
    key: string,
    visualizationType: DimensionVisualizationType,
    visualizationTypeUiText?: string
  ): Element<any> {
    const dropZoneTitle = `Drag ${visualizationTypeUiText || visualizationType} here`;

    return (
      <div
        key={key}
        className={this.className}
        onDragOver={this.enterDropZone}
        onDragLeave={this.leaveDropZone}
        onDrop={(event: SyntheticDragEvent<HTMLDivElement>) => this.dropDimension(event, visualizationType)}
      >
        <List.Item>{dropZoneTitle}</List.Item>
      </div>
    );
  }
}
