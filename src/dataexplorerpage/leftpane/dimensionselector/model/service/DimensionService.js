// @flow

import type { DataSource } from '../../../../../common/model/state/datasource/DataSource';
import type { Dimension } from '../state/entities/Dimension';

export default class DimensionService {
  // noinspection JSMethodCanBeStatic
  // eslint-disable-next-line no-unused-vars
  fetchDimensions(dataSource: DataSource): Promise<Dimension[]> {
    throw new TypeError('Abstract method error');
  }
}
