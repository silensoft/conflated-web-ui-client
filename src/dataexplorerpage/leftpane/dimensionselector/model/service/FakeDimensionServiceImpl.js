// @flow

import DimensionService from './DimensionService';
import type { Dimension } from '../state/entities/Dimension';
import type { DataSource } from '../../../../../common/model/state/datasource/DataSource';

export default class DimensionServiceImpl extends DimensionService {
  latency = 1000;

  // eslint-disable-next-line no-unused-vars
  fetchDimensions(dataSource: DataSource): Promise<Dimension[]> {
    return new Promise<Dimension[]>((resolve: (Dimension[]) => void) => {
      setTimeout(() => {
        resolve([
          {
            name: 'dimension1',
            expression: '',
            isTimestamp: false,
            isDate: false,
            isString: true,
            unit: 'none'
          },
          {
            name: 'Product category',
            expression: '',
            isTimestamp: false,
            isDate: false,
            isString: true,
            unit: 'none'
          },
          {
            name: 'timestamp',
            expression: '',
            isTimestamp: true,
            isDate: false,
            isString: false,
            unit: 'none'
          },
          {
            name: 'latitude',
            expression: '',
            isTimestamp: false,
            isDate: false,
            isString: false,
            unit: 'none'
          },
          {
            name: 'longitude',
            expression: '',
            isTimestamp: false,
            isDate: false,
            isString: false,
            unit: 'none'
          },
          {
            name: 'eNodeB name',
            expression: '',
            isTimestamp: false,
            isDate: false,
            isString: true,
            unit: 'none'
          },
          {
            name: 'date',
            expression: '',
            isTimestamp: false,
            isDate: true,
            isString: false,
            unit: 'none'
          },
          {
            name: 'dimension8',
            expression: '',
            isTimestamp: false,
            isDate: false,
            isString: false,
            unit: 'none'
          },
          {
            name: 'dimension9',
            expression: '',
            isTimestamp: false,
            isDate: false,
            isString: false,
            unit: 'none'
          }
        ]);
      }, this.latency);
    });
  }
}
