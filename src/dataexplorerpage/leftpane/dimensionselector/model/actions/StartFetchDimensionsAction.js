// @flow

import { Inject } from 'noicejs';
import type { DispatchAction } from 'oo-redux-utils';
import AbstractDimensionSelectorAction from './AbstractDimensionSelectorAction';
import DimensionService from '../service/DimensionService';
import type { DimensionSelectorState } from '../state/DimensionSelectorState';
import type { DataSource } from '../../../../../common/model/state/datasource/DataSource';
import type { Dimension } from '../state/entities/Dimension';
import AbstractDimensionSelectorDispatchingAction from './AbstractDimensionSelectorDispatchingAction';

type ConstructorArgs = {
  dimensionService: DimensionService,
  dispatchAction: DispatchAction,
  dataSource: DataSource
};

export default
@Inject('dimensionService')
class StartFetchDimensionsAction extends AbstractDimensionSelectorDispatchingAction {
  +dimensionService: DimensionService;

  +dataSource: DataSource;

  constructor({ dimensionService, dispatchAction, dataSource }: ConstructorArgs) {
    super(dispatchAction);
    this.dimensionService = dimensionService;
    this.dataSource = dataSource;
  }

  performActionAndReturnNewState(currentState: DimensionSelectorState): DimensionSelectorState {
    this.dimensionService
      .fetchDimensions(this.dataSource)
      .then((dimensions: Dimension[]) => this.dispatchAction(new FinishFetchDimensionsAction(dimensions)));

    return {
      ...currentState,
      isFetchingDimensions: true
    };
  }
}

class FinishFetchDimensionsAction extends AbstractDimensionSelectorAction {
  +dimensions: Dimension[];

  constructor(dimensions: Dimension[]) {
    super();
    this.dimensions = dimensions;
  }

  performActionAndReturnNewState(currentState: DimensionSelectorState): DimensionSelectorState {
    return {
      ...currentState,
      dimensions: this.dimensions,
      isFetchingDimensions: false
    };
  }
}
