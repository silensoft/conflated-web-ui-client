// @flow

import type { Dimension } from './entities/Dimension';

export type DimensionSelectorState = $Exact<{
  +dimensions: Dimension[],
  +isFetchingDimensions: boolean
}>;
