// @flow

import { ControllerFactory } from 'oo-redux-utils';
import HidePagePaneAction from '../../../common/components/page/model/actions/panevisibility/HidePagePaneAction';

export default class DataExplorerPageLeftPaneControllerFactory extends ControllerFactory {
  createController = () => ({
    hideDataExplorerPageLeftPane: () =>
      this.dispatchAction(new HidePagePaneAction('dataExplorerPage', 'leftPane'))
  });
}
