import React from 'react';
import { shallow as renderShallow } from 'enzyme';
import DataSourceSelector from './DataSourceSelectorView';

test('DataSourceSelector should render correctly', () => {
  const renderedDataSourceSelector = renderShallow(<DataSourceSelector />);
  expect(renderedDataSourceSelector).toMatchSnapshot();
});
