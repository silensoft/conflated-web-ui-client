// @flow

import type { DataSource } from '../../../../../common/model/state/datasource/DataSource';

export default class DataSourceService {
  // noinspection JSMethodCanBeStatic
  fetchDataSources(): Promise<DataSource[]> {
    throw new TypeError('Abstract method error');
  }
}
