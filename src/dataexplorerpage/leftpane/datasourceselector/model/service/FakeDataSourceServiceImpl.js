// @flow

import DataSourceService from './DataSourceService';
import type { DataSource } from '../../../../../common/model/state/datasource/DataSource';

export default class FakeDataSourceServiceImpl extends DataSourceService {
  latency = 1000;

  // TODO: encrypt user, password and sqlStatement in config service and decrypt in data service
  // noinspection ES6ModulesDependencies
  fetchDataSources(): Promise<DataSource[]> {
    // noinspection ES6ModulesDependencies
    return new Promise<DataSource[]>((resolve: (DataSource[]) => void) => {
      setTimeout(() => {
        resolve([
          {
            name: 'fetchedDS1',
            jdbcDriverClass: '',
            jdbcUrl: '',
            authentication: {
              user: '',
              password: ''
            },
            sqlStatement: ''
          },
          {
            name: 'fetchedDS2',
            jdbcDriverClass: '',
            jdbcUrl: '',
            authentication: {
              user: '',
              password: ''
            },
            sqlStatement: ''
          },
          {
            name: 'fetchedDS3',
            jdbcDriverClass: '',
            jdbcUrl: '',
            authentication: {
              user: '',
              password: ''
            },
            sqlStatement: ''
          },
          {
            name: 'fetchedDS4',
            jdbcDriverClass: '',
            jdbcUrl: '',
            authentication: {
              user: '',
              password: ''
            },
            sqlStatement: ''
          },
          {
            name: 'fetchedDS5',
            jdbcDriverClass: '',
            jdbcUrl: '',
            authentication: {
              user: '',
              password: ''
            },
            sqlStatement: ''
          }
        ]);
      }, this.latency);
    });
  }
}
