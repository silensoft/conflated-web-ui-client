// @flow

import type { DispatchAction } from 'oo-redux-utils';
import { AbstractDispatchingAction } from 'oo-redux-utils';
import type { DataSourceSelectorState } from '../state/DataSourceSelectorState';

// eslint-disable-next-line no-undef
export default class AbstractDataSourceSelectorDispatchingAction extends AbstractDispatchingAction<DataSourceSelectorState> {
  constructor(dispatchAction: DispatchAction) {
    super('', dispatchAction);
  }
}
