// @flow

import AbstractDataSourceSelectorAction from './AbstractDataSourceSelectorAction';
import type { DataSourceSelectorState } from '../state/DataSourceSelectorState';

export default class HideDataSourceChangeConfirmationAction extends AbstractDataSourceSelectorAction {
  performActionAndReturnNewState(currentState: DataSourceSelectorState): DataSourceSelectorState {
    return {
      ...currentState,
      isDataSourceChangeConfirmationShown: false
    };
  }
}
