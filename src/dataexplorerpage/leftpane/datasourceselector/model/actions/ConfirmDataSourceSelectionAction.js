// @flow

import type { DataSourceSelectorState } from '../state/DataSourceSelectorState';
import AbstractDataSourceSelectorAction from './AbstractDataSourceSelectorAction';

export default class ConfirmDataSourceSelectionAction extends AbstractDataSourceSelectorAction {
  performActionAndReturnNewState(currentState: DataSourceSelectorState): DataSourceSelectorState {
    return {
        ...currentState,
        selectedDataSourceToConfirm: null,
        isDataSourceChangeConfirmationShown: false
      };
  }
}
