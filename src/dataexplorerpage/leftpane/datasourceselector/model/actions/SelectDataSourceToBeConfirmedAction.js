// @flow

import type { DataSourceSelectorState } from '../state/DataSourceSelectorState';
import type { DataSource } from '../../../../../common/model/state/datasource/DataSource';
import AbstractDataSourceSelectorAction from './AbstractDataSourceSelectorAction';

export default class SelectDataSourceToBeConfirmedAction extends AbstractDataSourceSelectorAction {
  +dataSource: DataSource;

  constructor(dataSource: DataSource) {
    super();
    this.dataSource = dataSource;
  }

  performActionAndReturnNewState(currentState: DataSourceSelectorState): DataSourceSelectorState {
    return {
      ...currentState,
      selectedDataSourceToConfirm: this.dataSource,
      isDataSourceChangeConfirmationShown: true
    };
  }
}
