// @flow

import { Inject } from 'noicejs';
import type { DispatchAction } from 'oo-redux-utils';
import AbstractDataSourceSelectorAction from './AbstractDataSourceSelectorAction';
import type { DataSourceSelectorState } from '../state/DataSourceSelectorState';
import DataSourceService from '../service/DataSourceService';
import type { DataSource } from '../../../../../common/model/state/datasource/DataSource';
import AbstractDataSourceSelectorDispatchingAction from './AbstractDataSourceSelectorDispatchingAction';

type ConstructorArgs = {
  dataSourceService: DataSourceService,
  dispatchAction: DispatchAction
};

export default
@Inject('dataSourceService')
class StartFetchDataSourcesAction extends AbstractDataSourceSelectorDispatchingAction {
  +dataSourceService: DataSourceService;

  constructor({ dataSourceService, dispatchAction }: ConstructorArgs) {
    super(dispatchAction);
    this.dataSourceService = dataSourceService;
  }

  performActionAndReturnNewState(currentState: DataSourceSelectorState): DataSourceSelectorState {
    this.dataSourceService
      .fetchDataSources()
      .then((dataSources: DataSource[]) =>
        this.dispatchAction(new FinishFetchDataSourcesAction(dataSources))
      );

    return {
      ...currentState,
      isFetchingDataSources: true
    };
  }
}

class FinishFetchDataSourcesAction extends AbstractDataSourceSelectorAction {
  +dataSources: DataSource[];

  constructor(dataSources: DataSource[]) {
    super();
    this.dataSources = dataSources;
  }

  performActionAndReturnNewState(currentState: DataSourceSelectorState): DataSourceSelectorState {
    return {
      ...currentState,
      dataSources: this.dataSources,
      isFetchingDataSources: false
    };
  }
}
