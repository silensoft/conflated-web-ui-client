// @flow

import type { DataSource } from '../../../../../common/model/state/datasource/DataSource';

export type DataSourceSelectorState = $Exact<{
  +dataSources: DataSource[],
  +isFetchingDataSources: boolean,
  +isDataSourceChangeConfirmationShown: boolean,
  +selectedDataSourceToConfirm: ?DataSource
}>;
