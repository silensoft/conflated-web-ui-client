// @flow

import React, { useCallback } from 'react';
import type { Element } from 'react';
import { Popup } from 'semantic-ui-react';
import classNames from 'classnames';
import { selected } from '../ChartIcons.module.scss';
import type { ChartType } from '../../../../../../common/components/chartarea/chart/model/state/types/ChartType';

type Props = $Exact<{
  chartType: ChartType,
  content: ?string,
  iconClassName: any,
  notifyDragEnd: () => void,
  notifyDragStart: () => void,
  selectedChartType: ChartType,
  selectChartType: (chartType: ChartType) => void,
  tooltipText: string
}>;

const ChartIcon = ({
  chartType,
  content,
  iconClassName,
  notifyDragEnd,
  notifyDragStart,
  selectChartType,
  selectedChartType,
  tooltipText
}: Props): Element<any> => {
  const onDragStart = useCallback((event: SyntheticDragEvent<HTMLSpanElement>) => {
    event.dataTransfer.setData('chartType', chartType);
    notifyDragStart();
  });

  const className = classNames(iconClassName, { [selected]: chartType === selectedChartType });

  return (
    <Popup
      trigger={
        <span
          className={className}
          draggable
          onDragStart={onDragStart}
          onDragEnd={notifyDragEnd}
          onClick={() => selectChartType(chartType)}
        >
          {content}
        </span>
      }
      content={tooltipText}
      inverted
    />
  );
};

ChartIcon.defaultProps = {
  content: null
};

export default ChartIcon;
