// @flow

import type { Element } from 'react';
import React from 'react';
import {
  areaChartIcon,
  barChartIcon,
  boxPlotIcon,
  bubbleChartIcon,
  candlestickChartIcon,
  chartIcons,
  columnChartIcon,
  dataTableIcon,
  donutChartIcon,
  heatmapIcon,
  lineChartIcon,
  mapIcon,
  pieChartIcon,
  radarChartIcon,
  scatterPlotIcon,
  statisticIcon
} from './ChartIcons.module.scss';
import ChartIcon from './charticon/ChartIcon';
import type { ChartType } from '../../../../../common/components/chartarea/chart/model/state/types/ChartType';

type Props = $Exact<{
  notifyDragEnd: () => void,
  notifyDragStart: () => void,
  selectChartType: (chartType: ChartType) => void,
  selectedChartType: ChartType
}>;

const ChartIcons = (props: Props): Element<any> => (
  <div className={chartIcons}>
    <ChartIcon {...props} chartType="column" iconClassName={columnChartIcon} tooltipText="Column chart" />
    <ChartIcon {...props} chartType="bar" iconClassName={barChartIcon} tooltipText="Bar chart" />
    <ChartIcon {...props} chartType="area" iconClassName={areaChartIcon} tooltipText="Area chart" />
    <ChartIcon {...props} chartType="line" iconClassName={lineChartIcon} tooltipText="Line chart" />
    <ChartIcon
      {...props}
      chartType="donut"
      iconClassName={donutChartIcon}
      tooltipText="Donut or Gauge chart"
    />
    <ChartIcon {...props} chartType="pie" iconClassName={pieChartIcon} tooltipText="Pie chart" />
    <ChartIcon {...props} chartType="bubble" iconClassName={bubbleChartIcon} tooltipText="Bubble chart" />
    <ChartIcon {...props} chartType="scatter" iconClassName={scatterPlotIcon} tooltipText="Scatter plot" />
    <ChartIcon
      {...props}
      chartType="candlestick"
      iconClassName={candlestickChartIcon}
      tooltipText="Candlestick chart"
    />
    <ChartIcon {...props} chartType="boxplot" iconClassName={boxPlotIcon} tooltipText="Box plot" />
    <ChartIcon {...props} chartType="radar" iconClassName={radarChartIcon} tooltipText="Radar chart" />
    <ChartIcon {...props} chartType="heatmap" iconClassName={heatmapIcon} tooltipText="Heatmap" />
    <ChartIcon {...props} chartType="datatable" iconClassName={dataTableIcon} tooltipText="Data table" />
    <ChartIcon {...props} chartType="map" iconClassName={mapIcon} tooltipText="Map" />
    <ChartIcon
      {...props}
      chartType="statistic"
      iconClassName={statisticIcon}
      tooltipText="Statistic"
      content="123"
    />
  </div>
);

export default ChartIcons;
