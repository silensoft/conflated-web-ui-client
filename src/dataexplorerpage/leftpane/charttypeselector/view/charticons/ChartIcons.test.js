import React from 'react';
import { shallow as renderShallow } from 'enzyme/build';
import ChartIcons from './ChartIcons';

test('ChartTypeSelector should render correctly', () => {
  const renderedChartIcons = renderShallow(<ChartIcons />);
  expect(renderedChartIcons).toMatchSnapshot();
});
