// @flow

import type { Element } from 'react';
import React from 'react';
import { connect } from 'react-redux';
import type { Dispatch } from 'oo-redux-utils';
import type { AppState } from '../../../../store/AppState';
import ChartIcons from './charticons/ChartIcons';
import ChartTypeSelectorControllerFactory from '../controller/ChartTypeSelectorControllerFactory';
import SelectorView from '../../../../common/components/selector/view/SelectorView';

const mapAppStateToComponentProps = (appState: AppState) => ({
  selectedChart: appState.dataExplorerPage.chartAreaState.selectedChart
});

const createController = (dispatch: Dispatch) =>
  new ChartTypeSelectorControllerFactory(dispatch).createController();

type MappedState = $Call<typeof mapAppStateToComponentProps, AppState>;
type Controller = $Call<typeof createController, Dispatch>;
type Props = $Exact<{ ...MappedState, ...Controller }>;

const ChartTypeSelectorView = ({
  notifyDragEnd,
  notifyDragStart,
  selectChartType,
  selectedChart
}: Props): Element<any> => (
  <SelectorView
    id="chartTypeSelector"
    titleText="CHART TYPE"
    selectorContent={
      <ChartIcons
        selectedChartType={selectedChart.chartType}
        selectChartType={selectChartType}
        notifyDragStart={notifyDragStart}
        notifyDragEnd={notifyDragEnd}
      />
    }
    selectorStateNamespace="chartTypeSelector"
  />
);

export default connect<Props, $Exact<{}>, _, _, _, _>(
  mapAppStateToComponentProps,
  createController
)(ChartTypeSelectorView);
