import React from 'react';
import { shallow as renderShallow } from 'enzyme/build';
import ChartTypeSelector from './ChartTypeSelectorView';

test('ChartTypeSelector should render correctly', () => {
  const renderedChartTypeSelector = renderShallow(<ChartTypeSelector />);
  expect(renderedChartTypeSelector).toMatchSnapshot();
});
