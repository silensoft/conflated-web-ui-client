// @flow

export type LayoutSelectorState = $Exact<{
  +isLayoutLocked: boolean
}>;
