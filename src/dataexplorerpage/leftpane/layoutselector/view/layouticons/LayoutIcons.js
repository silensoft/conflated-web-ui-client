// @flow

import type { Element } from 'react';
import React from 'react';
import {
  layoutIcon1,
  layoutIcon10,
  layoutIcon11,
  layoutIcon12,
  layoutIcon13,
  layoutIcon14,
  layoutIcon15,
  layoutIcon2,
  layoutIcon3,
  layoutIcon4,
  layoutIcon5,
  layoutIcon6,
  layoutIcon7,
  layoutIcon8,
  layoutIcon9,
  layoutIcons
} from './LayoutIcons.module.scss';
import LayoutIcon from './layouticon/LayoutIcon';
import layout1 from '../../model/state/layouts/layout1';
import layout2 from '../../model/state/layouts/layout2';
import layout3 from '../../model/state/layouts/layout3';
import layout4 from '../../model/state/layouts/layout4';
import layout5 from '../../model/state/layouts/layout5';
import layout6 from '../../model/state/layouts/layout6';
import layout7 from '../../model/state/layouts/layout7';
import layout8 from '../../model/state/layouts/layout8';
import layout9 from '../../model/state/layouts/layout9';
import layout10 from '../../model/state/layouts/layout10';
import layout11 from '../../model/state/layouts/layout11';
import layout12 from '../../model/state/layouts/layout12';
import layout13 from '../../model/state/layouts/layout13';
import layout14 from '../../model/state/layouts/layout14';
import layout15 from '../../model/state/layouts/layout15';
import type { Layout } from '../../../../../common/components/chartarea/model/state/types/Layout';

type Props = $Exact<{
  selectedLayout: Layout,
  selectLayout: (layout: Layout) => void
}>;

const LayoutIcons = (props: Props): Element<any> => (
  <div className={layoutIcons}>
    <LayoutIcon {...props} iconClassName={layoutIcon1} layout={layout1} />
    <LayoutIcon {...props} iconClassName={layoutIcon2} layout={layout2} />
    <LayoutIcon {...props} iconClassName={layoutIcon3} layout={layout3} />
    <LayoutIcon {...props} iconClassName={layoutIcon4} layout={layout4} />
    <LayoutIcon {...props} iconClassName={layoutIcon5} layout={layout5} />
    <LayoutIcon {...props} iconClassName={layoutIcon6} layout={layout6} />
    <LayoutIcon {...props} iconClassName={layoutIcon7} layout={layout7} />
    <LayoutIcon {...props} iconClassName={layoutIcon8} layout={layout8} />
    <LayoutIcon {...props} iconClassName={layoutIcon9} layout={layout9} />
    <LayoutIcon {...props} iconClassName={layoutIcon10} layout={layout10} />
    <LayoutIcon {...props} iconClassName={layoutIcon11} layout={layout11} />
    <LayoutIcon {...props} iconClassName={layoutIcon12} layout={layout12} />
    <LayoutIcon {...props} iconClassName={layoutIcon13} layout={layout13} />
    <LayoutIcon {...props} iconClassName={layoutIcon14} layout={layout14} />
    <LayoutIcon {...props} iconClassName={layoutIcon15} layout={layout15} />
  </div>
);

export default LayoutIcons;
