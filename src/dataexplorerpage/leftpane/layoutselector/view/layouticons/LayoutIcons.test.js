import React from 'react';
import { shallow as renderShallow } from 'enzyme/build';
import LayoutIcons from './LayoutIcons';

test('LayoutIcons should render correctly', () => {
  const renderedLayoutIcons = renderShallow(<LayoutIcons />);
  expect(renderedLayoutIcons).toMatchSnapshot();
});
