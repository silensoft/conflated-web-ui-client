// @flow

import React from 'react';
import type { Element } from 'react';
import classNames from 'classnames';
import { selected } from '../LayoutIcons.module.scss';
import type { Layout } from '../../../../../../common/components/chartarea/model/state/types/Layout';

type Props = $Exact<{
  iconClassName: any,
  layout: Layout,
  selectedLayout: Layout,
  selectLayout: (layout: Layout) => void
}>;

const LayoutIcon = ({ iconClassName, layout, selectedLayout, selectLayout }: Props): Element<any> => {
  const className = classNames(iconClassName, { [selected]: layout === selectedLayout });
  return <span className={className} onClick={() => selectLayout(layout)} />;
};

export default LayoutIcon;
