// @flow

import type { Element } from 'react';
import React, { useCallback } from 'react';
import { connect } from 'react-redux';
import type { Dispatch } from 'oo-redux-utils';
import OOReduxUtils from 'oo-redux-utils';
import LayoutIcons from './layouticons/LayoutIcons';
import type { AppState } from '../../../../store/AppState';
import LayoutSelectorControllerFactory from '../controller/LayoutSelectorControllerFactory';
import SelectorView from '../../../../common/components/selector/view/SelectorView';
import LayoutSelectorTitleView from './title/LayoutSelectorTitleView';

const mapAppStateToComponentProps = (appState: AppState) =>
  OOReduxUtils.mergeOwnAndForeignState(appState.dataExplorerPage.layoutSelectorState, {
    layout: appState.dataExplorerPage.chartAreaState.layout,

    shouldShowDataExplorerPageLeftPanePermanently:
      appState.common.pageStates.dataExplorerPage.shouldShowPagePanePermanently.leftPane
  });

const createController = (dispatch: Dispatch) =>
  new LayoutSelectorControllerFactory(dispatch).createController();

type MappedState = $Call<typeof mapAppStateToComponentProps, AppState>;
type Controller = $Call<typeof createController, Dispatch>;
type Props = $Exact<{ ...MappedState, ...Controller }>;

const LayoutSelectorView = ({
  isLayoutLocked,
  layout,
  selectLayout,
  shouldShowDataExplorerPageLeftPanePermanently,
  toggleLayoutLocked,
  toggleShouldShowDataExplorerPageLeftPanePermanently
}: Props): Element<any> => {
  const handleLockIconClick = useCallback((event: SyntheticEvent<HTMLElement>) => {
    event.stopPropagation();
    toggleLayoutLocked();
  });

  const handlePinIconClick = useCallback((event: SyntheticEvent<HTMLElement>) => {
    event.stopPropagation();
    toggleShouldShowDataExplorerPageLeftPanePermanently();
  });

  return (
    <SelectorView
      id="layoutSelector"
      titleText="LAYOUT"
      titleContent={
        <LayoutSelectorTitleView
          handleLockIconClick={handleLockIconClick}
          handlePinIconClick={handlePinIconClick}
          isLayoutLocked={isLayoutLocked}
          shouldShowDataExplorerPageLeftPanePermanently={shouldShowDataExplorerPageLeftPanePermanently}
        />
      }
      selectorContent={<LayoutIcons selectedLayout={layout} selectLayout={selectLayout} />}
      selectorStateNamespace="layoutSelector"
    />
  );
};

export default connect<Props, $Exact<{}>, _, _, _, _>(
  mapAppStateToComponentProps,
  createController
)(LayoutSelectorView);
