// @flow

import type { Element } from 'react';
import React from 'react';
import { Icon, Popup } from 'semantic-ui-react';
import { actionIcon } from './LayoutSelectorTitleView.module.scss';

type Props = $Exact<{
  handleLockIconClick: (event: SyntheticEvent<HTMLElement>) => void,
  handlePinIconClick: (event: SyntheticEvent<HTMLElement>) => void,
  isLayoutLocked: boolean,
  shouldShowDataExplorerPageLeftPanePermanently: boolean
}>;

const LayoutSelectorTitleView = ({
  handleLockIconClick,
  handlePinIconClick,
  isLayoutLocked,
  shouldShowDataExplorerPageLeftPanePermanently
}: Props): Element<any> => (
  <React.Fragment>
    <Popup
      inverted
      trigger={
        <Icon
          className={actionIcon}
          name={isLayoutLocked ? 'lock' : 'lock open'}
          onClick={handleLockIconClick}
        />
      }
      content={
        isLayoutLocked
          ? 'Click to enable dragging and resizing charts in chart area'
          : 'Click to disable dragging and resizing charts in chart area'
      }
    />
    <Popup
      inverted
      trigger={
        <Icon
          className={actionIcon}
          style={{
            color: shouldShowDataExplorerPageLeftPanePermanently
              ? 'var(--secondary-text-color-on-hover)'
              : 'var(--secondary-text-color)'
          }}
          name="pin"
          onClick={handlePinIconClick}
        />
      }
      content="Pin or unpin left pane"
    />
  </React.Fragment>
);

export default LayoutSelectorTitleView;
