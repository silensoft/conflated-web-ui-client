import React from 'react';
import { shallow as renderShallow } from 'enzyme/build';
import LayoutSelector from './LayoutSelectorView';

test('LayoutSelector should render correctly', () => {
  const renderedLayoutSelector = renderShallow(<LayoutSelector />);
  expect(renderedLayoutSelector).toMatchSnapshot();
});
