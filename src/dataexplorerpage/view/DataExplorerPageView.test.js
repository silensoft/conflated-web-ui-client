import React from 'react';
import { shallow as renderShallow } from 'enzyme';
import DataExplorer from './DataExplorerPageView';

test('DataExplorer should render correctly', () => {
  const renderedDataExplorer = renderShallow(<DataExplorer />);
  expect(renderedDataExplorer).toMatchSnapshot();
});
