// @flow

import React from 'react';
import type { Element } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import type { Dispatch } from 'oo-redux-utils';
import { fullScreenMode } from './DataExplorerPageView.module.scss';
import type { AppState } from '../../store/AppState';
import PageView from '../../common/components/page/view/PageView';
import DataExplorerPageLeftPaneView from '../leftpane/view/DataExplorerPageLeftPaneView';
import DataExplorerPageRightPaneView from '../rightpane/view/DataExplorerPageRightPaneView';
import ChartAreaView from '../../common/components/chartarea/view/ChartAreaView';

const mapAppStateToComponentProps = (appState: AppState) => ({
  isFullScreenModeActive: appState.headerState.isFullScreenModeActive
});

type MappedState = $Call<typeof mapAppStateToComponentProps, AppState>;
type Props = $Exact<{ ...MappedState, dispatch: Dispatch }>;

const DataExplorerPageView = ({ isFullScreenModeActive }: Props): Element<any> => (
  <PageView
    className={isFullScreenModeActive ? fullScreenMode : ''}
    leftPane={<DataExplorerPageLeftPaneView />}
    middlePane={<ChartAreaView pageStateNamespace="dataExplorerPage" />}
    rightPane={<DataExplorerPageRightPaneView />}
    pageStateNamespace="dataExplorerPage"
    showPaneActivatorHintsOnComponentMount={false}
  />
);

export default connect<Props, $Exact<{}>, _, _, _, _>(mapAppStateToComponentProps)(DataExplorerPageView);
