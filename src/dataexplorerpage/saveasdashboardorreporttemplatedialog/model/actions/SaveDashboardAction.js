// @flow

import type { DispatchAction } from 'oo-redux-utils';
import type { Chart } from '../../../../common/components/chartarea/chart/model/state/Chart';
import type { Layout } from '../../../../common/components/chartarea/model/state/types/Layout';
import type { SaveAsDashboardOrReportTemplateDialogState } from '../state/SaveAsDashboardOrReportTemplateDialogState';
import type { DashboardGroup } from '../../../../dashboardspage/model/state/entities/DashboardGroup';
import AbstractSaveAsDashboardOrReportTemplateDialogDispatchingAction from './AbstractSaveAsDashboardOrReportTemplateDialogDispatchingAction';

export default class SaveDashboardAction extends AbstractSaveAsDashboardOrReportTemplateDialogDispatchingAction {
  +dashboardGroupName: string;

  +dashboardName: string;

  +dashboardGroups: DashboardGroup[];

  +charts: Chart[];

  +layout: Layout;

  // noinspection OverlyComplexFunctionJS
  constructor(
    dispatchAction: DispatchAction,
    dashboardGroupName: string,
    dashboardName: string,
    dashboardGroups: DashboardGroup[],
    charts: Chart[],
    layout: Layout
  ) {
    super(dispatchAction);
    this.dashboardGroupName = dashboardGroupName;
    this.dashboardName = dashboardName;
    this.dashboardGroups = dashboardGroups;
    this.charts = charts;
    this.layout = layout;
  }

  performActionAndReturnNewState(
    currentState: SaveAsDashboardOrReportTemplateDialogState
  ): SaveAsDashboardOrReportTemplateDialogState {
    return {
      ...currentState,
      isOpen: false,
      shouldShowSavedSuccessfullyNotification: true
    };
  }
}
