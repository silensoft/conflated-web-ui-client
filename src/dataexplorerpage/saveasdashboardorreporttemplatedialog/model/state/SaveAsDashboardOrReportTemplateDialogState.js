// @flow

export type SaveAsDashboardOrReportTemplateDialogState = $Exact<{
  +isOpen: boolean,
  +shouldShowSavedSuccessfullyNotification: boolean
}>;
