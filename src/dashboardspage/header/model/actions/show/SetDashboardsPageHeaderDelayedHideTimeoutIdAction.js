// @flow

import AbstractDashboardsPageHeaderAction from '../AbstractDashboardsPageHeaderAction';
import type { DashboardsPageHeaderState } from '../../state/DashboardsPageHeaderState';

export default class SetDashboardsPageHeaderDelayedHideTimeoutIdAction extends AbstractDashboardsPageHeaderAction {
  +timeoutId: TimeoutID;

  constructor(timeoutId: TimeoutID) {
    super();
    this.timeoutId = timeoutId;
  }

  performActionAndReturnNewState(currentState: DashboardsPageHeaderState): DashboardsPageHeaderState {
    clearTimeout(currentState.dashboardsHeaderDelayedHideTimeoutId);

    return {
      ...currentState,
      dashboardsHeaderDelayedHideTimeoutId: this.timeoutId
    };
  }
}
