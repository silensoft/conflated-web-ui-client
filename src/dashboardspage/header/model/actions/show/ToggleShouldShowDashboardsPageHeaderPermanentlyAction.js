// @flow

import AbstractDashboardsPageHeaderAction from '../AbstractDashboardsPageHeaderAction';
import type { DashboardsPageHeaderState } from '../../state/DashboardsPageHeaderState';

export default class ToggleShouldShowDashboardsPageHeaderPermanentlyAction extends AbstractDashboardsPageHeaderAction {
  performActionAndReturnNewState(currentState: DashboardsPageHeaderState): DashboardsPageHeaderState {
    return {
      ...currentState,
      shouldShowDashboardsHeaderPermanently: !currentState.shouldShowDashboardsHeaderPermanently
    };
  }
}
