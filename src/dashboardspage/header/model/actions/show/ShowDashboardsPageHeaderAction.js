// @flow

import _ from 'lodash';
import AbstractDashboardsPageHeaderAction from '../AbstractDashboardsPageHeaderAction';
import type { DashboardsPageHeaderState } from '../../state/DashboardsPageHeaderState';

export default class ShowDashboardsPageHeaderAction extends AbstractDashboardsPageHeaderAction {
  performActionAndReturnNewState(currentState: DashboardsPageHeaderState): DashboardsPageHeaderState {
    clearTimeout(currentState.dashboardsHeaderDelayedHideTimeoutId);

    return {
      ...currentState,
      shouldShowDashboardsHeader: true,
      dashboardsHeaderHideDelayInMillis: 200,
      dashboardsHeaderDelayedHideTimeoutId: setTimeout(() => _.noop(), Number.MAX_SAFE_INTEGER)
    };
  }
}
