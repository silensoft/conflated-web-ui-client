// @flow

export type DashboardsPageHeaderState = $Exact<{
  +shouldShowDashboardsHeader: boolean,
  +shouldShowDashboardsHeaderPermanently: boolean,
  +dashboardsHeaderHideDelayInMillis: number,
  +dashboardsHeaderHideTimeoutId: TimeoutID,
  +dashboardsHeaderDelayedHideTimeoutId: TimeoutID
}>;
