// @flow

import React from 'react';
import type { Element } from 'react';
import { Icon, Popup } from 'semantic-ui-react';
import { actionIcon } from '../DashboardsPageHeaderView.module.scss';

type Props = $Exact<{
  shouldShowDashboardsHeaderPermanently: boolean,
  toggleShouldShowDashboardsHeaderPermanently: () => void
}>;

const DashboardsPageHeaderPinIconView = ({
  shouldShowDashboardsHeaderPermanently,
  toggleShouldShowDashboardsHeaderPermanently
}: Props): Element<any> => (
  <Popup
    inverted
    trigger={
      <Icon
        className={actionIcon}
        style={{ color: shouldShowDashboardsHeaderPermanently ? 'var(--brand-color-1)' : 'rgb(0,0,0' }}
        size="large"
        name="pin"
        onClick={toggleShouldShowDashboardsHeaderPermanently}
      />
    }
    content={shouldShowDashboardsHeaderPermanently ? 'Unpin dashboards header' : 'Pin dashboards header'}
  />
);

export default DashboardsPageHeaderPinIconView;
