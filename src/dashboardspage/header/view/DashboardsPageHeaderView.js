// @flow

import type { Element } from 'react';
import React, { useMemo } from 'react';
import { connect } from 'react-redux';
import type { Dispatch } from 'oo-redux-utils';
import OOReduxUtils from 'oo-redux-utils';
import classNames from 'classnames';
import { Dropdown } from 'semantic-ui-react';
import {
  dashboardGroupSelector,
  dashboardSelector,
  dashboardSelectors,
  emptySpace,
  fullScreen,
  header,
  visible
} from './DashboardsPageHeaderView.module.scss';
import type { AppState } from '../../../store/AppState';
import DashboardsPageHeaderControllerFactory from '../controller/DashboardsPageHeaderControllerFactory';
import type { Dashboard } from '../../model/state/entities/Dashboard';
import type { DashboardGroup } from '../../model/state/entities/DashboardGroup';
import DashboardsSlideShowSlideChangeIntervalInputView from './slideshow/slidechangeintervalinput/DashboardsSlideShowSlideChangeIntervalInputView';
import DashboardsSlideShowPlayOrPauseButtonView from './slideshow/playorpausebutton/DashboardsSlideShowPlayOrPauseButtonView';
import DashboardsPageHeaderPinIconView from './pinicon/DashboardsPageHeaderPinIconView';
import DashboardsPageControllerFactory from '../../controller/DashboardsPageControllerFactory';

const mapAppStateToComponentProps = (appState: AppState) =>
  OOReduxUtils.mergeOwnAndForeignState(appState.dashboardsPage.headerState, {
    ...appState.dashboardsPage.dashboardsState,
    isFullScreenModeActive: appState.headerState.isFullScreenModeActive
  });

const createController = (dispatch: Dispatch) => ({
  ...new DashboardsPageControllerFactory(dispatch).createController(),
  ...new DashboardsPageHeaderControllerFactory(dispatch).createController()
});

type MappedState = $Call<typeof mapAppStateToComponentProps, AppState>;
type Controller = $Call<typeof createController, Dispatch>;
type Props = $Exact<{ ...MappedState, ...Controller }>;

const DashboardsPageHeaderView = ({
  cancelDelayedDashboardsHeaderHide,
  changeDashboardsSlideChangeInterval,
  dashboardGroups,
  dashboardSlideChangeIntervalInSecsStr,
  isDashboardsSlideShowPlaying,
  isFullScreenModeActive,
  hideDashboardsHeaderDelayed,
  selectedDashboardGroup,
  selectedDashboard,
  shouldShowDashboardsHeader,
  shouldShowDashboardsHeaderPermanently,
  showDashboard,
  showDashboardGroup,
  toggleDashboardsSlideShowPlay,
  toggleShouldShowDashboardsHeaderPermanently
}: Props): Element<any> => {
  const dashboardDropDownItems = useMemo(
    (): Array<Element<any>> | void =>
      selectedDashboardGroup?.dashboards.map((dashboard: Dashboard) => (
        <Dropdown.Item
          key={dashboard.name}
          text={dashboard.name}
          value={dashboard}
          onClick={() => showDashboard(dashboard)}
        />
      )),
    [selectedDashboardGroup]
  );

  const dashboardGroupDropDownItems = useMemo(
    (): Array<Element<any>> =>
      dashboardGroups.map((dashboardGroup: DashboardGroup) => (
        <Dropdown.Item
          key={dashboardGroup.name}
          text={dashboardGroup.name}
          value={dashboardGroup}
          onClick={() => showDashboardGroup(dashboardGroup)}
        />
      )),
    [dashboardGroups]
  );

  let dashboardSelectorContent;
  if (selectedDashboard) {
    dashboardSelectorContent = (
      <Dropdown scrolling className={dashboardSelector} text={selectedDashboard.name.toUpperCase()}>
        <Dropdown.Menu>{dashboardDropDownItems}</Dropdown.Menu>
      </Dropdown>
    );
  }

  let dashboardGroupSelectorContent;
  if (selectedDashboardGroup) {
    dashboardGroupSelectorContent = (
      <Dropdown scrolling className={dashboardGroupSelector} text={selectedDashboardGroup.name}>
        <Dropdown.Menu>{dashboardGroupDropDownItems}</Dropdown.Menu>
      </Dropdown>
    );
  }

  const className = classNames(header, {
    [visible]: shouldShowDashboardsHeader || shouldShowDashboardsHeaderPermanently,
    [fullScreen]: isFullScreenModeActive
  });

  return (
    <header
      className={className}
      onMouseEnter={cancelDelayedDashboardsHeaderHide}
      onFocus={cancelDelayedDashboardsHeaderHide}
      onMouseLeave={hideDashboardsHeaderDelayed}
      onBlur={hideDashboardsHeaderDelayed}
    >
      <div className={dashboardSelectors}>
        {dashboardSelectorContent}
        {dashboardGroupSelectorContent}
      </div>
      <div className={emptySpace} />
      <div>
        <DashboardsSlideShowSlideChangeIntervalInputView
          changeDashboardsSlideChangeInterval={changeDashboardsSlideChangeInterval}
          slideChangeIntervalInSecsStr={dashboardSlideChangeIntervalInSecsStr}
        />
        <DashboardsSlideShowPlayOrPauseButtonView
          isDashboardsSlideShowPlaying={isDashboardsSlideShowPlaying}
          toggleDashboardsSlideShowPlay={toggleDashboardsSlideShowPlay}
        />
        <DashboardsPageHeaderPinIconView
          shouldShowDashboardsHeaderPermanently={shouldShowDashboardsHeaderPermanently}
          toggleShouldShowDashboardsHeaderPermanently={toggleShouldShowDashboardsHeaderPermanently}
        />
      </div>
    </header>
  );
};

export default connect<Props, $Exact<{}>, _, _, _, _>(
  mapAppStateToComponentProps,
  createController
)(DashboardsPageHeaderView);
