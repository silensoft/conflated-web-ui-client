// @flow

import React from 'react';
import type { Element } from 'react';
import { Icon, Popup } from 'semantic-ui-react';
import { actionIcon } from '../../DashboardsPageHeaderView.module.scss';

type Props = $Exact<{
  isDashboardsSlideShowPlaying: boolean,
  toggleDashboardsSlideShowPlay: () => void
}>;

const DashboardsSlideShowPlayOrPauseButtonView = ({
  isDashboardsSlideShowPlaying,
  toggleDashboardsSlideShowPlay
}: Props): Element<any> => (
  <Popup
    inverted
    trigger={
      <Icon
        className={actionIcon}
        size="large"
        name={isDashboardsSlideShowPlaying ? 'pause' : 'play'}
        onClick={toggleDashboardsSlideShowPlay}
      />
    }
    content={
      isDashboardsSlideShowPlaying
        ? 'Stop dashboards slide show'
        : 'Start slide show of dashboards in current dashboard group'
    }
  />
);

export default DashboardsSlideShowPlayOrPauseButtonView;
