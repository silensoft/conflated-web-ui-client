// @flow

import React from 'react';
import type { Element } from 'react';
import { Input, Popup } from 'semantic-ui-react';
import { slideChangeIntervalInput } from './DashbaordsSlideShowSlideChangeIntervalInputView.module.scss';

type Props = $Exact<{
  changeDashboardsSlideChangeInterval: (value: string) => void,
  slideChangeIntervalInSecsStr: string
}>;

const DashboardsSlideShowSlideChangeIntervalInputView = ({
  changeDashboardsSlideChangeInterval,
  slideChangeIntervalInSecsStr
}: Props): Element<any> => (
  <React.Fragment>
    <Popup
      inverted
      trigger={
        <Input
          className={slideChangeIntervalInput}
          value={slideChangeIntervalInSecsStr}
          onChange={({ currentTarget: { value } }: SyntheticInputEvent<HTMLInputElement>) =>
            changeDashboardsSlideChangeInterval(value)
          }
        />
      }
      content="Slide change interval for dashboards slide show"
    />
    <span>secs</span>
  </React.Fragment>
);

export default DashboardsSlideShowSlideChangeIntervalInputView;
