// @flow

import type { Element } from 'react';
import React, { useCallback, useMemo } from 'react';
import { connect } from 'react-redux';
import type { Dispatch } from 'oo-redux-utils';
import DashboardListItem from './dashboardlistitem/DashboardListItem';
import type { AppState } from '../../../../store/AppState';
import SelectorWithDefaultActionsView from '../../../../common/components/selectorwithdefaultactions/view/SelectorWithDefaultActionsView';
import selectShownDashboards from '../model/state/selectors/selectShownDashboards';
import type { Dashboard } from '../../../model/state/entities/Dashboard';
import AllAndFavoritesTabView from '../../../../common/view/allandfavoritestabview/AllAndFavoritesTabView';
import DashboardsPageControllerFactory from '../../../controller/DashboardsPageControllerFactory';
import SelectorWithDefaultActionsControllerFactory from '../../../../common/components/selectorwithdefaultactions/controller/SelectorWithDefaultActionsControllerFactory';

const mapAppStateToComponentProps = (appState: AppState) => ({
  shownDashboards: selectShownDashboards(appState),
  selectedDashboard: appState.dashboardsPage.dashboardsState.selectedDashboard,
  isDashboardGroupSelectorOpen: appState.common.selectorStates.dashboardGroupSelector.isSelectorOpen
});

const createController = (dispatch: Dispatch) => ({
  toggleMaximizeSelector: new SelectorWithDefaultActionsControllerFactory(
    dispatch,
    'dashboardSelector'
  ).createController().toggleMaximizeSelector,

  showDashboard: new DashboardsPageControllerFactory(dispatch).createController().showDashboard
});

type MappedState = $Call<typeof mapAppStateToComponentProps, AppState>;
type Controller = $Call<typeof createController, Dispatch>;
type Props = { ...MappedState, ...Controller };

const DashboardSelectorView = ({
  isDashboardGroupSelectorOpen,
  selectedDashboard,
  showDashboard,
  shownDashboards,
  toggleMaximizeSelector
}: Props): Element<any> => {
  const handleMaximizeIconClick = useCallback(
    (event: SyntheticEvent<HTMLElement>) => {
      event.stopPropagation();

      toggleMaximizeSelector([
        {
          isOpen: isDashboardGroupSelectorOpen,
          stateNamespace: 'dashboardGroupSelector'
        }
      ]);
    },
    [isDashboardGroupSelectorOpen]
  );

  const dashboardListItems = useMemo(
    (): Array<Element<any>> =>
      shownDashboards.map((dashboard: Dashboard) => (
        <DashboardListItem
          key={dashboard.name}
          item={dashboard}
          selectedItem={selectedDashboard}
          onItemClick={showDashboard}
        />
      )),
    [shownDashboards, selectedDashboard]
  );

  return (
    <SelectorWithDefaultActionsView
      id="dashboardSelector"
      titleText="DASHBOARD"
      listItemsContent={
        <AllAndFavoritesTabView firstTabPaneListItems={dashboardListItems} secondTabPaneListItems={[]} />
      }
      handleMaximizeIconClick={handleMaximizeIconClick}
      selectorStateNamespace="dashboardSelector"
    />
  );
};

export default connect<Props, $Exact<{}>, _, _, _, _>(
  mapAppStateToComponentProps,
  createController
)(DashboardSelectorView);
