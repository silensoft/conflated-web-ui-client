// @flow

import type { Element } from 'react';
import React from 'react';
import type { ListItemViewProps } from '../../../../../common/view/listitems/listitem/ListItemView';
import ListItemView from '../../../../../common/view/listitems/listitem/ListItemView';
import type { Dashboard } from '../../../../model/state/entities/Dashboard';

const DashboardListItem = (props: ListItemViewProps<Dashboard>): Element<any> => <ListItemView {...props} />;
export default DashboardListItem;
