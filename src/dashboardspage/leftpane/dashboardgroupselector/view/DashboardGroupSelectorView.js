// @flow

import type { Element } from 'react';
import React, { useCallback, useMemo } from 'react';
import { connect } from 'react-redux';
import type { Dispatch } from 'oo-redux-utils';
import DashboardGroupListItem from './dashboardgrouplistitem/DashboardGroupListItem';
import type { AppState } from '../../../../store/AppState';
import DashboardGroupSelectorControllerFactory from '../controller/DashboardGroupSelectorControllerFactory';
import SelectorWithDefaultActionsView from '../../../../common/components/selectorwithdefaultactions/view/SelectorWithDefaultActionsView';
import selectShownDashboardGroups from '../model/state/selectors/selectShownDashboardGroups';
import type { DashboardGroup } from '../../../model/state/entities/DashboardGroup';
import AllAndFavoritesTabView from '../../../../common/view/allandfavoritestabview/AllAndFavoritesTabView';
import DashboardsPageControllerFactory from '../../../controller/DashboardsPageControllerFactory';
import SelectorWithDefaultActionsControllerFactory from '../../../../common/components/selectorwithdefaultactions/controller/SelectorWithDefaultActionsControllerFactory';

const mapAppStateToComponentProps = (appState: AppState) => ({
  shownDashboardGroups: selectShownDashboardGroups(appState),
  selectedDashboardGroup: appState.dashboardsPage.dashboardsState.selectedDashboardGroup,
  shouldShowDashboardsPageLeftPanePermanently:
    appState.common.pageStates.dashboardsPage.shouldShowPagePanePermanently.leftPane,
  isDashboardSelectorOpen: appState.common.selectorStates.dashboardSelector.isSelectorOpen
});

const createController = (dispatch: Dispatch) => ({
  toggleMaximizeSelector: new SelectorWithDefaultActionsControllerFactory(
    dispatch,
    'dashboardGroupSelector'
  ).createController().toggleMaximizeSelector,

  showDashboardGroup: new DashboardsPageControllerFactory(dispatch).createController().showDashboardGroup,
  ...new DashboardGroupSelectorControllerFactory(dispatch).createController()
});

type MappedState = $Call<typeof mapAppStateToComponentProps, AppState>;
type Controller = $Call<typeof createController, Dispatch>;
type Props = $Exact<{ ...MappedState, ...Controller }>;

const DashboardGroupSelectorView = ({
  isDashboardSelectorOpen,
  selectedDashboardGroup,
  showDashboardGroup,
  shownDashboardGroups,
  toggleMaximizeSelector,
  toggleShouldShowDashboardsPageLeftPanePermanently
}: Props): Element<any> => {
  const handleMaximizeIconClick = useCallback(
    (event: SyntheticEvent<HTMLElement>) => {
      event.stopPropagation();

      toggleMaximizeSelector([
        {
          isOpen: isDashboardSelectorOpen,
          stateNamespace: 'dashboardSelector'
        }
      ]);
    },
    [isDashboardSelectorOpen]
  );

  const handlePinIconClick = useCallback((event: SyntheticEvent<HTMLElement>) => {
    event.stopPropagation();
    toggleShouldShowDashboardsPageLeftPanePermanently();
  }, []);

  const dashboardGroupListItems = useMemo(
    (): Array<Element<any>> =>
      shownDashboardGroups.map((dashboardGroup: DashboardGroup) => (
        <DashboardGroupListItem
          key={dashboardGroup.name}
          item={dashboardGroup}
          selectedItem={selectedDashboardGroup}
          onItemClick={showDashboardGroup}
        />
      )),
    [shownDashboardGroups, selectedDashboardGroup]
  );

  return (
    <SelectorWithDefaultActionsView
      id="dashboardGroupSelector"
      titleText="DASHBOARD GROUP"
      listItemsContent={
        <AllAndFavoritesTabView firstTabPaneListItems={dashboardGroupListItems} secondTabPaneListItems={[]} />
      }
      handleMaximizeIconClick={handleMaximizeIconClick}
      handlePinIconClick={handlePinIconClick}
      selectorStateNamespace="dashboardGroupSelector"
    />
  );
};

export default connect<Props, $Exact<{}>, _, _, _, _>(
  mapAppStateToComponentProps,
  createController
)(DashboardGroupSelectorView);
