// @flow

import type { Element } from 'react';
import React from 'react';
import type { ListItemViewProps } from '../../../../../common/view/listitems/listitem/ListItemView';
import ListItemView from '../../../../../common/view/listitems/listitem/ListItemView';
import type { DashboardGroup } from '../../../../model/state/entities/DashboardGroup';

const DashboardGroupListItem = (props: ListItemViewProps<DashboardGroup>): Element<any> => (
  <ListItemView {...props} />
);

export default DashboardGroupListItem;
