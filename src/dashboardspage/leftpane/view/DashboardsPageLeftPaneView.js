// @flow

import _ from 'lodash';
import type { Element } from 'react';
import React, { useCallback, useEffect } from 'react';
import { connect } from 'react-redux';
import type { Dispatch } from 'oo-redux-utils';
import type { AppState } from '../../../store/AppState';
import DashboardsPageLeftPaneControllerFactory from '../controller/DashboardsPageLeftPaneControllerFactory';
import PagePaneView from '../../../common/view/pagepane/PagePaneView';
import DashboardsPageLeftPaneViewUtils from './DashboardsPageLeftPaneViewUtils';
import DashboardGroupSelectorView from '../dashboardgroupselector/view/DashboardGroupSelectorView';
import DashboardSelectorView from '../dashboardselector/view/DashboardSelectorView';

const mapAppStateToComponentProps = (appState: AppState) => ({
  isFullScreenModeActive: appState.headerState.isFullScreenModeActive,
  dashboardsPageLeftPaneGutterOffset: appState.common.pageStates.dashboardsPage.pagePaneGutterOffset.leftPane,
  shouldShowDashboardsPageLeftPane: appState.common.pageStates.dashboardsPage.shouldShowPagePane.leftPane,

  shouldShowDashboardsPageLeftPanePermanently:
    appState.common.pageStates.dashboardsPage.shouldShowPagePanePermanently.leftPane,

  isDashboardGroupSelectorOpen: appState.common.selectorStates.dashboardGroupSelector.isSelectorOpen,
  isDashboardSelectorOpen: appState.common.selectorStates.dashboardSelector.isSelectorOpen
});

const createController = (dispatch: Dispatch) =>
  new DashboardsPageLeftPaneControllerFactory(dispatch).createController();

type MappedState = $Call<typeof mapAppStateToComponentProps, AppState>;
type Controller = $Call<typeof createController, Dispatch>;
type Props = $Exact<{ ...MappedState, ...Controller }>;

const DashboardsPageLeftPaneView = ({
  hideDashboardsPageLeftPane,
  isDashboardGroupSelectorOpen,
  isDashboardSelectorOpen,
  isFullScreenModeActive,
  dashboardsPageLeftPaneGutterOffset,
  shouldShowDashboardsPageLeftPane,
  shouldShowDashboardsPageLeftPanePermanently
}: Props): Element<*> => {
  const updateSelectorContentHeights = useCallback(
    () =>
      _.before(2, () =>
        DashboardsPageLeftPaneViewUtils.updateSelectorContentHeights(
          isDashboardGroupSelectorOpen,
          isDashboardSelectorOpen
        )
      )(),
    [isDashboardGroupSelectorOpen, isDashboardSelectorOpen]
  );

  useEffect(() => updateSelectorContentHeights());
  useEffect(
    () => {
      setTimeout(() => updateSelectorContentHeights(), 1000);
    },
    [isFullScreenModeActive]
  );

  return (
    <PagePaneView
      id="dashboardsPageLeftPane"
      isFullScreenModeActive={isFullScreenModeActive}
      hidePagePane={hideDashboardsPageLeftPane}
      pane="leftPane"
      paneDefaultWidthCssVarName="dashboards-page-left-pane-default-width"
      paneGutterOffset={dashboardsPageLeftPaneGutterOffset}
      shouldShowPagePane={shouldShowDashboardsPageLeftPane}
      shouldShowPagePanePermanently={shouldShowDashboardsPageLeftPanePermanently}
    >
      <DashboardGroupSelectorView />
      <DashboardSelectorView />
    </PagePaneView>
  );
};

export default connect<Props, $Exact<{}>, _, _, _, _>(
  mapAppStateToComponentProps,
  createController
)(DashboardsPageLeftPaneView);
