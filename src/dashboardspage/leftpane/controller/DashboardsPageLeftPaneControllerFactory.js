// @flow

import { ControllerFactory } from 'oo-redux-utils';
import HidePagePaneAction from '../../../common/components/page/model/actions/panevisibility/HidePagePaneAction';

export default class DashboardsPageLeftPaneControllerFactory extends ControllerFactory {
  createController = () => ({
    hideDashboardsPageLeftPane: () =>
      this.dispatchAction(new HidePagePaneAction('dashboardsPage', 'leftPane'))
  });
}
