// @flow

import type { DashboardsState } from '../../state/DashboardsState';
import type { Dashboard } from '../../state/entities/Dashboard';
import AbstractDashboardsPageAction from '../AbstractDashboardsPageAction';

export default class ChangeSelectedDashboardAction extends AbstractDashboardsPageAction {
  +dashboard: Dashboard;

  constructor(dashboard: Dashboard) {
    super();
    this.dashboard = dashboard;
  }

  performActionAndReturnNewState(currentState: DashboardsState): DashboardsState {
    return {
      ...currentState,
      selectedDashboard: this.dashboard
    };
  }
}
