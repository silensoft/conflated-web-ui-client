// @flow

import type { DashboardsState } from '../../state/DashboardsState';
import type { DashboardGroup } from '../../state/entities/DashboardGroup';
import AbstractDashboardsPageAction from '../AbstractDashboardsPageAction';

export default class ChangeSelectedDashboardGroupAction extends AbstractDashboardsPageAction {
  +dashboardGroup: DashboardGroup;

  constructor(dashboardGroup: DashboardGroup) {
    super();
    this.dashboardGroup = dashboardGroup;
  }

  performActionAndReturnNewState(currentState: DashboardsState): DashboardsState {
    return {
      ...currentState,
      selectedDashboardGroup: this.dashboardGroup,
      selectedDashboard: this.dashboardGroup.dashboards?.[0],
      dashboardIndexShownInSlideShow: 0
    };
  }
}
