// @flow

import type { DispatchAction } from 'oo-redux-utils';
import Utils from '../../../../common/model/state/utils/Utils';
import AbstractDashboardsPageDispatchingAction from '../AbstractDashboardsPageDispatchingAction';
import type { DashboardsState } from '../../state/DashboardsState';

export default class ChangeDashboardSlideChangeIntervalAction extends AbstractDashboardsPageDispatchingAction {
  +dashboardSlideChangeIntervalInSecsStr: string;

  constructor(dispatchAction: DispatchAction, dashboardSlideChangeIntervalInSecsStr: string) {
    super(dispatchAction);
    this.dashboardSlideChangeIntervalInSecsStr = dashboardSlideChangeIntervalInSecsStr;
  }

  performActionAndReturnNewState(currentState: DashboardsState): DashboardsState {
    const newDashboardSlideChangeIntervalInSecsStr = Utils.parseIntOrDefault(
      this.dashboardSlideChangeIntervalInSecsStr,
      currentState.dashboardSlideChangeIntervalInSecsStr
    ).toString();

    return {
      ...currentState,
      dashboardSlideChangeIntervalInSecsStr: newDashboardSlideChangeIntervalInSecsStr
    };
  }
}
