// @flow

import type { DashboardsState } from '../../state/DashboardsState';
import AbstractDashboardsPageAction from '../AbstractDashboardsPageAction';
import type { Dashboard } from '../../state/entities/Dashboard';
import type { DashboardGroup } from '../../state/entities/DashboardGroup';
import Utils from '../../../../common/model/state/utils/Utils';

export default class AddDashboardToDashboardGroupAction extends AbstractDashboardsPageAction {
  +dashboard: Dashboard;

  +dashboardGroup: DashboardGroup;

  constructor(dashboard: Dashboard, dashboardGroup: DashboardGroup) {
    super();
    this.dashboard = dashboard;
    this.dashboardGroup = dashboardGroup;
  }

  performActionAndReturnNewState(currentState: DashboardsState): DashboardsState {
    const newDashboardGroup = {
      ...this.dashboardGroup,
      dashboards: [...this.dashboardGroup.dashboards, this.dashboard]
    };

    return {
      ...currentState,
      selectedDashboardGroup:
        currentState.selectedDashboardGroup === this.dashboardGroup
          ? newDashboardGroup
          : currentState.selectedDashboardGroup,
      dashboardGroups: Utils.replace(currentState.dashboardGroups, this.dashboardGroup, newDashboardGroup)
    };
  }
}
