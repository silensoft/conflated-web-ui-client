// @flow

import type { DashboardsState } from '../../state/DashboardsState';
import AbstractDashboardsPageAction from '../AbstractDashboardsPageAction';
import type { DashboardGroup } from '../../state/entities/DashboardGroup';

export default class AddDashboardGroupAction extends AbstractDashboardsPageAction {
  +dashboardGroup: DashboardGroup;

  constructor(dashboardGroup: DashboardGroup) {
    super();
    this.dashboardGroup = dashboardGroup;
  }

  performActionAndReturnNewState(currentState: DashboardsState): DashboardsState {
    return {
      ...currentState,
      dashboardGroups: [...currentState.dashboardGroups, this.dashboardGroup]
    };
  }
}
