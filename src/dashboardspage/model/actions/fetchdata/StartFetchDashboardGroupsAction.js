// @flow

import { Inject } from 'noicejs';
import type { DispatchAction } from 'oo-redux-utils';
import AbstractDashboardsPageAction from '../AbstractDashboardsPageAction';
import type { DashboardsState } from '../../state/DashboardsState';
import DashboardGroupsService from '../../service/DashboardGroupsService';
import AbstractDashboardsPageDispatchingAction from '../AbstractDashboardsPageDispatchingAction';
import type { DashboardGroup } from '../../state/entities/DashboardGroup';

type ConstructorArgs = {
  dashboardsService: DashboardGroupsService,
  dispatchAction: DispatchAction
};

export default
@Inject('dashboardsService')
class StartFetchDashboardGroupsAction extends AbstractDashboardsPageDispatchingAction {
  +dashboardsService: DashboardGroupsService;

  constructor({ dashboardsService, dispatchAction }: ConstructorArgs) {
    super(dispatchAction);
    this.dashboardsService = dashboardsService;
  }

  performActionAndReturnNewState(currentState: DashboardsState): DashboardsState {
    this.dashboardsService
      .fetchDashboardGroups()
      .then((dashboardGroups: DashboardGroup[]) =>
        this.dispatchAction(new FinishFetchDashboardGroupsAction(dashboardGroups))
      );

    return {
      ...currentState,
      isFetchingDashboardGroups: true
    };
  }
}

class FinishFetchDashboardGroupsAction extends AbstractDashboardsPageAction {
  dashboardGroups: DashboardGroup[];

  constructor(dashboardGroups: DashboardGroup[]) {
    super();
    this.dashboardGroups = dashboardGroups;
  }

  performActionAndReturnNewState(currentState: DashboardsState): DashboardsState {
    return {
      ...currentState,
      dashboardGroups: this.dashboardGroups,
      selectedDashboardGroup: this.dashboardGroups?.[0],
      selectedDashboard: this.dashboardGroups?.[0]?.dashboards?.[0],
      isFetchingDashboardGroups: false
    };
  }
}
