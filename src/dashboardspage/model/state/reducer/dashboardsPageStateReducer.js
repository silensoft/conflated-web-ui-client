// @flow

import _ from 'lodash';
import OOReduxUtils from 'oo-redux-utils';
import type { DashboardsState } from '../DashboardsState';
import AbstractDashboardsPageAction from '../../actions/AbstractDashboardsPageAction';
import AbstractDashboardsPageDispatchingAction from '../../actions/AbstractDashboardsPageDispatchingAction';
import type { DashboardGroup } from '../entities/DashboardGroup';

const initialDashboardsState: DashboardsState = {
  dashboardGroups: [],
  selectedDashboardGroup: (null: ?DashboardGroup),
  selectedDashboard: null,
  isFetchingDashboardGroups: false,
  isDashboardsSlideShowPlaying: false,
  dashboardsSlideShowIntervalId: setInterval(() => _.noop(), Number.MAX_SAFE_INTEGER),
  dashboardSlideChangeIntervalInSecsStr: '90',
  dashboardIndexShownInSlideShow: 0
};

export default OOReduxUtils.createStateReducer<DashboardsState>(initialDashboardsState, [
  AbstractDashboardsPageAction,
  AbstractDashboardsPageDispatchingAction
]);
