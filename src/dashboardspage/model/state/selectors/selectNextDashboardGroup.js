// @flow

import { createSelector } from 'reselect';
import type { AppState } from '../../../../store/AppState';
import type { DashboardGroup } from '../entities/DashboardGroup';

const dashboardGroupsSelector = (appState: AppState) =>
  appState.dashboardsPage.dashboardsState.dashboardGroups;

const currentDashboardGroupSelector = (appState: AppState) =>
  appState.dashboardsPage.dashboardsState.selectedDashboardGroup;

export default createSelector<AppState, void, ?DashboardGroup, _, _>(
  [dashboardGroupsSelector, currentDashboardGroupSelector],
  (dashboardGroups: DashboardGroup[], currentDashboardGroup: ?DashboardGroup): ?DashboardGroup => {
    if (currentDashboardGroup) {
      const currentDashboardGroupIndex = dashboardGroups.indexOf(currentDashboardGroup);
      if (currentDashboardGroupIndex < dashboardGroups.length - 1) {
        return dashboardGroups[currentDashboardGroupIndex + 1];
      }
    }

    return null;
  }
);
