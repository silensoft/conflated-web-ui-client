// @flow

import { createSelector } from 'reselect';
import type { AppState } from '../../../../store/AppState';
import type { DashboardGroup } from '../entities/DashboardGroup';
import type { Dashboard } from '../entities/Dashboard';

const currentDashboardGroupSelector = (appState: AppState) =>
  appState.dashboardsPage.dashboardsState.selectedDashboardGroup;

const currentDashboardSelector = (appState: AppState) =>
  appState.dashboardsPage.dashboardsState.selectedDashboard;

export default createSelector<AppState, void, ?Dashboard, _, _>(
  [currentDashboardGroupSelector, currentDashboardSelector],
  (currentDashboardGroup: ?DashboardGroup, currentDashboard: ?Dashboard): ?Dashboard => {
    if (currentDashboardGroup && currentDashboard) {
      const currentDashboardIndex = currentDashboardGroup.dashboards.indexOf(currentDashboard);
      if (currentDashboardIndex < currentDashboardGroup.dashboards.length - 1) {
        return currentDashboardGroup.dashboards[currentDashboardIndex + 1];
      }
    }

    return null;
  }
);
