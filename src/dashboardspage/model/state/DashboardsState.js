// @flow

import type { DashboardGroup } from './entities/DashboardGroup';
import type { Dashboard } from './entities/Dashboard';

export type DashboardsState = $Exact<{
  +dashboardGroups: DashboardGroup[],
  +selectedDashboardGroup: ?DashboardGroup,
  +selectedDashboard: ?Dashboard,
  +isFetchingDashboardGroups: boolean,
  +isDashboardsSlideShowPlaying: boolean,
  +dashboardsSlideShowIntervalId: IntervalID,
  +dashboardSlideChangeIntervalInSecsStr: string,
  +dashboardIndexShownInSlideShow: number
}>;
