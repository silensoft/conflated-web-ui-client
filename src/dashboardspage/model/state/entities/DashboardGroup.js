// @flow

import type { Dashboard } from './Dashboard';

export type DashboardGroup = $Exact<{
  +name: string,
  +dashboards: Dashboard[]
}>;
