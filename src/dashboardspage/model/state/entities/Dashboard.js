// @flow

import type { Layout } from '../../../../common/components/chartarea/model/state/types/Layout';
import type { Chart } from '../../../../common/components/chartarea/chart/model/state/Chart';

export type Dashboard = $Exact<{
  +name: string,
  +layout: Layout,
  +charts: Chart[]
}>;
