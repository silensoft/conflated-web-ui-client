// @flow

import type { DashboardGroup } from '../state/entities/DashboardGroup';

export default class DashboardGroupsService {
  // noinspection JSMethodCanBeStatic,ES6ModulesDependencies
  fetchDashboardGroups(): Promise<DashboardGroup[]> {
    throw new TypeError('Abstract method error');
  }
}
