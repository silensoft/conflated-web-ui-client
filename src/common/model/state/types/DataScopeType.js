// @flow

export type DataScopeType = 'already fetched' | 'all';
