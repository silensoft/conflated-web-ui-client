// @flow

import _ from 'lodash';
import Constants from '../../../Constants';

export default class Utils {
  static pick<T: Object>(items: T[], key: $Keys<T>, wantedValue: ?any): T[] {
    return items.filter(
      (item: T) => (wantedValue != null && item[key] === wantedValue) || (wantedValue == null && item[key])
    );
  }

  static without<T: Object>(items: T[], key: $Keys<T>, unwantedValue: any): T[] {
    return items.filter((item: T) => item[key] !== unwantedValue);
  }

  static replace<T: Object>(items: T[], oldItem: T, newItem: T): T[] {
    return items.map((item: T) => (item === oldItem ? newItem : item));
  }

  static findElem<T: Object>(items: T[], key: $Keys<T>, wantedValue: any): ?T {
    return items.find((item: T) => item[key] === wantedValue);
  }

  static findLastElem<T: Object>(items: T[], key: $Keys<T>, wantedValue: ?any): ?T {
    return _.findLast(
      items,
      (item: T) => (!wantedValue != null && item[key] === wantedValue) || (wantedValue == null && item[key])
    );
  }

  static has<T: Object>(items: T[], key: $Keys<T>, wantedValue: any): boolean {
    return !_.isUndefined(items.find((item: T) => item[key] === wantedValue));
  }

  static merge<T: Object>(items: T[], wantedItem: T, objectToMerge: $Shape<T>): T[] {
    return items.map((item: T) => (item === wantedItem ? { ...item, ...objectToMerge } : item));
  }

  static parseIntOrDefault(numberStr: string, defaultValue: number | string): number {
    const value = parseInt(numberStr, 10);
    const finalDefaultValue = typeof defaultValue === 'number' ? defaultValue : parseInt(defaultValue, 10);
    return _.isFinite(value) ? value : finalDefaultValue;
  }

  static secsToMillis(valueInSecs: number): number {
    return valueInSecs * Constants.NBR_OF_MILLISECS_IN_SEC;
  }
}
