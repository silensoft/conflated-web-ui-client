// @flow

const emptyDataSource = {
  name: '',
  jdbcDriverClass: '',
  jdbcUrl: '',
  authentication: {
    user: '',
    password: ''
  },
  sqlStatement: ''
};

export default emptyDataSource;
