// @flow
import React from 'react';
import type { Element } from 'react';
import { Input } from 'semantic-ui-react';

type Props = $Exact<{
  className: string,
  isShown: boolean,
  onChange: string => void
}>;

const SearchInputView = ({ className, isShown, onChange }: Props): Element<any> | null => {
  if (isShown) {
    return (
      <Input
        className={className}
        placeholder="Search..."
        fluid
        icon="search"
        iconPosition="left"
        size="small"
        onChange={({ currentTarget: { value } }: SyntheticEvent<HTMLInputElement>) => onChange(value)}
      />
    );
  }

  return null;
};

export default SearchInputView;
