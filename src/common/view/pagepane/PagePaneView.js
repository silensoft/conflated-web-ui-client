// @flow

import type { Element } from 'react';
import React from 'react';
import classNames from 'classnames';
import { fullScreen, left, pagePane, permanentlyVisible, right, visible } from './PagePaneView.module.scss';
import type { Pane } from '../../components/page/model/state/types/Pane';

type Props = $Exact<{
  children: Array<Element<any>>,
  hidePagePane: () => void,
  id: string,
  isFullScreenModeActive: boolean,
  pane: Pane,
  paneDefaultWidthCssVarName: string,
  paneGutterOffset: number,
  shouldShowPagePane: boolean,
  shouldShowPagePanePermanently: boolean
}>;

const PagePaneView = ({
  children,
  hidePagePane,
  id,
  isFullScreenModeActive,
  pane,
  paneDefaultWidthCssVarName,
  paneGutterOffset,
  shouldShowPagePane,
  shouldShowPagePanePermanently
}: Props): Element<any> => {
  const className = classNames(pagePane, {
    [left]: pane === 'leftPane',
    [right]: pane === 'rightPane',
    [visible]: shouldShowPagePane,
    [permanentlyVisible]: shouldShowPagePanePermanently,
    [fullScreen]: isFullScreenModeActive
  });

  const style = {
    flexBasis: `calc(var(--${paneDefaultWidthCssVarName}) ${
      pane === 'leftPane' ? '+' : '-'
    } ${paneGutterOffset}px)`,
    width: `var(--${paneDefaultWidthCssVarName})`
  };

  return (
    <aside id={id} className={className} style={style} onMouseLeave={hidePagePane} onBlur={hidePagePane}>
      {children}
    </aside>
  );
};

export default PagePaneView;
