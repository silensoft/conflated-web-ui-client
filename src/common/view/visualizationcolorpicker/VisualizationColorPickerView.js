// @flow

import type { Element } from 'react';
import React, { useMemo } from 'react';
import { Dropdown } from 'semantic-ui-react';
import { visualizationColorPicker } from './VisualizationColorPickerView.module.scss';
import type { Theme } from '../../../dataexplorerpage/settings/state/entities/Theme';

type Props = $Exact<{
  changeVisualizationColor: string => void,
  currentColor: ?string,
  theme: Theme
}>;

const VisualizationColorPickerView = ({
  changeVisualizationColor,
  currentColor,
  theme
}: Props): Element<any> => {
  const colorMenuItems = useMemo(
    (): Array<Element<any>> =>
      theme.colors.map((themeColor: string) => (
        <Dropdown.Item
          key={themeColor}
          style={{ color: themeColor }}
          icon="meanpath"
          value={themeColor}
          onClick={() => changeVisualizationColor(themeColor)}
        />
      )),
    [theme]
  );

  return (
    <Dropdown
      upward={false}
      icon=""
      className={visualizationColorPicker}
      style={{ backgroundColor: currentColor }}
    >
      <Dropdown.Menu>{colorMenuItems}</Dropdown.Menu>
    </Dropdown>
  );
};

export default VisualizationColorPickerView;
