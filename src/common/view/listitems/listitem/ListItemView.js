// @flow

import React, { useCallback } from 'react';
import type { Element } from 'react';
import { List } from 'semantic-ui-react';
import classNames from 'classnames';
import { listItem, listItemContent, listItemIcon, selected } from './ListItemView.module.scss';

export type ListItemViewProps<T: { +name: string }> = $Exact<{
  dragEventDataKey?: string,
  draggable?: boolean,
  iconClassName?: string,
  iconName?: string,
  item: T,
  onItemClick: T => void,
  selectedItem?: ?T
}>;

const ListItemView = <T: { +name: string }>({
  dragEventDataKey,
  draggable,
  iconClassName,
  iconName,
  item,
  onItemClick,
  selectedItem
}: ListItemViewProps<T>): Element<any> => {
  const onDragStart = useCallback((event: SyntheticDragEvent<HTMLDivElement>) => {
    event.dataTransfer.setData(dragEventDataKey, event.currentTarget.id);
  });

  const className = classNames(listItem, { [selected]: item.name === (selectedItem?.name ?? '') });

  let listIcon;
  if (iconName) {
    listIcon = <List.Icon className={iconClassName || listItemIcon} name={iconName} />;
  }

  return (
    <div id={item.name} draggable={draggable} onDragStart={onDragStart}>
      <List.Item className={className} onClick={() => onItemClick(item)}>
        {listIcon}
        <List.Content className={listItemContent}>{item.name}</List.Content>
      </List.Item>
    </div>
  );
};

ListItemView.defaultProps = {
  dragEventDataKey: '',
  draggable: false,
  iconClassName: '',
  iconName: '',
  selectedItem: null
};

export default ListItemView;
