// @flow

import type { Element } from 'react';
import React from 'react';
import { connect } from 'react-redux';
import type { Dispatch } from 'oo-redux-utils';
import NumberInput from 'semantic-ui-react-numberinput';
import {
  fetchedRowCount,
  label,
  numberInput,
  xAxisCategoriesShownCount
} from './DataPointsCountSelectorView.module.scss';
import type { AppState } from '../../../../store/AppState';
import DataPointsCountSelectorControllerFactory from '../controller/DataPointsCountSelectorControllerFactory';
import SelectorView from '../../selector/view/SelectorView';
import type { DataPointsCountSelectorPageStateNamespace } from '../model/state/namespace/DataPointsCountSelectorPageStateNamespace';
import Constants from '../../../Constants';
import selectorStateNamespaces from '../../selector/model/state/namespace/SelectorStateNamespace';

type OwnProps = $Exact<{ pageStateNamespace: DataPointsCountSelectorPageStateNamespace }>;

const mapAppStateToComponentProps = (appState: AppState, { pageStateNamespace }: OwnProps) => ({
  selectedChart: appState[pageStateNamespace].chartAreaState.selectedChart
});

const createController = (dispatch: Dispatch, { pageStateNamespace }: OwnProps) =>
  new DataPointsCountSelectorControllerFactory(dispatch, pageStateNamespace).createController();

type MappedState = $Call<typeof mapAppStateToComponentProps, AppState, OwnProps>;
type Controller = $Call<typeof createController, Dispatch, OwnProps>;
type Props = $Exact<{ ...OwnProps, ...MappedState, ...Controller }>;

const DataPointsCountSelectorView = ({
  changeFetchedRowCountForSelectedChart,
  changeXAxisCategoriesShownCountForSelectedChart,
  selectedChart,
  pageStateNamespace
}: Props): Element<any> => {
  let xAxisCategoriesShownCountInput;

  if (selectedChart.supportsDataPointsCount()) {
    xAxisCategoriesShownCountInput = (
      <div className={xAxisCategoriesShownCount}>
        <div className={label}>Show X-axis categories</div>
        <NumberInput
          buttonPlacement="right"
          className={numberInput}
          maxLength={5}
          maxValue={Constants.MAX_X_AXIS_CATEGORIES_SHOWN_COUNT}
          minValue={1}
          value={selectedChart.xAxisCategoriesShownCount.toString()}
          onChange={(value: string) => changeXAxisCategoriesShownCountForSelectedChart(value)}
        />
      </div>
    );
  }

  const selectorStateNamespace = `${pageStateNamespace}DataPointsCountSelector`;

  return (
    <SelectorView
      id={selectorStateNamespace}
      titleText={selectedChart.supportsDataPointsCount() ? 'SHOWN/FETCHED DATA' : 'FETCHED DATA'}
      selectorStateNamespace={selectorStateNamespaces[selectorStateNamespace]}
      selectorContent={
        <React.Fragment>
          {xAxisCategoriesShownCountInput}
          <div className={fetchedRowCount}>
            <div className={label}>Fetch database rows</div>
            <NumberInput
              buttonPlacement="right"
              className={numberInput}
              maxLength={5}
              maxValue={Constants.MAX_FETCHED_ROWS_COUNT}
              minValue={1}
              value={selectedChart.fetchedRowCount.toString()}
              onChange={(value: string) => changeFetchedRowCountForSelectedChart(value)}
            />
          </div>
        </React.Fragment>
      }
    />
  );
};

export default connect<Props, OwnProps, _, _, _, _>(
  mapAppStateToComponentProps,
  createController
)(DataPointsCountSelectorView);
