// @flow

export type DataPointsCountSelectorPageStateNamespace = 'dashboardsPage' | 'dataExplorerPage';
