// @flow

import type { GridItem } from './GridItem';

export type Layout = GridItem[];
