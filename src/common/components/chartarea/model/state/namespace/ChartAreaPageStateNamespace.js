// @flow

import type { TriggersPageStateNamespace } from '../../../../triggerspage/model/state/namespace/TriggersPageStateNamespace';

export type ChartAreaPageStateNamespace = 'dataExplorerPage' | 'dashboardsPage' | TriggersPageStateNamespace;
