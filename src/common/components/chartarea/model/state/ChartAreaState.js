// @flow

import { Chart } from '../../chart/model/state/Chart';
import type { Layout } from './types/Layout';

export type ChartAreaState = $Exact<{
  +layout: Layout,
  +selectedChart: Chart,
  +charts: Chart[],
  +previousLayout?: ?Layout,
  +copiedChart?: ?Chart
}>;
