// @flow

import AbstractChartAreaAction from '../AbstractChartAreaAction';
import type { ChartAreaState } from '../../state/ChartAreaState';
import type { Layout } from '../../state/types/Layout';
import type { ChartAreaPageStateNamespace } from '../../state/namespace/ChartAreaPageStateNamespace';

export default class ChangeChartAreaLayoutAndStorePreviousLayoutAction extends AbstractChartAreaAction {
  +layout: Layout;

  constructor(stateNamespace: ChartAreaPageStateNamespace, layout: Layout) {
    super(stateNamespace);
    this.layout = layout;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    if (this.layout.length > 0) {
      return {
        ...currentState,
        previousLayout: currentState.layout,
        layout: this.layout
      };
    } else {
      return {
        ...currentState,
        layout: currentState.previousLayout || []
      };
    }
  }
}
