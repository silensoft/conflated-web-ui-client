// @flow

import AbstractChartAreaAction from '../AbstractChartAreaAction';
import type { ChartAreaState } from '../../state/ChartAreaState';
import type { Chart } from '../../../chart/model/state/Chart';
import type { Layout } from '../../state/types/Layout';
import type { ChartAreaPageStateNamespace } from '../../state/namespace/ChartAreaPageStateNamespace';

export default class ChangeChartAreaLayoutAndChartsAction extends AbstractChartAreaAction {
  +layout: Layout;

  +charts: Chart[];

  constructor(stateNamespace: ChartAreaPageStateNamespace, layout: Layout, charts: Chart[]) {
    super(stateNamespace);
    this.layout = layout;
    this.charts = charts;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    return {
      ...currentState,
      layout: this.layout,
      charts: this.charts
    };
  }
}
