// @flow

import AbstractChartAreaAction from '../../../AbstractChartAreaAction';
import type { ChartAreaState } from '../../../../state/ChartAreaState';
import type { Chart } from '../../../../../chart/model/state/Chart';
import type { ChartAreaPageStateNamespace } from '../../../../state/namespace/ChartAreaPageStateNamespace';
import ChartAreaStateUpdater from '../../../../state/utils/ChartAreaStateUpdater';

export default class UpdateChartExportMenuCloseTimeoutIdAction extends AbstractChartAreaAction {
  +chart: Chart;

  +timeoutId: TimeoutID | 0;

  constructor(stateNamespace: ChartAreaPageStateNamespace, chart: Chart, timeoutId: TimeoutID | 0) {
    super(stateNamespace);
    this.chart = chart;
    this.timeoutId = timeoutId;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    this.chart.exportMenuCloseTimeoutID = this.timeoutId;
    return ChartAreaStateUpdater.getNewStateForChangedChart(currentState, this.chart);
  }
}
