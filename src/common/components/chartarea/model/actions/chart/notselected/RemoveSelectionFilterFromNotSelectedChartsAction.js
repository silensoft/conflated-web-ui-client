// @flow

import _ from 'lodash';
import AbstractChartAreaAction from '../../AbstractChartAreaAction';
import type { ChartAreaState } from '../../../state/ChartAreaState';
import type { Chart } from '../../../../chart/model/state/Chart';
import type { ChartAreaPageStateNamespace } from '../../../state/namespace/ChartAreaPageStateNamespace';

export default class RemoveSelectionFilterFromNotSelectedChartsAction extends AbstractChartAreaAction {
  +selectedChart: ?Chart;

  constructor(stateNamespace: ChartAreaPageStateNamespace, selectedChart: ?Chart) {
    super(stateNamespace);
    this.selectedChart = selectedChart;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    const { charts } = currentState;

    if (this.selectedChart != null) {
      return {
        ...currentState,
        charts: [
          this.selectedChart,
          ..._.without(charts, this.selectedChart).map(
            (chart: Chart): Chart => {
              if (this.selectedChart != null) {
                chart.selectedFilters.removeSelectionFilter(this.selectedChart.id);
              }

              return chart;
            }
          )
        ]
      };
    }

    return currentState;
  }
}
