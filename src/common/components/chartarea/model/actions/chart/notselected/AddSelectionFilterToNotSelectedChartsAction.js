// @flow

import _ from 'lodash';
import type { DispatchAction } from 'oo-redux-utils';
import type { ChartAreaState } from '../../../state/ChartAreaState';
import type { Chart } from '../../../../chart/model/state/Chart';
import type { ChartAreaPageStateNamespace } from '../../../state/namespace/ChartAreaPageStateNamespace';
import type { SelectedDimension } from '../../../../chart/model/state/selecteddimension/SelectedDimension';
import AbstractChartAreaDispatchingAction from '../../AbstractChartAreaDispatchingAction';

export default class AddSelectionFilterToNotSelectedChartsAction extends AbstractChartAreaDispatchingAction {
  +chart: Chart;

  +selectedDimension: SelectedDimension;

  +filterExpression: string;

  constructor(
    stateNamespace: ChartAreaPageStateNamespace,
    dispatchAction: DispatchAction,
    chart: Chart,
    selectedDimension: SelectedDimension,
    filterExpression: string
  ) {
    super(stateNamespace, dispatchAction);
    this.chart = chart;
    this.selectedDimension = selectedDimension;
    this.filterExpression = filterExpression;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    const { charts } = currentState;

    return {
      ...currentState,
      charts: [
        this.chart,
        ..._.without(charts, this.chart).map(
          (chart: Chart): Chart => {
            chart.selectedFilters.addSelectionFilter(
              this.chart.id,
              this.selectedDimension,
              this.filterExpression
            );

            return chart;
          }
        )
      ]
    };
  }
}
