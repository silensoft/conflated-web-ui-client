// @flow

import type { ChartAreaState } from '../../../state/ChartAreaState';
import type { Chart } from '../../../../chart/model/state/Chart';
import type { ChartAreaPageStateNamespace } from '../../../state/namespace/ChartAreaPageStateNamespace';
import type { DataPoint } from '../../../../chart/model/state/types/DataPoint';
import AbstractChartAreaAction from '../../AbstractChartAreaAction';
import ChartAreaStateUpdater from '../../../state/utils/ChartAreaStateUpdater';

export default class DeselectChartDataPointAction extends AbstractChartAreaAction {
  +chart: Chart;

  +dataPoint: DataPoint;

  constructor(stateNamespace: ChartAreaPageStateNamespace, chart: Chart, dataPoint: DataPoint) {
    super(stateNamespace);
    this.chart = chart;
    this.dataPoint = dataPoint;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    this.chart.deselectDataPoint(this.dataPoint);
    return ChartAreaStateUpdater.getNewStateForChangedChart(currentState, this.chart);
  }
}
