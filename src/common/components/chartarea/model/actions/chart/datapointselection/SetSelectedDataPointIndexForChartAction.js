// @flow

import type { ChartAreaState } from '../../../state/ChartAreaState';
import type { ChartAreaPageStateNamespace } from '../../../state/namespace/ChartAreaPageStateNamespace';
import type { Chart } from '../../../../chart/model/state/Chart';
import AbstractChartAreaAction from '../../AbstractChartAreaAction';
import ChartAreaStateUpdater from '../../../state/utils/ChartAreaStateUpdater';

export default class SetSelectedDataPointIndexForChartAction extends AbstractChartAreaAction {
  +chart: Chart;

  +selectedDataPointIndex: ?number;

  constructor(stateNamespace: ChartAreaPageStateNamespace, chart: Chart, selectedDataPointIndex: ?number) {
    super(stateNamespace);
    this.chart = chart;
    this.selectedDataPointIndex = selectedDataPointIndex;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    this.chart.selectedDataPointIndex = this.selectedDataPointIndex;
    return ChartAreaStateUpdater.getNewStateForChangedChart(currentState, this.chart);
  }
}
