// @flow

import AbstractChartAreaAction from '../../AbstractChartAreaAction';
import type { ChartAreaState } from '../../../state/ChartAreaState';
import type { ColumnNameToValuesMap } from '../../../../chart/model/state/chartdata/ColumnNameToValuesMap';
import type { ChartAreaPageStateNamespace } from '../../../state/namespace/ChartAreaPageStateNamespace';
import ChartAreaStateUpdater from '../../../state/utils/ChartAreaStateUpdater';
import Utils from '../../../../../../model/state/utils/Utils';

export default class FinishFetchChartDataAction extends AbstractChartAreaAction {
  +columnNameToValuesMap: ColumnNameToValuesMap;

  +chartId: string;

  constructor(
    stateNamespace: ChartAreaPageStateNamespace,
    columnNameToValuesMap: ColumnNameToValuesMap,
    chartId: string
  ) {
    super(stateNamespace);
    this.chartId = chartId;
    this.columnNameToValuesMap = columnNameToValuesMap;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    const chart = Utils.findElem(currentState.charts, 'id', this.chartId);

    if (chart) {
      chart.setChartData(this.columnNameToValuesMap);
      return ChartAreaStateUpdater.getNewStateForChangedChart(currentState, chart);
    }

    return currentState;
  }
}
