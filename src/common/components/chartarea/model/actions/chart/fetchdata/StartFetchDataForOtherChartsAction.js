// @flow

import { Inject } from 'noicejs';
import _ from 'lodash';
import type { DispatchAction } from 'oo-redux-utils';
import type { ChartAreaState } from '../../../state/ChartAreaState';
import type { Chart } from '../../../../chart/model/state/Chart';
import ChartDataService from '../../../../chart/model/service/ChartDataService';
import type { ColumnNameToValuesMap } from '../../../../chart/model/state/chartdata/ColumnNameToValuesMap';
import FinishFetchChartDataAction from './FinishFetchChartDataAction';
import type { ChartAreaPageStateNamespace } from '../../../state/namespace/ChartAreaPageStateNamespace';
import AbstractChartAreaDispatchingAction from '../../AbstractChartAreaDispatchingAction';

type ConstructorArgs = {
  chartDataService: ChartDataService,
  stateNamespace: ChartAreaPageStateNamespace,
  dispatchAction: DispatchAction,
  chart: ?Chart
};

export default
@Inject('chartDataService')
class StartFetchDataForOtherChartsAction extends AbstractChartAreaDispatchingAction {
  +chartDataService: ChartDataService;

  +chart: ?Chart;

  constructor({ chartDataService, stateNamespace, dispatchAction, chart }: ConstructorArgs) {
    super(stateNamespace, dispatchAction);
    this.chartDataService = chartDataService;
    this.chart = chart;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    const { charts } = currentState;

    _.without(charts, this.chart).forEach((chart: Chart) =>
      this.chartDataService
        .fetchChartData(
          chart.dataSource,
          chart.getColumns(),
          chart.getSelectedFilters(),
          chart.getSelectedSortBys()
        )
        .then((columnNameToValuesMap: ColumnNameToValuesMap) =>
          this.dispatchAction(
            new FinishFetchChartDataAction(this.stateNamespace, columnNameToValuesMap, chart.id)
          )
        )
    );

    return {
      ...currentState,
      charts: [
        ...(this.chart ? [this.chart] : []),
        ..._.without(charts, this.chart).map(
          (chart: Chart): Chart => {
            chart.setIsFetchingChartData(true);
            return chart;
          }
        )
      ]
    };
  }
}
