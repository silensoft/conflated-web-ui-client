// @flow

import AbstractChartAreaAction from '../../AbstractChartAreaAction';
import type { ChartAreaState } from '../../../state/ChartAreaState';
import type { Chart } from '../../../../chart/model/state/Chart';
import type { ChartAreaPageStateNamespace } from '../../../state/namespace/ChartAreaPageStateNamespace';

export default class CopyChartAction extends AbstractChartAreaAction {
  +chart: Chart;

  constructor(stateNamespace: ChartAreaPageStateNamespace, chart: Chart) {
    super(stateNamespace);
    this.chart = chart;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    return {
      ...currentState,
      copiedChart: this.chart
    };
  }
}
