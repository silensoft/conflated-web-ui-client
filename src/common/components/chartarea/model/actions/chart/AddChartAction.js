// @flow

import AbstractChartAreaAction from '../AbstractChartAreaAction';
import type { ChartAreaState } from '../../state/ChartAreaState';
import type { Chart } from '../../../chart/model/state/Chart';
import type { Layout } from '../../state/types/Layout';
import type { ChartAreaPageStateNamespace } from '../../state/namespace/ChartAreaPageStateNamespace';

export default class AddChartAction extends AbstractChartAreaAction {
  +chart: Chart;

  +layout: Layout;

  constructor(stateNamespace: ChartAreaPageStateNamespace, chart: Chart, layout: Layout) {
    super(stateNamespace);
    this.chart = chart;
    this.layout = layout;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    return {
      ...currentState,
      layout: this.layout,
      charts: [...currentState.charts, this.chart]
    };
  }
}
