// @flow

import type { ChartAreaState } from '../../../state/ChartAreaState';
import type { Chart } from '../../../../chart/model/state/Chart';
import type { SelectedDimension } from '../../../../chart/model/state/selecteddimension/SelectedDimension';
import type { ChartAreaPageStateNamespace } from '../../../state/namespace/ChartAreaPageStateNamespace';
import type { DrillDown } from '../../../../chart/model/state/types/DrillDown';
import AbstractChartAreaAction from '../../AbstractChartAreaAction';
import ChartAreaStateUpdater from '../../../state/utils/ChartAreaStateUpdater';

export default class DrillDownChartAction extends AbstractChartAreaAction {
  +chart: Chart;

  +drillDown: DrillDown;

  +newDrillDownSelectedDimension: SelectedDimension;

  constructor(
    stateNamespace: ChartAreaPageStateNamespace,
    chart: Chart,
    drillDown: DrillDown,
    newDrillDownSelectedDimension: SelectedDimension
  ) {
    super(stateNamespace);
    this.chart = chart;
    this.drillDown = drillDown;
    this.newDrillDownSelectedDimension = newDrillDownSelectedDimension;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    this.chart.drillDown(this.drillDown, this.newDrillDownSelectedDimension);
    return ChartAreaStateUpdater.getNewStateForChangedChart(currentState, this.chart);
  }
}
