// @flow

import AbstractChartAreaAction from '../../../../AbstractChartAreaAction';
import type { ChartAreaState } from '../../../../../state/ChartAreaState';
import type { SelectedMeasure } from '../../../../../../chart/model/state/selectedmeasure/SelectedMeasure';
import type { ChartAreaPageStateNamespace } from '../../../../../state/namespace/ChartAreaPageStateNamespace';
import ChartAreaStateUpdater from '../../../../../state/utils/ChartAreaStateUpdater';

export default class ChangeSelectedMeasureColorForSelectedChartAction extends AbstractChartAreaAction {
  +selectedMeasure: SelectedMeasure;

  +color: string;

  constructor(stateNamespace: ChartAreaPageStateNamespace, selectedMeasure: SelectedMeasure, color: string) {
    super(stateNamespace);
    this.selectedMeasure = selectedMeasure;
    this.color = color;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    currentState.selectedChart.changeSelectedMeasureColor(this.selectedMeasure, this.color);
    return ChartAreaStateUpdater.getNewStateForChangedChart(currentState, currentState.selectedChart);
  }
}
