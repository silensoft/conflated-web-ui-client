// @flow

import type { AggregationFunction } from '../../../../../../chart/model/state/selectedmeasure/types/AggregationFunction';
import type { SelectedFilter } from '../../../../../../chart/model/state/selectedfilters/selectedfilter/SelectedFilter';
import AbstractChartAreaAction from '../../../../AbstractChartAreaAction';
import ChartAreaStateUpdater from '../../../../../state/utils/ChartAreaStateUpdater';
import type { ChartAreaState } from '../../../../../state/ChartAreaState';
import type { ChartAreaPageStateNamespace } from '../../../../../state/namespace/ChartAreaPageStateNamespace';

export default class ChangeSelectedFilterAggregationFunctionForSelectedChartAction extends AbstractChartAreaAction {
  +selectedFilter: SelectedFilter;

  +aggregationFunction: AggregationFunction;

  constructor(
    stateNamespace: ChartAreaPageStateNamespace,
    selectedFilter: SelectedFilter,
    aggregationFunction: AggregationFunction
  ) {
    super(stateNamespace);
    this.selectedFilter = selectedFilter;
    this.aggregationFunction = aggregationFunction;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    const { selectedChart } = currentState;

    selectedChart.selectedFilters.changeSelectedFilterAggregationFunction(
      this.selectedFilter,
      this.aggregationFunction
    );

    return ChartAreaStateUpdater.getNewStateForChangedChart(currentState, selectedChart);
  }
}
