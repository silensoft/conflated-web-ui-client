// @flow

import ChartAreaStateUpdater from '../../../../state/utils/ChartAreaStateUpdater';
import type { SelectedFilter } from '../../../../../chart/model/state/selectedfilters/selectedfilter/SelectedFilter';
import AbstractChartAreaAction from '../../../AbstractChartAreaAction';
import type { ChartAreaState } from '../../../../state/ChartAreaState';
import type { ChartAreaPageStateNamespace } from '../../../../state/namespace/ChartAreaPageStateNamespace';

export default class RemoveSelectedFilterFromSelectedChartAction extends AbstractChartAreaAction {
  +selectedFilter: SelectedFilter;

  constructor(stateNamespace: ChartAreaPageStateNamespace, selectedFilter: SelectedFilter) {
    super(stateNamespace);
    this.selectedFilter = selectedFilter;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    const { selectedChart } = currentState;
    selectedChart.selectedFilters.removeSelectedFilter(this.selectedFilter);
    return ChartAreaStateUpdater.getNewStateForChangedChart(currentState, currentState.selectedChart);
  }
}
