// @flow

import AbstractChartAreaAction from '../../../../AbstractChartAreaAction';
import type { ChartAreaState } from '../../../../../state/ChartAreaState';
import type { SelectedMeasure } from '../../../../../../chart/model/state/selectedmeasure/SelectedMeasure';
import type { MeasureVisualizationType } from '../../../../../../chart/model/state/selectedmeasure/types/MeasureVisualizationType';
import type { ChartAreaPageStateNamespace } from '../../../../../state/namespace/ChartAreaPageStateNamespace';
import ChartAreaStateUpdater from '../../../../../state/utils/ChartAreaStateUpdater';

export default class ChangeSelectedMeasureVisualizationTypeAndColorForSelectedChartAction extends AbstractChartAreaAction {
  +selectedMeasure: SelectedMeasure;

  +visualizationType: MeasureVisualizationType;

  constructor(
    stateNamespace: ChartAreaPageStateNamespace,
    selectedMeasure: SelectedMeasure,
    visualizationType: MeasureVisualizationType
  ) {
    super(stateNamespace);
    this.selectedMeasure = selectedMeasure;
    this.visualizationType = visualizationType;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    const { selectedChart } = currentState;

    selectedChart.changeSelectedMeasureTypeAndColor(
      this.selectedMeasure,
      this.visualizationType,
      selectedChart.getMeasureVisualizationColorFor(this.selectedMeasure, this.visualizationType)
    );

    return ChartAreaStateUpdater.getNewStateForChangedChart(currentState, selectedChart);
  }
}
