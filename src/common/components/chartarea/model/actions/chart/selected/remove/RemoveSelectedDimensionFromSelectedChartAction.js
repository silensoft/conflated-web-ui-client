// @flow

import type { SelectedDimension } from '../../../../../chart/model/state/selecteddimension/SelectedDimension';
import AbstractChartAreaAction from '../../../AbstractChartAreaAction';
import type { ChartAreaState } from '../../../../state/ChartAreaState';
import ChartAreaStateUpdater from '../../../../state/utils/ChartAreaStateUpdater';
import type { ChartAreaPageStateNamespace } from '../../../../state/namespace/ChartAreaPageStateNamespace';

export default class RemoveSelectedDimensionFromSelectedChartAction extends AbstractChartAreaAction {
  +selectedDimension: SelectedDimension;

  constructor(stateNamespace: ChartAreaPageStateNamespace, selectedDimension: SelectedDimension) {
    super(stateNamespace);
    this.selectedDimension = selectedDimension;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    const { selectedChart } = currentState;
    selectedChart.removeSelectedDimension(this.selectedDimension);
    return ChartAreaStateUpdater.getNewStateForChangedChart(currentState, selectedChart);
  }
}
