// @flow

import AbstractChartAreaAction from '../../../../AbstractChartAreaAction';
import type { ChartAreaState } from '../../../../../state/ChartAreaState';
import type { SelectedDimension } from '../../../../../../chart/model/state/selecteddimension/SelectedDimension';
import type { ChartAreaPageStateNamespace } from '../../../../../state/namespace/ChartAreaPageStateNamespace';
import ChartAreaStateUpdater from '../../../../../state/utils/ChartAreaStateUpdater';

export default class ChangeSelectedDimensionColorForSelectedChartAction extends AbstractChartAreaAction {
  +selectedDimension: SelectedDimension;

  +color: string;

  constructor(
    stateNamespace: ChartAreaPageStateNamespace,
    selectedDimension: SelectedDimension,
    color: string
  ) {
    super(stateNamespace);
    this.color = color;
    this.selectedDimension = selectedDimension;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    const { selectedChart } = currentState;
    selectedChart.changeSelectedDimensionColor(this.selectedDimension, this.color);
    return ChartAreaStateUpdater.getNewStateForChangedChart(currentState, selectedChart);
  }
}
