// @flow

import type { DataScopeType } from '../../../../../../../../model/state/types/DataScopeType';
import type { SelectedFilter } from '../../../../../../chart/model/state/selectedfilters/selectedfilter/SelectedFilter';
import AbstractChartAreaAction from '../../../../AbstractChartAreaAction';
import type { ChartAreaState } from '../../../../../state/ChartAreaState';
import ChartAreaStateUpdater from '../../../../../state/utils/ChartAreaStateUpdater';
import type { ChartAreaPageStateNamespace } from '../../../../../state/namespace/ChartAreaPageStateNamespace';

export default class ChangeSelectedFilterDataScopeTypeForSelectedChartAction extends AbstractChartAreaAction {
  +selectedFilter: SelectedFilter;

  +dataScopeType: DataScopeType;

  constructor(
    stateNamespace: ChartAreaPageStateNamespace,
    selectedFilter: SelectedFilter,
    dataScopeType: DataScopeType
  ) {
    super(stateNamespace);
    this.selectedFilter = selectedFilter;
    this.dataScopeType = dataScopeType;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    const { selectedChart } = currentState;
    selectedChart.selectedFilters.changeSelectedFilterDataScopeType(this.selectedFilter, this.dataScopeType);
    return ChartAreaStateUpdater.getNewStateForChangedChart(currentState, selectedChart);
  }
}
