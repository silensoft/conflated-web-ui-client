// @flow

import _ from 'lodash';
import AbstractChartAreaAction from '../../../../AbstractChartAreaAction';
import type { ChartAreaState } from '../../../../../state/ChartAreaState';
import type { ChartAreaPageStateNamespace } from '../../../../../state/namespace/ChartAreaPageStateNamespace';
import type { DataSource } from '../../../../../../../../model/state/datasource/DataSource';
import ChartFactory from '../../../../../../chart/model/state/factory/ChartFactory';

export default class ChangeDataSourceForSelectedChartAction extends AbstractChartAreaAction {
  +dataSource: DataSource;

  constructor(stateNamespace: ChartAreaPageStateNamespace, dataSource: DataSource) {
    super(stateNamespace);
    this.dataSource = dataSource;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    const { charts, selectedChart } = currentState;

    const newChart = ChartFactory.createChart({
      ...selectedChart.getChartConfiguration(),
      dataSource: this.dataSource,
      selectedMeasures: [],
      selectedDimensions: [],
      selectedFilters: [],
      selectedSortBys: [],
      chartData: {},
      xAxisScrollPosition: 0,
      selectedDataPointIndex: undefined,
      drillDowns: undefined,
      currentDrillDownSelectedDimension: undefined,
      selectedDataPoints: undefined,
      isExportMenuOpen: false,
      exportMenuCloseTimeoutID: undefined,
      menuConfirmationType: undefined
    });

    return {
      ...currentState,
      charts: [..._.without(charts, selectedChart), newChart],
      selectedChart: newChart
    };
  }
}
