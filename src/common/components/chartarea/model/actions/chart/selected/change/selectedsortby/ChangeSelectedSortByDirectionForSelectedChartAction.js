// @flow

import type { SelectedSortBy } from '../../../../../../chart/model/state/selectedsortbys/selectedsortby/SelectedSortBy';
import type { SortDirection } from '../../../../../../chart/model/state/selectedsortbys/selectedsortby/types/SortDirection';
import type { ChartAreaState } from '../../../../../state/ChartAreaState';
import AbstractChartAreaAction from '../../../../AbstractChartAreaAction';
import ChartAreaStateUpdater from '../../../../../state/utils/ChartAreaStateUpdater';
import type { ChartAreaPageStateNamespace } from '../../../../../state/namespace/ChartAreaPageStateNamespace';

export default class ChangeSelectedSortByDirectionForSelectedChartAction extends AbstractChartAreaAction {
  +selectedSortBy: SelectedSortBy;

  +sortDirection: SortDirection;

  constructor(
    stateNamespace: ChartAreaPageStateNamespace,
    selectedSortBy: SelectedSortBy,
    sortDirection: SortDirection
  ) {
    super(stateNamespace);
    this.selectedSortBy = selectedSortBy;
    this.sortDirection = sortDirection;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    const { selectedChart } = currentState;
    selectedChart.selectedSortBys.changeSelectedSortByDirection(this.selectedSortBy, this.sortDirection);
    return ChartAreaStateUpdater.getNewStateForChangedChart(currentState, selectedChart);
  }
}
