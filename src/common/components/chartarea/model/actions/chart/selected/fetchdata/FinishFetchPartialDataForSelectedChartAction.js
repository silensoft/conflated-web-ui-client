// @flow

import AbstractChartAreaAction from '../../../AbstractChartAreaAction';
import type { ColumnNameToValuesMap } from '../../../../../chart/model/state/chartdata/ColumnNameToValuesMap';
import type { ChartAreaPageStateNamespace } from '../../../../state/namespace/ChartAreaPageStateNamespace';
import type { ChartAreaState } from '../../../../state/ChartAreaState';
import ChartAreaStateUpdater from '../../../../state/utils/ChartAreaStateUpdater';

export default class FinishFetchPartialDataForSelectedChartAction extends AbstractChartAreaAction {
  chartData: ColumnNameToValuesMap;

  constructor(stateNamespace: ChartAreaPageStateNamespace, chartData: ColumnNameToValuesMap) {
    super(stateNamespace);
    this.chartData = chartData;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    const { selectedChart } = currentState;
    selectedChart.isFetchingChartData = false;
    selectedChart.mergeChartData(this.chartData);
    return ChartAreaStateUpdater.getNewStateForChangedChart(currentState, selectedChart);
  }
}
