// @flow

import type { TimeSortOption } from '../../../../../../chart/model/state/selectedsortbys/selectedsortby/types/TimeSortOption';
import AbstractChartAreaAction from '../../../../AbstractChartAreaAction';
import ChartAreaStateUpdater from '../../../../../state/utils/ChartAreaStateUpdater';
import type { SortDirection } from '../../../../../../chart/model/state/selectedsortbys/selectedsortby/types/SortDirection';
import type { Dimension } from '../../../../../../../../../dataexplorerpage/leftpane/dimensionselector/model/state/entities/Dimension';
import type { ChartAreaState } from '../../../../../state/ChartAreaState';
import type { Measure } from '../../../../../../../../../dataexplorerpage/leftpane/measureselector/model/state/entities/Measure';
import type { ChartAreaPageStateNamespace } from '../../../../../state/namespace/ChartAreaPageStateNamespace';

export default class AddSortByTimeToSelectedChartAction extends AbstractChartAreaAction {
  +dimension: Dimension | Measure;

  +timeSortOption: TimeSortOption;

  +sortDirection: SortDirection;

  constructor(
    stateNamespace: ChartAreaPageStateNamespace,
    dimension: Dimension | Measure,
    timeSortOption: TimeSortOption,
    sortDirection: SortDirection
  ) {
    super(stateNamespace);
    this.timeSortOption = timeSortOption;
    this.dimension = dimension;
    this.sortDirection = sortDirection;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    const { selectedChart } = currentState;

    selectedChart.selectedSortBys.addSelectedSortByTime(
      this.dimension,
      this.timeSortOption,
      this.sortDirection
    );

    return ChartAreaStateUpdater.getNewStateForChangedChart(currentState, selectedChart);
  }
}
