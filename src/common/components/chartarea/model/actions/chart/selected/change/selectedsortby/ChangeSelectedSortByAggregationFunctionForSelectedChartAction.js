// @flow

import type { SelectedSortBy } from '../../../../../../chart/model/state/selectedsortbys/selectedsortby/SelectedSortBy';
import type { AggregationFunction } from '../../../../../../chart/model/state/selectedmeasure/types/AggregationFunction';
import type { ChartAreaState } from '../../../../../state/ChartAreaState';
import AbstractChartAreaAction from '../../../../AbstractChartAreaAction';
import ChartAreaStateUpdater from '../../../../../state/utils/ChartAreaStateUpdater';
import type { ChartAreaPageStateNamespace } from '../../../../../state/namespace/ChartAreaPageStateNamespace';

export default class ChangeSelectedSortByAggregationFunctionForSelectedChartAction extends AbstractChartAreaAction {
  +selectedSortBy: SelectedSortBy;

  +aggregationFunction: AggregationFunction;

  constructor(
    stateNamespace: ChartAreaPageStateNamespace,
    selectedSortBy: SelectedSortBy,
    aggregationFunction: AggregationFunction
  ) {
    super(stateNamespace);
    this.selectedSortBy = selectedSortBy;
    this.aggregationFunction = aggregationFunction;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    const { selectedChart } = currentState;

    selectedChart.selectedSortBys.changeSelectedSortByAggregationFunction(
      this.selectedSortBy,
      this.aggregationFunction
    );

    return ChartAreaStateUpdater.getNewStateForChangedChart(currentState, currentState.selectedChart);
  }
}
