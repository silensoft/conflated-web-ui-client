// @flow

import type { FilterInputType } from '../../../../../../chart/model/state/selectedfilters/selectedfilter/types/FilterInputType';
import ChartAreaStateUpdater from '../../../../../state/utils/ChartAreaStateUpdater';
import type { SelectedFilter } from '../../../../../../chart/model/state/selectedfilters/selectedfilter/SelectedFilter';
import AbstractChartAreaAction from '../../../../AbstractChartAreaAction';
import type { ChartAreaState } from '../../../../../state/ChartAreaState';
import type { ChartAreaPageStateNamespace } from '../../../../../state/namespace/ChartAreaPageStateNamespace';

export default class ChangeSelectedFilterInputTypeForSelectedChartAction extends AbstractChartAreaAction {
  +selectedFilter: SelectedFilter;

  +filterInputType: FilterInputType;

  constructor(
    stateNamespace: ChartAreaPageStateNamespace,
    selectedFilter: SelectedFilter,
    filterInputType: FilterInputType
  ) {
    super(stateNamespace);
    this.selectedFilter = selectedFilter;
    this.filterInputType = filterInputType;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    const { selectedChart } = currentState;
    selectedChart.selectedFilters.changeSelectedFilterInputType(this.selectedFilter, this.filterInputType);
    return ChartAreaStateUpdater.getNewStateForChangedChart(currentState, selectedChart);
  }
}
