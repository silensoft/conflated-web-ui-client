// @flow

import { Inject } from 'noicejs';
import type { DispatchAction } from 'oo-redux-utils';
import type { ChartAreaState } from '../../../../state/ChartAreaState';
import ChartDataService from '../../../../../chart/model/service/ChartDataService';
import type { ChartAreaPageStateNamespace } from '../../../../state/namespace/ChartAreaPageStateNamespace';
import AbstractChartAreaDispatchingAction from '../../../AbstractChartAreaDispatchingAction';
import StartFetchDataForSelectedChartAction from './StartFetchDataForSelectedChartAction';
import StartFetchMeasureFilterMinAndMaxValuesForSelectedChartAction from './StartFetchMeasureFilterMinAndMaxValuesForSelectedChartAction';
import StartFetchValuesForDimensionsUsedInFiltersInSelectedChartAction from './StartFetchValuesForDimensionsUsedInFiltersInSelectedChartAction';
import type { SelectedFilter } from '../../../../../chart/model/state/selectedfilters/selectedfilter/SelectedFilter';

type ConstructorArgs = {
  chartDataService: ChartDataService,
  dispatchAction: DispatchAction,
  stateNamespace: ChartAreaPageStateNamespace,
  selectedFilter: SelectedFilter
};

export default
@Inject('chartDataService')
class StartFetchDataForChangedFilterInSelectedChartAction extends AbstractChartAreaDispatchingAction {
  +chartDataService: ChartDataService;

  +selectedFilter: SelectedFilter;

  constructor({ chartDataService, dispatchAction, stateNamespace, selectedFilter }: ConstructorArgs) {
    super(stateNamespace, dispatchAction);
    this.chartDataService = chartDataService;
    this.selectedFilter = selectedFilter;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    const { selectedChart } = currentState;
    let ActionClass;

    if (this.selectedFilter.shouldFetchMeasureMinMaxValues(selectedChart.chartData)) {
      ActionClass = StartFetchMeasureFilterMinAndMaxValuesForSelectedChartAction;
    } else if (this.selectedFilter.shouldFetchChartData(selectedChart.chartData)) {
      ActionClass = StartFetchDataForSelectedChartAction;
    } else if (this.selectedFilter.shouldFetchDimensionValues(selectedChart.chartData)) {
      ActionClass = StartFetchValuesForDimensionsUsedInFiltersInSelectedChartAction;
    }

    if (ActionClass) {
      return this.performAction(
        new ActionClass({
          chartDataService: this.chartDataService,
          dispatchAction: this.dispatchAction,
          stateNamespace: this.stateNamespace
        }),
        currentState
      );
    }

    return currentState;
  }
}
