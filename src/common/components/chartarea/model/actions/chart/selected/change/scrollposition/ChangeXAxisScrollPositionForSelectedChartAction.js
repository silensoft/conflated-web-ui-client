// @flow

import AbstractChartAreaAction from '../../../../AbstractChartAreaAction';
import type { ChartAreaState } from '../../../../../state/ChartAreaState';
import type { ChartAreaPageStateNamespace } from '../../../../../state/namespace/ChartAreaPageStateNamespace';
import ChartAreaStateUpdater from '../../../../../state/utils/ChartAreaStateUpdater';

export default class ChangeXAxisScrollPositionForSelectedChartAction extends AbstractChartAreaAction {
  +xAxisScrollPosition: number;

  constructor(stateNamespace: ChartAreaPageStateNamespace, xAxisScrollPosition: number) {
    super(stateNamespace);
    this.xAxisScrollPosition = xAxisScrollPosition;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    const { selectedChart } = currentState;
    selectedChart.xAxisScrollPosition = this.xAxisScrollPosition;
    return ChartAreaStateUpdater.getNewStateForChangedChart(currentState, selectedChart);
  }
}
