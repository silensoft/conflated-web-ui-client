// @flow

import type { Dimension } from '../../../../../../../../../dataexplorerpage/leftpane/dimensionselector/model/state/entities/Dimension';
import type { Measure } from '../../../../../../../../../dataexplorerpage/leftpane/measureselector/model/state/entities/Measure';
import type { SelectedSortByType } from '../../../../../../chart/model/state/selectedsortbys/selectedsortby/types/SelectedfSortByType';
import ChartAreaStateUpdater from '../../../../../state/utils/ChartAreaStateUpdater';
import type { SortDirection } from '../../../../../../chart/model/state/selectedsortbys/selectedsortby/types/SortDirection';
import type { ChartAreaState } from '../../../../../state/ChartAreaState';
import AbstractChartAreaAction from '../../../../AbstractChartAreaAction';
import type { ChartAreaPageStateNamespace } from '../../../../../state/namespace/ChartAreaPageStateNamespace';

export default class AddSortByToSelectedChartAction extends AbstractChartAreaAction {
  +measureOrDimension: Dimension | Measure;

  +type: SelectedSortByType;

  +sortDirection: SortDirection;

  constructor(
    stateNamespace: ChartAreaPageStateNamespace,
    measureOrDimension: Dimension | Measure,
    type: SelectedSortByType,
    sortDirection: SortDirection
  ) {
    super(stateNamespace);
    this.measureOrDimension = measureOrDimension;
    this.type = type;
    this.sortDirection = sortDirection;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    const { selectedChart } = currentState;
    selectedChart.selectedSortBys.addSelectedSortBy(this.measureOrDimension, this.type, this.sortDirection);
    return ChartAreaStateUpdater.getNewStateForChangedChart(currentState, selectedChart);
  }
}
