// @flow

import _ from 'lodash';
import AbstractChartAreaAction from '../../../../AbstractChartAreaAction';
import type { ChartAreaState } from '../../../../../state/ChartAreaState';
import type { ChartType } from '../../../../../../chart/model/state/types/ChartType';
import type { ChartAreaPageStateNamespace } from '../../../../../state/namespace/ChartAreaPageStateNamespace';

export default class ChangeChartTypeForSelectedChartAction extends AbstractChartAreaAction {
  +chartType: ChartType;

  constructor(stateNamespace: ChartAreaPageStateNamespace, chartType: ChartType) {
    super(stateNamespace);
    this.chartType = chartType;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    const { charts, selectedChart } = currentState;

    const newChartType =
      selectedChart.chartType === 'boxplot' && this.chartType === 'boxplot' ? 'violin' : this.chartType;

    const newChart = selectedChart.getNewChartOfType(newChartType);

    return {
      ...currentState,
      charts: [..._.without(charts, selectedChart), newChart],
      selectedChart: newChart
    };
  }
}
