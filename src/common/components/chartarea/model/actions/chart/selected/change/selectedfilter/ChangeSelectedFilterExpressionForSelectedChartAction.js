// @flow

import type { SelectedFilter } from '../../../../../../chart/model/state/selectedfilters/selectedfilter/SelectedFilter';
import type { ChartAreaState } from '../../../../../state/ChartAreaState';
import ChartAreaStateUpdater from '../../../../../state/utils/ChartAreaStateUpdater';
import AbstractChartAreaAction from '../../../../AbstractChartAreaAction';
import type { ChartAreaPageStateNamespace } from '../../../../../state/namespace/ChartAreaPageStateNamespace';

export default class ChangeSelectedFilterExpressionForSelectedChartAction extends AbstractChartAreaAction {
  +selectedFilter: SelectedFilter;

  +expression: string;

  constructor(
    stateNamespace: ChartAreaPageStateNamespace,
    selectedFilter: SelectedFilter,
    expression: string
  ) {
    super(stateNamespace);
    this.selectedFilter = selectedFilter;
    this.expression = expression;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    const { selectedChart } = currentState;
    selectedChart.selectedFilters.changeSelectedFilterExpression(this.selectedFilter, this.expression);
    return ChartAreaStateUpdater.getNewStateForChangedChart(currentState, currentState.selectedChart);
  }
}
