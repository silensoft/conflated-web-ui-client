// @flow

import AbstractChartAreaAction from '../AbstractChartAreaAction';
import type { ChartAreaPageStateNamespace } from '../../state/namespace/ChartAreaPageStateNamespace';
import type { Chart } from '../../../chart/model/state/Chart';
import ClearChartAction from './ClearChartAction';
import DeleteChartAction from './RemoveChartAction';
import type { ChartAreaState } from '../../state/ChartAreaState';
import HideChartMenuClearOrDeleteConfirmationAction from './menu/confirm/HideChartMenuClearOrDeleteConfirmationAction';

export default class ClearOrRemoveChartAction extends AbstractChartAreaAction {
  +chart: Chart;

  constructor(stateNamespace: ChartAreaPageStateNamespace, chart: Chart) {
    super(stateNamespace);
    this.chart = chart;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    let newState;

    if (this.chart.menuConfirmationType === 'clear') {
      newState = this.performAction(new ClearChartAction(this.stateNamespace, this.chart), currentState);
    } else {
      newState = this.performAction(new DeleteChartAction(this.stateNamespace, this.chart), currentState);
    }

    return this.performAction(new HideChartMenuClearOrDeleteConfirmationAction(this.stateNamespace, this.chart), newState);
  }
}
