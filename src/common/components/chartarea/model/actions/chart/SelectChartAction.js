// @flow

import AbstractChartAreaAction from '../AbstractChartAreaAction';
import type { ChartAreaState } from '../../state/ChartAreaState';
import type { ChartAreaPageStateNamespace } from '../../state/namespace/ChartAreaPageStateNamespace';
import type { Chart } from '../../../chart/model/state/Chart';

export default class SelectChartAction extends AbstractChartAreaAction {
  +chart: Chart;

  constructor(stateNamespace: ChartAreaPageStateNamespace, chart: Chart) {
    super(stateNamespace);
    this.chart = chart;
  }

  performActionAndReturnNewState(currentState: ChartAreaState): ChartAreaState {
    return {
      ...currentState,
      selectedChart: this.chart
    };
  }
}
