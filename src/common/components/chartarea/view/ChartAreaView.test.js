import React from 'react';
import { shallow as renderShallow } from 'enzyme';
import ChartAreaView from './ChartAreaView';

test('ChartArea should render correctly', () => {
  const renderedChartArea = renderShallow(<ChartAreaView />);
  expect(renderedChartArea).toMatchSnapshot();
});
