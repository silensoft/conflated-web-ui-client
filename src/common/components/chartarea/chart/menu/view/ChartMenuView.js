// @flow

import React, { useCallback } from 'react';
import type { Element } from 'react';
import { connect } from 'react-redux';
import type { Dispatch } from 'oo-redux-utils';
import _ from 'lodash';
import { Button, Dropdown, Icon } from 'semantic-ui-react';
import {
  buttonsArea,
  dangerItem,
  keyboardShortcut,
  menuItemWithKeyboardShortcut,
  selectedChart
} from '../../view/ChartView.module.scss';
import type { ChartAreaPageStateNamespace } from '../../../model/state/namespace/ChartAreaPageStateNamespace';
import ChartMenuControllerFactory from '../controller/ChartMenuControllerFactory';
import type { Chart } from '../../model/state/Chart';

type OwnProps = $Exact<{ chart: Chart, className: string, pageStateNamespace: ChartAreaPageStateNamespace }>;

const mapAppStateToComponentProps = () => ({});
const createController = (dispatch: Dispatch, { pageStateNamespace }: OwnProps) =>
  new ChartMenuControllerFactory(dispatch, pageStateNamespace).createController();

type Controller = $Call<typeof createController, Dispatch, OwnProps>;
type Props = $Exact<{ ...OwnProps, ...Controller }>;

const ChartMenuView = ({
  allowChartMenuToBeOpened,
  chart,
  className,
  clearChart,
  clearOrRemoveChart,
  closeChartExportMenu,
  copyChart,
  hideChartMenuActionConfirmation,
  openChartExportMenu,
  pasteChart,
  showClearChartConfirmationInChartMenu,
  showDeleteChartConfirmationInChartMenu,
  updateChartExportMenuCloseTimeoutId
}: Props): Element<any> => {
  const handleEnterMenuExportItem = useCallback(
    () => {
      if (!chart.isExportMenuOpen) {
        openChartExportMenu(chart);
      }
    },
    [chart]
  );

  const handleLeaveMenuExportItem = useCallback(
    () => {
      const timeoutID = setTimeout(() => {
        closeChartExportMenu(chart);
      }, 200);

      updateChartExportMenuCloseTimeoutId(chart, timeoutID);
    },
    [chart]
  );

  const cutChart = useCallback(
    () => {
      copyChart(selectedChart);
      clearChart(selectedChart);
    },
    [chart]
  );

  const handleCancelButtonClick = useCallback(
    () => {
      hideChartMenuActionConfirmation(chart);

      setTimeout(() => {
        allowChartMenuToBeOpened(chart);
      }, 0);
    },
    [chart]
  );

  let hasConfirmAction;
  if (chart.menuConfirmationType === 'none') {
    hasConfirmAction = false;
  } else if (chart.menuConfirmationType) {
    hasConfirmAction = true;
  }

  return (
    <div className={className}>
      <Dropdown
        open={hasConfirmAction}
        size="large"
        floating
        direction="left"
        icon={<Icon name="bars" size="large" />}
      >
        <Dropdown.Menu>
          <Dropdown.Item
            className={menuItemWithKeyboardShortcut}
            disabled={hasConfirmAction}
            value="copy"
            onClick={() => copyChart(chart)}
          >
            <span>Copy</span>
            <span className={keyboardShortcut}>Ctrl-C</span>
          </Dropdown.Item>
          <Dropdown.Item
            className={menuItemWithKeyboardShortcut}
            disabled={hasConfirmAction}
            value="paste"
            onClick={() => pasteChart(chart)}
          >
            <span>Paste</span>
            <span className={keyboardShortcut}>Ctrl-V</span>
          </Dropdown.Item>
          <Dropdown.Item
            className={menuItemWithKeyboardShortcut}
            disabled={hasConfirmAction}
            value="cut"
            onClick={cutChart}
          >
            <span>Cut</span>
            <span className={keyboardShortcut}>Ctrl-X</span>
          </Dropdown.Item>
          <Dropdown.Divider />
          <Dropdown
            disabled={hasConfirmAction}
            open={chart.isExportMenuOpen}
            icon=""
            text="Export as"
            pointing="right"
            item
            onMouseEnter={handleEnterMenuExportItem}
            onMouseLeave={handleLeaveMenuExportItem}
            onMouseMove={handleEnterMenuExportItem}
          >
            <Dropdown.Menu onMouseEnter={handleEnterMenuExportItem} onMouseLeave={handleLeaveMenuExportItem}>
              <Dropdown.Item text="PNG" value="png" onClick={chart.exportToPng} />
              <Dropdown.Item text="PDF" value="pdf" onClick={chart.exportToPdf} />
              <Dropdown.Item text="SVG" value="svg" onClick={chart.exportToSvg} />
            </Dropdown.Menu>
          </Dropdown>
          <Dropdown.Divider />
          <Dropdown.Item
            disabled={chart.menuConfirmationType === 'delete'}
            className={chart.menuConfirmationType === 'clear' ? dangerItem : ''}
            text={`Clear chart${chart.menuConfirmationType === 'clear' ? '?' : ''}`}
            value="clear"
            onClick={() => showClearChartConfirmationInChartMenu(chart)}
          />
          <Dropdown.Item
            disabled={chart.menuConfirmationType === 'clear'}
            className={`${menuItemWithKeyboardShortcut} ${
              chart.menuConfirmationType === 'delete' ? dangerItem : ''
            }`}
            value="delete"
            onClick={() => showDeleteChartConfirmationInChartMenu(chart)}
          >
            <span>{`Delete chart${chart.menuConfirmationType === 'delete' ? '?' : ''}`}</span>
            <span className={keyboardShortcut}>Del</span>
          </Dropdown.Item>
          {chart.menuConfirmationType ? (
            <Dropdown.Item className={buttonsArea}>
              <Button size="tiny" color="red" onClick={clearOrRemoveChart(chart)}>
                {chart.menuConfirmationType.toUpperCase()}
              </Button>
              <Button size="tiny" secondary onClick={handleCancelButtonClick}>
                CANCEL
              </Button>
            </Dropdown.Item>
          ) : (
            undefined
          )}
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );
};

export default connect<Props, OwnProps, _, _, _, _>(
  mapAppStateToComponentProps,
  createController
)(ChartMenuView);
