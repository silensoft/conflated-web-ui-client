// @flow

import type { Element } from 'react';
import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import type { Dispatch } from 'oo-redux-utils';
import { Icon } from 'semantic-ui-react';
import { drillUpIcon } from './DrillUpIconView.module.scss';
import type { ChartAreaPageStateNamespace } from '../../../model/state/namespace/ChartAreaPageStateNamespace';
import DrillUpIconControllerFactory from '../controller/DrillUpIconControllerFactory';
import type { Chart } from '../../model/state/Chart';

type OwnProps = $Exact<{ chart: Chart, pageStateNamespace: ChartAreaPageStateNamespace }>;

const mapAppStateToComponentProps = () => ({});
const createController = (dispatch: Dispatch, { pageStateNamespace }: OwnProps) =>
  new DrillUpIconControllerFactory(dispatch, pageStateNamespace).createController();

type Controller = $Call<typeof createController, Dispatch, OwnProps>;
type Props = $Exact<{ ...OwnProps, ...Controller }>;

const DrillUpIconView = ({ chart, drillUpChart }: Props): Element<any> | null =>
  chart.drillDowns.length > 0 ? (
    <div className={drillUpIcon}>
      <Icon name="arrow alternate circle left outline" size="large" onClick={drillUpChart(chart)} />
    </div>
  ) : null;

export default connect<Props, OwnProps, _, _, _, _>(
  mapAppStateToComponentProps,
  createController
)(DrillUpIconView);
