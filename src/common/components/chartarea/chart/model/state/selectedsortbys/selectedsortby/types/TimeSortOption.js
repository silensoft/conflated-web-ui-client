// @flow

export type TimeSortOption =
  | 'none'
  | 'Latest value'
  | 'Latest increase'
  | 'Latest decrease'
  | 'Historical minimum'
  | 'Historical maximum'
  | 'Historical average'
  | 'Historical median'
  | 'Historical 5th percentile'
  | 'Historical 25th percentile'
  | 'Historical 75th percentile'
  | 'Historical 95th percentile'
  | 'Historical increase'
  | 'Historical decrease'
  | 'Historical max increase'
  | 'Historical max decrease'
  | 'Instantaneous increase in history'
  | 'Instantaneous decrease in history';
