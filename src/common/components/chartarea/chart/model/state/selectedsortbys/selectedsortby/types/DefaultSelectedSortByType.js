// @flow

export type DefaultSelectedSortByType = 'none' | 'x-axis categories' | 'legend' | 'measure' | 'measure over legend';
