// @flow

export type SelectedSortByType = 'measure' | 'dimension' | 'time';
