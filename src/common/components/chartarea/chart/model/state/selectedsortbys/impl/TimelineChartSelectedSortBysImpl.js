// @flow

import BasicChartSelectedSortBysImpl from './BasicChartSelectedSortBysImpl';
import type { Dimension } from '../../../../../../../../dataexplorerpage/leftpane/dimensionselector/model/state/entities/Dimension';
import type { Measure } from '../../../../../../../../dataexplorerpage/leftpane/measureselector/model/state/entities/Measure';
import type { DimensionVisualizationType } from '../../selecteddimension/types/DimensionVisualizationType';
import type { Chart } from '../../Chart';

export default class TimelineChartSelectedSortBysImpl extends BasicChartSelectedSortBysImpl {
  updateSelectedSortBysWhenAddingSelectedDimension(
    measureOrDimension: Dimension | Measure,
    // eslint-disable-next-line no-unused-vars
    visualizationType: DimensionVisualizationType,
    // eslint-disable-next-line no-unused-vars
    chart: Chart
  ) {
    this.selectedSortBys = [];
    this.addSelectedSortBy(measureOrDimension, 'dimension', 'ASC', 'x-axis categories');
  }
}
