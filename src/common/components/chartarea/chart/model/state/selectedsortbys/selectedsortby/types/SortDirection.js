// @flow

export type SortDirection = 'ASC' | 'DESC';
