// @flow

import type { SelectedSortBy } from './selectedsortby/SelectedSortBy';
import type { DefaultSelectedSortByType } from './selectedsortby/types/DefaultSelectedSortByType';
import type { Measure } from '../../../../../../../dataexplorerpage/leftpane/measureselector/model/state/entities/Measure';
import type { Dimension } from '../../../../../../../dataexplorerpage/leftpane/dimensionselector/model/state/entities/Dimension';
import type { SelectedSortByType } from './selectedsortby/types/SelectedfSortByType';
import type { SortDirection } from './selectedsortby/types/SortDirection';
import type { TimeSortOption } from './selectedsortby/types/TimeSortOption';
import type { DataScopeType } from '../../../../../../model/state/types/DataScopeType';
import type { SelectedMeasure } from '../selectedmeasure/SelectedMeasure';
import type { AggregationFunction } from '../selectedmeasure/types/AggregationFunction';
import type { SelectedDimension } from '../selecteddimension/SelectedDimension';
import type { DimensionVisualizationType } from '../selecteddimension/types/DimensionVisualizationType';
import type { Chart } from '../Chart';

export interface SelectedSortBys {
  addSelectedSortBy(
    measureOrDimension: Measure | Dimension,
    type: SelectedSortByType,
    sortDirection: SortDirection,
    defaultSortByType?: DefaultSelectedSortByType,
    aggregationFunction?: AggregationFunction
  ): ?SelectedSortBy;

  addSelectedSortByAverageOfMeasures(selectedMeasures: SelectedMeasure[]): ?SelectedSortBy;

  addSelectedSortByMeasureOverLegendPartitionedByXAxisCategories(
    dimension: Dimension | Measure,
    xAxisCategoriesSelectedDimension: SelectedDimension
  ): ?SelectedSortBy;

  addSelectedSortByTime(
    dimension: Dimension | Measure,
    timeSortOption: TimeSortOption,
    sortDirection: SortDirection
  ): ?SelectedSortBy;

  changeSelectedSortByAggregationFunction(
    selectedSortBy: SelectedSortBy,
    aggregationFunction: AggregationFunction
  ): void;

  changeSelectedSortByDataScopeType(selectedSortBy: SelectedSortBy, dataScopeType: DataScopeType): void;

  changeSelectedSortByDirection(selectedSortBy: SelectedSortBy, sortDirection: SortDirection): void;

  getConvertSelectedSortBys(selectedDimensions: SelectedDimension[]): SelectedSortBy[];

  getDefaultOfType(defaultType: DefaultSelectedSortByType): ?SelectedSortBy;

  getSelectedSortBys(): SelectedSortBy[];

  removeSelectedSortBy(selectedSortBy: SelectedSortBy): void;

  updateSelectedSortBysWhenAddingSelectedDimension(
    measureOrDimension: Dimension | Measure,
    visualizationType: DimensionVisualizationType,
    chart: Chart
  ): void;

  updateSelectedSortBysWhenAddingSelectedMeasure(
    measureOrDimension: Measure | Dimension,
    selectedMeasures: SelectedMeasure[]
  ): void;

  updateSelectedSortBysWhenChangingSelectedMeasureAggregationFunction(
    aggregationFunction: AggregationFunction,
    selectedMeasures: SelectedMeasure[]
  ): void;

  updateSelectedSortBysWhenRemovingSelectedDimension(
    selectedDimension: SelectedDimension,
    selectedMeasures: SelectedMeasure[]
  ): void;

  updateSelectedSortBysWhenRemovingSelectedMeasure(
    selectedMeasure: SelectedMeasure,
    selectedMeasures: SelectedMeasure[]
  ): void;
}
