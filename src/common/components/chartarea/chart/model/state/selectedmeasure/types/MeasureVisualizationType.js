// @flow

export type MeasureVisualizationType =
  | 'none'
  | 'column'
  | 'area'
  | 'line'
  | 'radius'
  | 'color'
  | 'tooltip'
  | 'text'
  | 'x-axis'
  | 'y-axis'
  | 'open'
  | 'high'
  | 'low'
  | 'close';
