// @flow

export type AggregationFunction =
  | 'NONE'
  | 'SUM'
  | 'AVG'
  | 'COUNT'
  | 'DISTINCT'
  | 'MIN'
  | 'MAX'
  | 'STDDEV'
  | 'VAR';
