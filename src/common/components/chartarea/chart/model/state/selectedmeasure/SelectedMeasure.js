// @flow

import type { Dimension } from '../../../../../../../dataexplorerpage/leftpane/dimensionselector/model/state/entities/Dimension';
import type { AggregationFunction } from './types/AggregationFunction';
import type { MeasureVisualizationType } from './types/MeasureVisualizationType';
import type { Measure } from '../../../../../../../dataexplorerpage/leftpane/measureselector/model/state/entities/Measure';

export type SelectedMeasure = {
  +measure: Measure | Dimension,
  +sqlColumn: {
    +name: string,
    +expression: string
  },
  +aggregationFunction: AggregationFunction,
  +visualizationType: MeasureVisualizationType,
  +visualizationColor: string
};
