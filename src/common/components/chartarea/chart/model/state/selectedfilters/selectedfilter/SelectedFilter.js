// @flow

import type { Element } from 'react';
import type { SelectedFilterConfiguration } from './SelectedFilterConfiguration';
import type { ColumnNameToValuesMap } from '../../chartdata/ColumnNameToValuesMap';
import type { ChartData } from '../../chartdata/ChartData';
import type { FilterInputType } from './types/FilterInputType';
import type { AggregationFunction } from '../../selectedmeasure/types/AggregationFunction';
import type { DataScopeType } from '../../../../../../../model/state/types/DataScopeType';
import type { Measure } from '../../../../../../../../dataexplorerpage/leftpane/measureselector/model/state/entities/Measure';
import type { Dimension } from '../../../../../../../../dataexplorerpage/leftpane/dimensionselector/model/state/entities/Dimension';
import type { FilterType } from './types/FilterType';

export interface SelectedFilter {
  +allowedDimensionFilterInputTypes: FilterInputType[];
  +aggregationFunction: AggregationFunction;
  +chartId: string;
  +dataScopeType: DataScopeType;
  +filterExpression: string;
  +filterInputType: FilterInputType;
  +isDrillDownFilter: boolean;
  +isSelectionFilter: boolean;
  +measureOrDimension: Measure | Dimension;
  +sqlColumn: {
    +name: string,
    +expression: string
  };
  +type: FilterType;

  getConfiguration(): SelectedFilterConfiguration;
  applyFilter(chartData: ColumnNameToValuesMap): ColumnNameToValuesMap;
  getFilterInputView(
    className: string,
    chartData: ChartData,
    changeFilterExpression: (string) => void
  ): Element<any>;

  shouldFetchChartData(chartData: ChartData): boolean;
  shouldFetchDimensionValues(chartData: ChartData): boolean;
  shouldFetchMeasureMinMaxValues(chartData: ChartData): boolean;
}
