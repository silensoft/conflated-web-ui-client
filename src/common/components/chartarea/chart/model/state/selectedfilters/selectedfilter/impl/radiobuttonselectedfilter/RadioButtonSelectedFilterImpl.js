// @flow

import type { Element } from 'react';
import React from 'react';
import AbstractSelectedFilterImpl from '../AbstractSelectedFilterImpl';
import type { ColumnNameToValuesMap } from '../../../../chartdata/ColumnNameToValuesMap';
import type { ChartData } from '../../../../chartdata/ChartData';
import RadioButtonsFilterInputView from '../../../../../../../../filterselector/view/selectedfilter/filterinput/radiobuttons/RadioButtonsFilterInputView';

export default class RadioButtonSelectedFilterImpl extends AbstractSelectedFilterImpl {
  applyFilter(chartData: ColumnNameToValuesMap): ColumnNameToValuesMap {
    const filteredInIndexes = [];
    const newChartData = chartData;

    if (this.filterExpression && chartData[this.sqlColumn.name]) {
      newChartData[this.sqlColumn.name] = chartData[this.sqlColumn.name].filter(
        (chartDataValue: string, index: number): boolean => {
          const chartDataValueIsSameAsFilterExpression = chartDataValue === this.filterExpression;
          if (chartDataValueIsSameAsFilterExpression) {
            filteredInIndexes.push(index);
          }
          return chartDataValueIsSameAsFilterExpression;
        }
      );
    }

    return this.filterChartDataOtherColumns(newChartData, filteredInIndexes);
  }

  getFilterInputView(
    className: string,
    chartData: ChartData,
    changeFilterExpression: string => void
  ): Element<any> {
    return (
      <RadioButtonsFilterInputView
        changeFilterExpression={changeFilterExpression}
        chartData={chartData}
        className={className}
        selectedFilter={this}
      />
    );
  }
}
