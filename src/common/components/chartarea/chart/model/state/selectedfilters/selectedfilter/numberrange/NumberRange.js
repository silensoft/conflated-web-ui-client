// @flow

export type NumberRange = {
  +startValue: number,
  +endValue: number
};
