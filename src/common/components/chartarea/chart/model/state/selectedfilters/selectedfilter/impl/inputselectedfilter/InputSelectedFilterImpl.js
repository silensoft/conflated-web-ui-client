// @flow

import type { Element } from 'react';
import React from 'react';
import type { ChartData } from '../../../../chartdata/ChartData';
import InputFilterInputView from '../../../../../../../../filterselector/view/selectedfilter/filterinput/input/InputFilterInputView';
import AbstractSelectedFilterImpl from '../AbstractSelectedFilterImpl';

export default class InputSelectedFilterImpl extends AbstractSelectedFilterImpl {
  getFilterInputView(
    className: string,
    chartData: ChartData,
    changeFilterExpression: string => void
  ): Element<any> {
    return (
      <InputFilterInputView
        changeFilterExpression={changeFilterExpression}
        className={className}
        filterExpression={this.filterExpression}
        isSelectionFilter={this.isSelectionFilter}
      />
    );
  }
}
