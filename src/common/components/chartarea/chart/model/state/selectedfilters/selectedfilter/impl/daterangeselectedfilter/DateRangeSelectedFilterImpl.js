// @flow

import moment from 'moment';
import AbstractSelectedFilterImpl from '../AbstractSelectedFilterImpl';
import type { ColumnNameToValuesMap } from '../../../../chartdata/ColumnNameToValuesMap';

export default class DateRangeSelectedFilterImpl extends AbstractSelectedFilterImpl {
  applyFilter(chartData: ColumnNameToValuesMap): ColumnNameToValuesMap {
    if (!this.filterExpression) {
      return chartData;
    }

    const filteredInIndexes = [];
    const newChartData = chartData;
    const filterDateRangeParts = this.filterExpression.split(' - ');
    if (filterDateRangeParts.length === 2) {
      const filterStartDate = moment(filterDateRangeParts[0]);
      const filterEndDate = moment(filterDateRangeParts[1]);

      if (chartData[this.sqlColumn.name]) {
        newChartData[this.sqlColumn.name] = chartData[this.sqlColumn.name].filter(
          (chartDataDate: string, index: number): boolean => {
            const chartDataDateIsBetweenFilterStartDateAndFilterEndDate = moment(chartDataDate).isBetween(
              filterStartDate,
              filterEndDate
            );

            if (chartDataDateIsBetweenFilterStartDateAndFilterEndDate) {
              filteredInIndexes.push(index);
            }

            return chartDataDateIsBetweenFilterStartDateAndFilterEndDate;
          }
        );
      }
    }

    return this.filterChartDataOtherColumns(newChartData, filteredInIndexes);
  }
}
