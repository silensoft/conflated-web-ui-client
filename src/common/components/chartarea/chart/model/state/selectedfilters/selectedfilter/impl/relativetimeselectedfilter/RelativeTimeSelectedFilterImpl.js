// @flow

import type { Element } from 'react';
import React from 'react';
import _ from 'lodash';
import moment from 'moment';
import AbstractSelectedFilterImpl from '../AbstractSelectedFilterImpl';
import type { ColumnNameToValuesMap } from '../../../../chartdata/ColumnNameToValuesMap';
import type { ChartData } from '../../../../chartdata/ChartData';
import RelativeTimeFilterInputView from '../../../../../../../../filterselector/view/selectedfilter/filterinput/relativetime/RelativeTimeFilterInputView';

export default class RelativeTimeSelectedFilterImpl extends AbstractSelectedFilterImpl {
  applyFilter(chartData: ColumnNameToValuesMap): ColumnNameToValuesMap {
    if (!this.filterExpression) {
      return chartData;
    }

    const relativeTimeParts = this.filterExpression.split(' ');

    if (relativeTimeParts.length !== 2) {
      return chartData;
    }

    const filteredInIndexes = [];
    const newChartData = chartData;
    const relativeTimeValue = parseInt(relativeTimeParts[0], 10);

    if (_.isFinite(relativeTimeValue)) {
      const relativeTimeUnit = relativeTimeParts[1];
      const relativeTimeBeginTimestamp = moment().subtract(relativeTimeValue, relativeTimeUnit.toLowerCase());

      if (chartData[this.sqlColumn.name]) {
        newChartData[this.sqlColumn.name] = chartData[this.sqlColumn.name].filter(
          (chartDataTimestamp: string, index: number): boolean => {
            const chartDataTimestampIsAfterRelativeTimeBeginTimestamp = moment(chartDataTimestamp).isAfter(
              relativeTimeBeginTimestamp
            );
            if (chartDataTimestampIsAfterRelativeTimeBeginTimestamp) {
              filteredInIndexes.push(index);
            }
            return chartDataTimestampIsAfterRelativeTimeBeginTimestamp;
          }
        );
      }
    }

    return this.filterChartDataOtherColumns(newChartData, filteredInIndexes);
  }

  getFilterInputView(
    className: string,
    chartData: ChartData,
    changeFilterExpression: string => void
  ): Element<any> {
    return (
      <RelativeTimeFilterInputView
        changeFilterExpression={changeFilterExpression}
        filterExpression={this.filterExpression}
      />
    );
  }
}
