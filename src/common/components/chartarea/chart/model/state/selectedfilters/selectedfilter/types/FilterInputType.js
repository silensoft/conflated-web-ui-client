// @flow

export type FilterInputType =
  | 'Input filter'
  | 'Range filter'
  | 'Dropdown filter'
  | 'Relative time filter'
  | 'Date range filter'
  | 'Timestamp range filter'
  | 'Checkboxes filter'
  | 'Radio buttons filter';
