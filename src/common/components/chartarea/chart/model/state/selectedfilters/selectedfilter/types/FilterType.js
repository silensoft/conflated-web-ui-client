// @flow

export type FilterType = 'measure' | 'dimension';
