// @flow

import type { FilterType } from './types/FilterType';
import type { FilterInputType } from './types/FilterInputType';
import type { AggregationFunction } from '../../selectedmeasure/types/AggregationFunction';
import type { Measure } from '../../../../../../../../dataexplorerpage/leftpane/measureselector/model/state/entities/Measure';
import type { Dimension } from '../../../../../../../../dataexplorerpage/leftpane/dimensionselector/model/state/entities/Dimension';
import type { DataScopeType } from '../../../../../../../model/state/types/DataScopeType';

export type SelectedFilterConfiguration = {
  +allowedDimensionFilterInputTypes: FilterInputType[],
  +aggregationFunction: AggregationFunction,
  +chartId: string,
  +dataScopeType: DataScopeType,
  +filterExpression: string,
  +filterInputType: FilterInputType,
  +isDrillDownFilter: boolean,
  +isSelectionFilter: boolean,
  +measureOrDimension: Measure | Dimension,
  +sqlColumn: {
    +name: string,
    +expression: string
  },
  +type: FilterType
};
