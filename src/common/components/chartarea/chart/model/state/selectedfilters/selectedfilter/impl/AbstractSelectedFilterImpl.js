// @flow

import type { Element } from 'react';
import _ from 'lodash';
import type { Measure } from '../../../../../../../../../dataexplorerpage/leftpane/measureselector/model/state/entities/Measure';
import type { Dimension } from '../../../../../../../../../dataexplorerpage/leftpane/dimensionselector/model/state/entities/Dimension';
import type { AggregationFunction } from '../../../selectedmeasure/types/AggregationFunction';
import type { FilterType } from '../types/FilterType';
import type { FilterInputType } from '../types/FilterInputType';
import type { DataScopeType } from '../../../../../../../../model/state/types/DataScopeType';
import type { SelectedFilter } from '../SelectedFilter';
import type { SelectedFilterConfiguration } from '../SelectedFilterConfiguration';
import type { ColumnNameToValuesMap } from '../../../chartdata/ColumnNameToValuesMap';
import type { ChartData } from '../../../chartdata/ChartData';

export default class AbstractSelectedFilterImpl implements SelectedFilter {
  +measureOrDimension: Measure | Dimension;

  +sqlColumn: {
    +name: string,
    +expression: string
  };

  +aggregationFunction: AggregationFunction;

  +type: FilterType;

  +filterExpression: string;

  +filterInputType: FilterInputType;

  +dataScopeType: DataScopeType;

  +allowedDimensionFilterInputTypes: FilterInputType[];

  +chartId: string;

  +isSelectionFilter: boolean;

  +isDrillDownFilter: boolean;

  constructor(selectedFilterConfiguration: SelectedFilterConfiguration) {
    this.measureOrDimension = selectedFilterConfiguration.measureOrDimension;
    this.sqlColumn = selectedFilterConfiguration.sqlColumn;
    this.aggregationFunction = selectedFilterConfiguration.aggregationFunction;
    this.type = selectedFilterConfiguration.type;
    this.filterExpression = selectedFilterConfiguration.filterExpression;
    this.filterInputType = selectedFilterConfiguration.filterInputType;
    this.dataScopeType = selectedFilterConfiguration.dataScopeType;
    this.allowedDimensionFilterInputTypes = selectedFilterConfiguration.allowedDimensionFilterInputTypes;
    this.chartId = selectedFilterConfiguration.chartId;
    this.isSelectionFilter = selectedFilterConfiguration.isSelectionFilter;
    this.isDrillDownFilter = selectedFilterConfiguration.isDrillDownFilter;
  }

  getConfiguration(): SelectedFilterConfiguration {
    return {
      measureOrDimension: this.measureOrDimension,
      sqlColumn: this.sqlColumn,
      aggregationFunction: this.aggregationFunction,
      type: this.type,
      filterExpression: this.filterExpression,
      filterInputType: this.filterInputType,
      dataScopeType: this.dataScopeType,
      allowedDimensionFilterInputTypes: this.allowedDimensionFilterInputTypes,
      chartId: this.chartId,
      isSelectionFilter: this.isSelectionFilter,
      isDrillDownFilter: this.isDrillDownFilter
    };
  }

  // eslint-disable-next-line no-unused-vars
  applyFilter(chartData: ColumnNameToValuesMap): ColumnNameToValuesMap {
    throw new Error('Abstract method called');
  }

  getFilterInputView(
    // eslint-disable-next-line no-unused-vars
    className: string,
    // eslint-disable-next-line no-unused-vars,no-unused-vars
    chartData: ChartData,
    // eslint-disable-next-line no-unused-vars
    changeFilterExpression: string => void
  ): Element<any> {
    throw new Error('Abstract method called');
  }

  filterChartDataOtherColumns(
    chartData: ColumnNameToValuesMap,
    filteredInIndexes: number[]
  ): ColumnNameToValuesMap {
    const newChartData = chartData;

    if (!_.isEmpty(filteredInIndexes)) {
      Object.keys(chartData)
        .filter((column: string) => !column.endsWith('___') && column !== this.sqlColumn.name)
        .forEach((column: string) => {
          if (chartData[column]) {
            newChartData[column] = chartData[column].filter((unused: any, index: number) =>
              filteredInIndexes.includes(index)
            );
          }
        });
    }

    return newChartData;
  }

  shouldFetchChartData(chartData: ChartData): boolean {
    const filterValues = chartData.getForSelectedFilter(this);

    return (
      (this.type === 'measure' &&
        this.filterInputType === 'Range filter' &&
        this.dataScopeType === 'already fetched' &&
        !filterValues) ||
      (this.type === 'dimension' &&
        this.dataScopeType === 'already fetched' &&
        !filterValues &&
        (this.filterInputType === 'dropdown' ||
          this.filterInputType === 'checkboxes' ||
          this.filterInputType === 'radio buttons'))
    );
  }

  shouldFetchDimensionValues(chartData: ChartData): boolean {
    const allFilterValues = chartData.getAllValues(this);

    return (
      this.type === 'dimension' &&
      this.dataScopeType === 'all' &&
      !allFilterValues &&
      (this.filterInputType === 'dropdown' ||
        this.filterInputType === 'checkboxes' ||
        this.filterInputType === 'radio buttons')
    );
  }

  shouldFetchMeasureMinMaxValues(chartData: ChartData): boolean {
    const [minValue, maxValue] = chartData.getMinAndMaxValueForSelectedFilter(this);

    return (
      this.type === 'measure' &&
      this.filterInputType === 'Range filter' &&
      this.dataScopeType === 'all' &&
      minValue == null &&
      maxValue == null
    );
  }
}
