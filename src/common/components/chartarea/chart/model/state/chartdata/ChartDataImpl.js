// @flow

import _ from 'lodash';
import type { ColumnNameToValuesMap } from './ColumnNameToValuesMap';
import type { ChartData } from './ChartData';
import Constants from '../../../../../../Constants';
import type { SelectedFilter } from '../selectedfilters/selectedfilter/SelectedFilter';
import type { SelectedMeasure } from '../selectedmeasure/SelectedMeasure';
import type { SelectedDimension } from '../selecteddimension/SelectedDimension';
import type { MeasureVisualizationType } from '../selectedmeasure/types/MeasureVisualizationType';
import Utils from '../../../../../../model/state/utils/Utils';
import type { DimensionVisualizationType } from '../selecteddimension/types/DimensionVisualizationType';
import type { SelectedSortBy } from '../selectedsortbys/selectedsortby/SelectedSortBy';
import type { DataScopeType } from '../../../../../../model/state/types/DataScopeType';
import RowComparer from './rowcomparer/RowComparer';
import type { TriggersPageStateNamespace } from '../../../../../triggerspage/model/state/namespace/TriggersPageStateNamespace';

export default class ChartDataImpl implements ChartData {
  columnNameToDataMap: ColumnNameToValuesMap;

  constructor(columnNameToDataMap?: ColumnNameToValuesMap) {
    this.columnNameToDataMap = columnNameToDataMap ?? {};
  }

  filterChartData(selectedFilters: SelectedFilter[], dataScopeType?: DataScopeType = 'already fetched') {
    this.setUnfilteredChartData();

    selectedFilters
      .filter((selectedFilter: SelectedFilter) => selectedFilter.dataScopeType === dataScopeType)
      .forEach((selectedFilter: SelectedFilter) => {
        this.columnNameToDataMap = selectedFilter.applyFilter(this.columnNameToDataMap);
      });
  }

  getAllValues(selectedFilter: SelectedFilter): Array<any> {
    return this.columnNameToDataMap[`${selectedFilter.sqlColumn.name}___all___`] ?? [];
  }

  getBubbleChartData(
    selectedMeasures: SelectedMeasure[],
    selectedDimensions: SelectedDimension[]
  ): [any[], any[], any[], any[]] {
    const xAxisData = this.getForSelectedMeasureOfType(selectedMeasures, 'x-axis');
    const yAxisData = this.getForSelectedMeasureOfType(selectedMeasures, 'y-axis');
    const radiusData = this.getForSelectedMeasureOfType(selectedMeasures, 'radius');
    const legendData = this.getForSelectedDimensionOfType(selectedDimensions, 'Legend');

    return [xAxisData, yAxisData, radiusData, legendData];
  }

  getChartDataAsRows(): Array<{ [string]: any }> {
    const chartDataRowCount = this.getRowCount();
    const chartDataRows = [];

    for (let rowIndex = 0; rowIndex < chartDataRowCount; rowIndex++) {
      chartDataRows.push(
        Object.keys(this.columnNameToDataMap).reduce(
          (chartDataRow: { [string]: any }, columnName: string) =>
            Object.assign(chartDataRow, {
              [columnName]: this.columnNameToDataMap[columnName]?.[rowIndex] ?? undefined
            }),
          {}
        )
      );
    }

    return chartDataRows;
  }

  getChartDataRowsAsColumns(chartDataRows: Array<{ [string]: any }>): ColumnNameToValuesMap {
    return Object.keys(this.columnNameToDataMap).reduce(
      (chartData: Object, columnName: string) =>
        Object.assign(chartData, {
          [columnName]: chartDataRows.map((chartDataRow: Object) => chartDataRow[columnName])
        }),
      {}
    );
  }

  getColumnNameToValuesMap(): ColumnNameToValuesMap {
    return this.columnNameToDataMap;
  }

  getForSelectedDimension(selectedDimension: ?SelectedDimension): Array<any> {
    if (selectedDimension) {
      return this.columnNameToDataMap[selectedDimension.sqlColumn.name] ?? [];
    }

    return [];
  }

  getForSelectedDimensionOfType(
    selectedDimensions: SelectedDimension[],
    visualizationType: DimensionVisualizationType
  ): Array<any> {
    const selectedDimension = Utils.findElem(selectedDimensions, 'visualizationType', visualizationType);
    return this.getForSelectedDimension(selectedDimension);
  }

  getForSelectedFilter(selectedFilter: ?SelectedFilter): Array<any> {
    if (selectedFilter) {
      if (selectedFilter.dataScopeType === 'all') {
        return this.columnNameToDataMap[`${selectedFilter.sqlColumn.name}___all___`] ?? [];
      }

      const unfilteredData = this.columnNameToDataMap[`${selectedFilter.sqlColumn.name}___unfiltered___`];
      return unfilteredData ?? (this.columnNameToDataMap[selectedFilter.sqlColumn.name] ?? []);
    }

    return [];
  }

  getForSelectedMeasure(selectedMeasure: ?SelectedMeasure): Array<any> {
    if (selectedMeasure) {
      return this.columnNameToDataMap[selectedMeasure.sqlColumn.name] ?? [];
    }

    return [];
  }

  getForSelectedMeasureOfType(
    selectedMeasures: SelectedMeasure[],
    visualizationType: MeasureVisualizationType
  ): Array<any> {
    const selectedMeasure = Utils.findElem(selectedMeasures, 'visualizationType', visualizationType);
    return this.getForSelectedMeasure(selectedMeasure);
  }

  getForSelectedSortBy(selectedSortBy: ?SelectedSortBy): Array<any> {
    if (selectedSortBy) {
      return this.columnNameToDataMap[selectedSortBy.sqlColumn.name] ?? [];
    }

    return [];
  }

  getMinAndMaxValueForSelectedFilter(selectedFilter: SelectedFilter): [number, number] {
    let minValue = 0;
    let maxValue = Constants.SLIDER_MAX_VALUE;
    const columnName = selectedFilter.sqlColumn.name;

    const lowerCaseSqlColumnName = columnName.toLowerCase();
    const isPercentColumn =
      lowerCaseSqlColumnName.includes('percent') || lowerCaseSqlColumnName.includes('%');

    if (isPercentColumn) {
      maxValue = Constants.PERCENT_SLIDER_MAX_VALUE;
    } else {
      const measureValues =
        this.columnNameToDataMap[`${columnName}___unfiltered___`] ?? this.columnNameToDataMap[columnName];

      const measureMinValue = this.columnNameToDataMap[`${columnName}___min___`]?.[0];
      const measureMaxValue = this.columnNameToDataMap[`${columnName}___max___`]?.[0];

      if (selectedFilter.dataScopeType === 'already fetched' && measureValues) {
        minValue = _.min(measureValues);
        maxValue = _.max(measureValues);
      } else if (
        selectedFilter.dataScopeType === 'all' &&
        measureMinValue != null &&
        measureMaxValue != null
      ) {
        minValue = measureMinValue;
        maxValue = measureMaxValue;
      }
    }

    return [minValue, maxValue];
  }

  getMapLocationData(selectedDimensions: SelectedDimension[]): [Array<any>, Array<any>] {
    return [
      this.getForSelectedDimensionOfType(selectedDimensions, 'Latitude'),
      this.getForSelectedDimensionOfType(selectedDimensions, 'Longitude')
    ];
  }

  getRowCount(): number {
    const columnNames = Object.keys(this.columnNameToDataMap).filter(
      (columnName: string) => !columnName.endsWith('___')
    );

    if (columnNames.length > 0) {
      return columnNames.reduce(
        (accumulatedRowCount: number, columnName: string) =>
          Math.min(accumulatedRowCount, this.columnNameToDataMap[columnName]?.length ?? 0),
        Number.MAX_SAFE_INTEGER
      );
    }

    return 0;
  }

  getScatterChartData(
    selectedMeasures: SelectedMeasure[],
    selectedDimensions: SelectedDimension[]
  ): [any[], any[], any[]] {
    const xAxisData = this.getForSelectedMeasureOfType(selectedMeasures, 'x-axis');
    const yAxisData = this.getForSelectedMeasureOfType(selectedMeasures, 'y-axis');
    const legendData = this.getForSelectedDimensionOfType(selectedDimensions, 'Legend');

    return [xAxisData, yAxisData, legendData];
  }

  getTriggerData(pageStateNamespace: TriggersPageStateNamespace): [Array<any>, Array<any>, Array<any>] {
    let triggerNameData;

    if (pageStateNamespace === 'alertsPage') {
      triggerNameData = this.columnNameToDataMap['"Alert name"'] ?? [];
    } else {
      triggerNameData = this.columnNameToDataMap['"Goal name"'] ?? [];
    }

    return [triggerNameData, ...this.getTriggerGroupData(pageStateNamespace)];
  }

  getTriggerGroupData(pageStateNamespace: TriggersPageStateNamespace): [Array<any>, Array<any>] {
    let triggerGroupNameData;
    let severityOrStatusData;

    if (pageStateNamespace === 'alertsPage') {
      triggerGroupNameData = this.columnNameToDataMap['"Alert group"'] ?? [];
      severityOrStatusData = this.columnNameToDataMap.Severity ?? [];
    } else {
      triggerGroupNameData = this.columnNameToDataMap['"Goal group"'] ?? [];
      severityOrStatusData = this.columnNameToDataMap.Status ?? [];
    }

    return [triggerGroupNameData, severityOrStatusData];
  }

  setUnfilteredChartData() {
    Object.keys(this.columnNameToDataMap)
      .filter((columnName: string) => !columnName.endsWith('___'))
      .forEach((columnName: string) => {
        if (!this.columnNameToDataMap[`${columnName}___unfiltered___`]) {
          this.columnNameToDataMap[`${columnName}___unfiltered___`] = this.columnNameToDataMap[columnName];
        }

        this.columnNameToDataMap[columnName] = this.columnNameToDataMap[`${columnName}___unfiltered___`];
      });
  }

  sort(
    selectedSortBys: SelectedSortBy[],
    chartDataRows: Array<{ [string]: any }>,
    dataScopeType: DataScopeType
  ) {
    Utils.pick(selectedSortBys, 'dataScopeType', dataScopeType).forEach(
      ({ sqlColumn, sortDirection }: SelectedSortBy) => {
        if (chartDataRows.length > 0 && chartDataRows[0][sqlColumn.name]) {
          chartDataRows.sort((chartDataRow1: { [string]: any }, chartDataRow2: { [string]: any }) =>
            RowComparer.compareRows(chartDataRow1, chartDataRow2, sortDirection, sqlColumn.name)
          );
        }
      }
    );
  }

  sortChartData(selectedSortBys: SelectedSortBy[], dataScopeType?: DataScopeType = 'already fetched') {
    if (_.isEmpty(selectedSortBys) || !Utils.has(selectedSortBys, 'dataScopeType', dataScopeType)) {
      return;
    }

    const chartDataRows = this.getChartDataAsRows();
    this.sort(selectedSortBys, chartDataRows, dataScopeType);
    this.columnNameToDataMap = this.getChartDataRowsAsColumns(chartDataRows);
  }
}
