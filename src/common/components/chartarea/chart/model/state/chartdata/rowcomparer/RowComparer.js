// @flow

import type { SortDirection } from '../../selectedsortbys/selectedsortby/types/SortDirection';

export default class RowComparer {
  static compareRows(
    chartDataRow: { [string]: any },
    otherchartDataRow: { [string]: any },
    sortDirection: SortDirection,
    sqlColumnName: string
  ): number {
    if (sortDirection === 'ASC') {
      return this.compareRowsForAscendingOrder(chartDataRow, otherchartDataRow, sortDirection, sqlColumnName);
    }

    return this.compareRowsForDescendingOrder(chartDataRow, otherchartDataRow, sortDirection, sqlColumnName);
  }

  static compareRowsForAscendingOrder(
    chartDataRow: { [string]: any },
    otherChartDataRow: { [string]: any },
    sortDirection: SortDirection,
    sqlColumnName: string
  ): number {
    if (chartDataRow[sqlColumnName] > otherChartDataRow[sqlColumnName]) {
      return 1;
    } else if (chartDataRow[sqlColumnName] === otherChartDataRow[sqlColumnName]) {
      return 0;
    }
    return -1;
  }

  static compareRowsForDescendingOrder(
    chartDataRow: { [string]: any },
    otherChartDataRow: { [string]: any },
    sortDirection: SortDirection,
    sqlColumnName: string
  ): number {
    if (otherChartDataRow[sqlColumnName] > chartDataRow[sqlColumnName]) {
      return 1;
    } else if (chartDataRow[sqlColumnName] === otherChartDataRow[sqlColumnName]) {
      return 0;
    }
    return -1;
  }
}
