// @flow

export type ColumnNameToValuesMap = { [string]: Array<any> | void | null };
