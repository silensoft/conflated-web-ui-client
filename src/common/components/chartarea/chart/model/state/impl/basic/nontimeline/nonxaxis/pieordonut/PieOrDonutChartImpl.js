// @flow

import type { Element } from 'react';
import type { DimensionVisualizationType } from '../../../../../selecteddimension/types/DimensionVisualizationType';
import NonTimelineChartImpl from '../../NonTimelineChartImpl';
import type { SelectedDimension } from '../../../../../selecteddimension/SelectedDimension';
import type { DataSeries } from '../../../../../types/DataSeries';
import type { SelectedMeasure } from '../../../../../selectedmeasure/SelectedMeasure';
import DimensionDropZoneListItemViewFactory from '../../../../../../../../../../../dataexplorerpage/leftpane/dimensionselector/view/dimensiondropzonelistitemviewfactory/DimensionDropZoneListItemViewFactory';
import type { MeasureVisualizationType } from '../../../../../selectedmeasure/types/MeasureVisualizationType';
import type { LegendPosition } from '../../../../../types/LegendPosition';

export default class PieOrDonutChartImpl extends NonTimelineChartImpl {
  // eslint-disable-next-line no-unused-vars
  getApexChartDataSeries(shownXAxisCategories: Array<any>): DataSeries[] | any[] {
    const emptyData: any[] = [0];

    if (this.selectedMeasures.length === 1) {
      const measureData = this.chartData.getForSelectedMeasure(this.selectedMeasures[0]);
      return measureData?.length > 0 ? measureData : emptyData;
    } else if (this.selectedMeasures.length > 1) {
      return this.selectedMeasures.map(
        (selectedMeasure: SelectedMeasure) => this.chartData.getForSelectedMeasure(selectedMeasure)[0] ?? 0
      );
    }

    return emptyData;
  }

  getColors(): string[] {
    return super.getAllColors();
  }

  getConvertedSelectedDimensions(): SelectedDimension[] {
    const convertedSelectedDimensions = [];

    if (this.selectedDimensions.length >= 1) {
      convertedSelectedDimensions.push({
        ...this.selectedDimensions[0],
        visualizationType: 'Legend'
      });
    }

    if (this.selectedDimensions.length >= 2) {
      this.selectedDimensions.slice(1)
        .forEach((selectedDimension: SelectedDimension) =>
          convertedSelectedDimensions.push({
            ...selectedDimension,
            visualizationType: 'Drilldown'
          })
        );
    }

    return convertedSelectedDimensions;
  }

  getDimensionDropZoneListItemViews(
    dimensionDropZoneListItemViewFactory: DimensionDropZoneListItemViewFactory
  ): Array<Element<any>> {
    if (this.hasSelectedDimensionOfType('Legend')) {
      return [dimensionDropZoneListItemViewFactory.createDimensionDropZoneListItem('1', 'Drilldown')];
    } else {
      return [dimensionDropZoneListItemViewFactory.createDimensionDropZoneListItem('1', 'Legend')];
    }
  }

  getLabels(): string[] {
    if (this.selectedDimensions.length === 0) {
      if (this.selectedMeasures.length === 1) {
        return [this.selectedMeasures[0].measure.name];
      } else if (this.selectedMeasures.length > 1) {
        return this.selectedMeasures.map(({ measure: { name } }: SelectedMeasure) => name);
      }
    } else if (this.selectedMeasures.length === 1) {
      return this.chartData.getForSelectedDimension(
        this.currentDrillDownSelectedDimension ?? this.selectedDimensions[0]
      );
    }

    return [''];
  }

  getNextDimensionVisualizationType(): DimensionVisualizationType {
    return this.hasSelectedDimensionOfType('Legend') ? super.getNextDimensionVisualizationType() : 'Legend';
  }

  getNextMeasureVisualizationType(
    // eslint-disable-next-line no-unused-vars
    measureVisualizationType?: MeasureVisualizationType,
    // eslint-disable-next-line no-unused-vars
    selectedMeasureIndex?: number
  ): MeasureVisualizationType {
    return 'none';
  }

  getPrimarySelectedDimensionType(): DimensionVisualizationType {
    return 'Legend';
  }

  hasFloatingSubtitle(): boolean {
    return true;
  }

  hasFloatingTitle(): boolean {
    return true;
  }

  isPieOrDonutWithMultipleMeasuresOnly(): boolean {
    return this.selectedDimensions.length === 0 && this.selectedMeasures.length > 1;
  }

  shouldShowDataLabels(): boolean {
    return true;
  }

  shouldShowDataLabelsDropShadow(): boolean {
    return true;
  }

  shouldShowLegend(): [boolean, LegendPosition] {
    return [!(this.selectedDimensions.length === 0 && this.selectedMeasures.length === 1), 'right'];
  }

  supportsDataPointsCount(): boolean {
    return true;
  }
}
