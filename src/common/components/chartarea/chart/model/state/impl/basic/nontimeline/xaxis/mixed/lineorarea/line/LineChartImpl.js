// @flow

import type { MeasureVisualizationType } from '../../../../../../../selectedmeasure/types/MeasureVisualizationType';
import LineOrAreaChartImpl from '../LineOrAreaChartImpl';

export default class LineChartImpl extends LineOrAreaChartImpl {
  getNextMeasureVisualizationType(
    // eslint-disable-next-line no-unused-vars
    measureVisualizationType?: MeasureVisualizationType,
    // eslint-disable-next-line no-unused-vars
    selectedMeasureIndex?: number
  ): MeasureVisualizationType {
    return 'line';
  }
}
