// @flow

import type { FillType } from '../../../../../../../types/FillType';
import LineOrAreaChartImpl from '../LineOrAreaChartImpl';
import type { MeasureVisualizationType } from '../../../../../../../selectedmeasure/types/MeasureVisualizationType';

export default class AreaChartImpl extends LineOrAreaChartImpl {
  getNextMeasureVisualizationType(
    // eslint-disable-next-line no-unused-vars
    measureVisualizationType?: MeasureVisualizationType,
    // eslint-disable-next-line no-unused-vars
    selectedMeasureIndex?: number
  ): MeasureVisualizationType {
    return 'area';
  }

  getFillType(): FillType {
    return 'gradient';
  }
}
