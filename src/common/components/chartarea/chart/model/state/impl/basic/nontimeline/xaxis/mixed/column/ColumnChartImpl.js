// @flow

import MixedChartImpl from '../MixedChartImpl';
import type { MeasureVisualizationType } from '../../../../../../selectedmeasure/types/MeasureVisualizationType';
import type { ChartController } from '../../../../../../../../view/basic/apex/ApexChartView';

export default class ColumnChartImpl extends MixedChartImpl {
  getLegendType(): string {
    return 'timestamp or legend';
  }

  getNextMeasureVisualizationType(): MeasureVisualizationType {
    return 'column';
  }

  handleSelectDataPoint(params: Object, actions: ChartController) {
    const { dataPointIndex, selectedDataPoints, w } = params;
    const { setSelectedDataPointIndexForChart } = actions;

    if (this.hasTimestampLegend()) {
      declare var Apex: Object;
      // eslint-disable-next-line no-undef,no-underscore-dangle
      const foundChartInstance: Object = Apex._chartInstances.find(
        (chartInstance: Object) => chartInstance.id === this.id
      );

      const twoLabelsSelectedSeries = selectedDataPoints.filter(
        (dataPoints: ?(any[])) => dataPoints && dataPoints.length === 2
      );

      const noLabelsSelectedSeries = selectedDataPoints.filter(
        (dataPoints: ?(any[])) => !dataPoints || dataPoints.length === 0
      );

      const selectedDataPointCount = selectedDataPoints.filter(
        (dataPoints: ?(any[])) => dataPoints && dataPoints.length > 0
      ).length;

      if (
        noLabelsSelectedSeries.length === 1 &&
        selectedDataPointCount === w.globals.series.length - 1 &&
        this.selectedDataPointIndex != null
      ) {
        const removedSelections = [];
        selectedDataPoints.forEach((dataPoints: any[], seriesIndex: number) =>
          dataPoints.forEach((dataPoint: number) =>
            removedSelections.push({
              seriesIndex,
              dataPoint
            })
          )
        );

        removedSelections.forEach(({ seriesIndex, dataPoint }: Object) => {
          this.isInternallyTriggeredDataPointSelection = true;
          foundChartInstance.chart.toggleDataPointSelection(seriesIndex, dataPoint);
        });
      }

      if (
        twoLabelsSelectedSeries.length === 1 &&
        selectedDataPointCount === w.globals.series.length &&
        this.selectedDataPointIndex != null
      ) {
        const seriesIndex = selectedDataPoints.findIndex(
          (dataPoints: ?(any[])) => dataPoints && dataPoints.length === 1
        );

        this.isInternallyTriggeredDataPointSelection = true;
        foundChartInstance.chart.toggleDataPointSelection(seriesIndex, this.selectedDataPointIndex);
        this.isInternallyTriggeredDataPointSelection = true;
        foundChartInstance.chart.toggleDataPointSelection(0, dataPointIndex);
        return;
      }

      if (selectedDataPointCount !== 0 && this.selectedDataPointIndex != null) {
        return;
      } else if (this.selectedDataPointIndex != null) {
        setSelectedDataPointIndexForChart(this, undefined);
      }

      if (selectedDataPointCount === 1 && this.selectedDataPointIndex != null) {
        w.globals.series.forEach((value: number, index: number) => {
          if (
            !selectedDataPoints[index] ||
            (selectedDataPoints[index] && !selectedDataPoints[index].includes(dataPointIndex))
          ) {
            this.isInternallyTriggeredDataPointSelection = true;
            foundChartInstance.chart.toggleDataPointSelection(index, dataPointIndex);
          }
        });
      }

      if (this.selectedDataPointIndex == null) {
        setSelectedDataPointIndexForChart(this, dataPointIndex);
      }
    }

    super.handleSelectDataPoint(params, actions);
  }

  isXAxisScrollable(): boolean {
    return this.selectedDimensions.length > 0;
  }

  shouldShowStroke(): boolean {
    return true;
  }

  supportsAllDimension(): boolean {
    return true;
  }
}
