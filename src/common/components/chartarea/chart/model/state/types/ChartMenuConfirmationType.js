// @flow

export type ChartMenuConfirmationType = 'clear' | 'delete' | 'none';
