// @flow

export type DataSeries = {
  name: string,
  type?: ?string,
  data: any[]
};
