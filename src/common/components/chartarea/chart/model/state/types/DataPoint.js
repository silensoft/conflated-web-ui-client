// @flow

export type DataPoint = {
  dataSeriesIndex: number,
  labelIndex: number
};
