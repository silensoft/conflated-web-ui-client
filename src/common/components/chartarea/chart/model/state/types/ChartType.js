// @flow

export type ChartType =
  | 'column'
  | 'bar'
  | 'area'
  | 'line'
  | 'donut'
  | 'pie'
  | 'bubble'
  | 'scatter'
  | 'candlestick'
  | 'boxplot'
  | 'violin'
  | 'radar'
  | 'heatmap'
  | 'datatable'
  | 'map'
  | 'statistic'
  | 'alertsdatatable'
  | 'goalsdatatable';
