// @flow

export type FillType = 'gradient' | 'solid';
