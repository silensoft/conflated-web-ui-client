// @flow

export type LegendPosition = 'bottom' | 'right';
