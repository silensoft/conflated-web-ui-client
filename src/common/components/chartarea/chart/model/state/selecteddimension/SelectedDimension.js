// @flow

import type { Dimension } from '../../../../../../../dataexplorerpage/leftpane/dimensionselector/model/state/entities/Dimension';
import type { DimensionVisualizationType } from './types/DimensionVisualizationType';
import type { Measure } from '../../../../../../../dataexplorerpage/leftpane/measureselector/model/state/entities/Measure';

export type SelectedDimension = $Exact<{
  +dimension: Dimension | Measure,
  +sqlColumn: {
    +name: string,
    +expression: string
  },
  +visualizationType: DimensionVisualizationType,
  +previousVisualizationType?: ?DimensionVisualizationType,
  +visualizationColor?: string
}>;
