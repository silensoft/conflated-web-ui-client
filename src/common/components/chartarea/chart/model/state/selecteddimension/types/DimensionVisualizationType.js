// @flow

export type DimensionVisualizationType =
  | 'none'
  | 'X-axis categories'
  | 'Data points'
  | 'Timeline'
  | 'Legend'
  | 'Latitude'
  | 'Longitude'
  | 'Tooltip'
  | 'Drilldown'
  | 'Column';
