// @flow

import type { Column } from './types/Column';
import type { ColumnNameToValuesMap } from '../state/chartdata/ColumnNameToValuesMap';
import type { MinMaxMeasureColumn } from './types/MinMaxMeasureColumn';
import ChartDataService from './ChartDataService';
import HashValueCalculator from '../../../../../model/state/utils/HashValueCalculator';
import type { DataSource } from '../../../../../model/state/datasource/DataSource';
import type { SelectedSortBy } from '../state/selectedsortbys/selectedsortby/SelectedSortBy';
import type { SelectedFilter } from '../state/selectedfilters/selectedfilter/SelectedFilter';

export default class CachingChartDataServiceProxyImpl extends ChartDataService {
  static MAX_NUMBER_OF_CHART_DATAS_STORED = 50;

  static CACHED_CHART_DATA_KEYS = 'ChartDataCache-cachedChartDataKeys';

  chartDataService: ChartDataService;

  constructor(chartDataService: ChartDataService) {
    super();
    this.chartDataService = chartDataService;
  }

  fetchChartData(
    dataSource: DataSource,
    columns: Column[],
    selectedFilters: SelectedFilter[],
    selectedSortBys: SelectedSortBy[]
  ): Promise<ColumnNameToValuesMap> {
    const chartDataKey = `ChartDataCache-ChartData-${HashValueCalculator.hashObject({
      dataSource,
      columns,
      filters: selectedFilters,
      sortBys: selectedSortBys
    })}`;

    const cachedChartDataInJson = localStorage.getItem(chartDataKey);

    if (cachedChartDataInJson) {
      return new Promise<ColumnNameToValuesMap>((resolve: ColumnNameToValuesMap => void) => {
        const cachedChartData = JSON.parse(cachedChartDataInJson);
        resolve(cachedChartData);
      });
    }

    const chartDataFetchPromise = this.chartDataService.fetchChartData(
      dataSource,
      columns,
      selectedFilters,
      selectedSortBys
    );

    chartDataFetchPromise.then((columnNameToValuesMap: ColumnNameToValuesMap) => {
      let cachedChartDataKeys = [];
      const cachedChartDataKeysInJson = localStorage.getItem(
        CachingChartDataServiceProxyImpl.CACHED_CHART_DATA_KEYS
      );

      if (cachedChartDataKeysInJson) {
        cachedChartDataKeys = JSON.parse(cachedChartDataKeysInJson);
      }

      if (cachedChartDataKeys.length > CachingChartDataServiceProxyImpl.MAX_NUMBER_OF_CHART_DATAS_STORED) {
        const firstChartDataKey = cachedChartDataKeys.shift();
        localStorage.removeItem(firstChartDataKey);
      }

      const chartDataInJson = JSON.stringify(columnNameToValuesMap);

      let cacheUpdated = false;

      while (!cacheUpdated && cachedChartDataKeys.length > 0) {
        // noinspection UnusedCatchParameterJS
        try {
          localStorage.setItem(chartDataKey, chartDataInJson);
        } catch (error) {
          const firstChartDataKey = cachedChartDataKeys.shift();
          localStorage.removeItem(firstChartDataKey);
        }
        cacheUpdated = true;
      }

      cachedChartDataKeys.push(chartDataKey);
      localStorage.setItem(
        CachingChartDataServiceProxyImpl.CACHED_CHART_DATA_KEYS,
        JSON.stringify(cachedChartDataKeys)
      );
    });

    return chartDataFetchPromise;
  }

  fetchMinAndMaxValues(
    dataSource: DataSource,
    minMaxMeasureColumns: MinMaxMeasureColumn[],
    dimensionColumns: Column[],
    selectedFilters: SelectedFilter[]
  ): Promise<ColumnNameToValuesMap> {
    return this.chartDataService.fetchMinAndMaxValues(
      dataSource,
      minMaxMeasureColumns,
      dimensionColumns,
      selectedFilters
    );
  }

  fetchDimensionValues(dataSource: DataSource, dimensionColumns: Column[]): Promise<ColumnNameToValuesMap> {
    return this.chartDataService.fetchDimensionValues(dataSource, dimensionColumns);
  }
}
