// @flow

import type { ColumnNameToValuesMap } from '../state/chartdata/ColumnNameToValuesMap';
import type { Column } from './types/Column';
import type { MinMaxMeasureColumn } from './types/MinMaxMeasureColumn';
import type { DataSource } from '../../../../../model/state/datasource/DataSource';
import type { SelectedSortBy } from '../state/selectedsortbys/selectedsortby/SelectedSortBy';
import type { SelectedFilter } from '../state/selectedfilters/selectedfilter/SelectedFilter';

export default class ChartDataService {
  // noinspection JSMethodCanBeStatic
  fetchChartData(
    // eslint-disable-next-line no-unused-vars
    dataSource: DataSource,
    // eslint-disable-next-line no-unused-vars
    columns: Column[],
    // eslint-disable-next-line no-unused-vars
    filters: SelectedFilter[],
    // eslint-disable-next-line no-unused-vars
    sortBys: SelectedSortBy[]
  ): Promise<ColumnNameToValuesMap> {
    throw new TypeError('Abstract method error');
  }

  // noinspection JSMethodCanBeStatic
  fetchMinAndMaxValues(
    // eslint-disable-next-line no-unused-vars
    dataSource: DataSource,
    // eslint-disable-next-line no-unused-vars
    minMaxMeasureColumns: MinMaxMeasureColumn[],
    // eslint-disable-next-line no-unused-vars
    dimensionColumns: Column[],
    // eslint-disable-next-line no-unused-vars
    filters: SelectedFilter[]
  ): Promise<ColumnNameToValuesMap> {
    throw new TypeError('Abstract method error');
  }

  // noinspection JSMethodCanBeStatic
  // eslint-disable-next-line no-unused-vars
  fetchDimensionValues(dataSource: DataSource, dimensionColumns: Column[]): Promise<ColumnNameToValuesMap> {
    throw new TypeError('Abstract method error');
  }
}
