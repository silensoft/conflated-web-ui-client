// @flow

export type MinMaxMeasureColumn = {
  name: string,
  expression: string,
  fetchedRowCount: number
};
