// @flow

import { NamespacedControllerFactory } from 'oo-redux-utils';
import type { ChartAreaPageStateNamespace } from '../../../model/state/namespace/ChartAreaPageStateNamespace';
import ChangeXAxisScrollPositionForSelectedChartAction from '../../../model/actions/chart/selected/change/scrollposition/ChangeXAxisScrollPositionForSelectedChartAction';

export default class ChartScrollbarControllerFactory extends NamespacedControllerFactory<ChartAreaPageStateNamespace> {
  createController = () => ({
    changeXAxisScrollPosition: (xAxisScrollPosition: number) =>
      this.dispatchAction(
        new ChangeXAxisScrollPositionForSelectedChartAction(this.stateNamespace, xAxisScrollPosition)
      )
  });
}
