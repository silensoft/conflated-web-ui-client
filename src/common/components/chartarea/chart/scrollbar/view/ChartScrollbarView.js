// @flow

import type { Element } from 'react';
import React from 'react';
import Scrollbar from 'semantic-ui-react-scrollbar';
import _ from 'lodash';
import { connect } from 'react-redux';
import type { Dispatch } from 'oo-redux-utils';
import ChartScrollbarControllerFactory from '../controller/ChartScrollbarControllerFactory';
import type { Chart } from '../../model/state/Chart';
import type { ChartAreaPageStateNamespace } from '../../../model/state/namespace/ChartAreaPageStateNamespace';

type OwnProps = $Exact<{ chart: Chart, className: string, pageStateNamespace: ChartAreaPageStateNamespace }>;
const mapAppStateToComponentProps = () => ({});

const createController = (dispatch: Dispatch, { pageStateNamespace }: OwnProps) =>
  new ChartScrollbarControllerFactory(dispatch, pageStateNamespace).createController();

type Controller = $Call<typeof createController, Dispatch, OwnProps>;
type Props = $Exact<{ ...OwnProps, ...Controller }>;

const ChartScrollbarView = ({ changeXAxisScrollPosition, chart, className }: Props): Element<any> | null => {
  const maxScrollPosition = chart.getMaxScrollPosition();

  return maxScrollPosition > 0 ? (
    <Scrollbar
      className={className}
      maxScrollPosition={maxScrollPosition}
      changeScrollPosition={(scrollPosition: number) => changeXAxisScrollPosition(scrollPosition)}
    />
  ) : null;
};

export default connect<Props, OwnProps, _, _, _, _>(
  mapAppStateToComponentProps,
  createController
)(ChartScrollbarView);
