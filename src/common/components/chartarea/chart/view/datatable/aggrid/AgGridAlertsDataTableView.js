// @flow

import type { Element } from 'react';
import React, { useMemo } from 'react';
import { AgGridReact } from 'ag-grid-react';
import HashValueCalculator from '../../../../../../model/state/utils/HashValueCalculator';
import type { SelectedDimension } from '../../../model/state/selecteddimension/SelectedDimension';
import type { Chart } from '../../../model/state/Chart';

type Props = $Exact<{ chart: Chart, height: number, width: number }>;

const AgGridAlertsDataTableView = ({ chart, height, width }: Props): Element<any> => {
  const columnWidthWeights = {
    Severity: 0.05,
    'Trigger time': 0.12,
    'Active duration': 0.07,
    'Alert group': 0.14,
    'Alert name': 0.2,
    'Trigger details': 0.15,
    Status: 0.07,
    Assignee: 0.08,
    'Status last modified': 0.12
  };

  let columnDefs = useMemo(
    () =>
      chart.selectedDimensions.map(
        ({ dimension: { name, isDate, isString, isTimestamp }, sqlColumn }: SelectedDimension): Object => {
          let filter = 'agNumberColumnFilter';

          if (isString) {
            filter = 'agTextColumnFilter';
          } else if (isDate || isTimestamp) {
            filter = 'agDateColumnFilter';
          }

          const colDef: Object = {
            headerName: name,
            field: sqlColumn.name,
            sortable: true,
            resizable: true,
            tooltipField: name,
            width: columnWidthWeights[name] * (width - 22),
            filter
          };

          const severityToPriorityValueMap = {
            Critical: 4,
            Major: 3,
            Minor: 2,
            Info: 1
          };

          if (name === 'Severity') {
            colDef.comparator = (severity1: string, severity2: string) =>
              severityToPriorityValueMap[severity1] - severityToPriorityValueMap[severity2];
          }

          return colDef;
        }
      ),
    [chart.selectedDimensions]
  );

  const severityIndicatorColumnDef = useMemo(
    () => ({
      width: 20,
      field: 'Severity',
      cellStyle(params: Object): Object {
        let color;
        switch (params.value) {
          case 'Critical':
            color = 'red';
            break;
          case 'Major':
            color = 'orange';
            break;
          case 'Minor':
            color = 'yellow';
            break;
          default:
            color = 'white';
        }
        return { color, 'background-color': color };
      }
    }),
    []
  );

  columnDefs = [severityIndicatorColumnDef, ...columnDefs];
  const dataRows = chart.chartData.getChartDataAsRows();
  const key = HashValueCalculator.hashObject({ columnDefs, dataRows });

  return (
    <div className="ag-theme-fresh" style={{ height }}>
      <AgGridReact
        key={key}
        columnDefs={columnDefs}
        rowData={dataRows}
        rowSelection="multiple"
        pagination
        enableBrowserTooltips
      />
    </div>
  );
};

export default AgGridAlertsDataTableView;
