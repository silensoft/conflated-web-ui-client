// @flow

import type { Element } from 'react';
import React, { useMemo } from 'react';
import { AgGridReact } from 'ag-grid-react';
import HashValueCalculator from '../../../../../../model/state/utils/HashValueCalculator';
import type { SelectedDimension } from '../../../model/state/selecteddimension/SelectedDimension';
import type { Chart } from '../../../model/state/Chart';

type Props = $Exact<{ chart: Chart, height: number, width: number }>;

const AgGridGoalsDataTableView = ({ chart, height, width }: Props): Element<any> => {
  const columnWidthWeights = {
    Status: 0.1,
    'Trigger time': 0.12,
    'Goal group': 0.2,
    'Goal name': 0.3,
    'Trigger details': 0.28
  };

  let columnDefs = useMemo(
    () =>
      chart.selectedDimensions.map(
        ({ dimension: { name, isString, isTimestamp }, sqlColumn }: SelectedDimension): Object => {
          let filter = 'agNumberColumnFilter';

          if (isString) {
            filter = 'agTextColumnFilter';
          } else if (isTimestamp) {
            filter = 'agDateColumnFilter';
          }

          const colDef: Object = {
            headerName: name,
            field: sqlColumn.name,
            sortable: true,
            resizable: true,
            tooltipField: name,
            width: columnWidthWeights[name] * (width - 22),
            filter
          };

          const statusToPriorityValueMap = {
            'Far below target': 3,
            'Below target': 2,
            'On target': 1
          };

          if (name === 'Status') {
            colDef.comparator = (status1: string, status2: string) =>
              statusToPriorityValueMap[status1] - statusToPriorityValueMap[status2];
          }

          return colDef;
        }
      ),
    [chart.selectedDimensions]
  );

  const statusIndicatorColumnDef = useMemo(
    () => ({
      width: 20,
      field: 'Status',
      cellStyle(params: Object): Object {
        let color;
        switch (params.value) {
          case 'On target':
            color = 'green';
            break;
          case 'Below target':
            color = 'yellow';
            break;
          case 'Far below target':
            color = 'red';
            break;
          default:
            color = 'white';
        }
        return { color, 'background-color': color };
      }
    }),
    []
  );

  columnDefs = [statusIndicatorColumnDef, ...columnDefs];
  const dataRows = chart.chartData.getChartDataAsRows();
  const key = HashValueCalculator.hashObject({ columnDefs, dataRows });

  return (
    <div className="ag-theme-fresh" style={{ height }}>
      <AgGridReact
        key={key}
        columnDefs={columnDefs}
        rowData={dataRows}
        rowSelection="multiple"
        pagination
        enableBrowserTooltips
      />
    </div>
  );
};

export default AgGridGoalsDataTableView;
