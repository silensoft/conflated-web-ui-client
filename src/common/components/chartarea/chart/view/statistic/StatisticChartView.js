// @flow

import _ from 'lodash';
import type { Element } from 'react';
import React from 'react';
import type { Dispatch } from 'oo-redux-utils';
import { connect } from 'react-redux';
import { statisticGroup } from './StatisticChartView.module.scss';
import ChartControllerFactory from '../../controller/ChartControllerFactory';
import type { ChartAreaPageStateNamespace } from '../../../model/state/namespace/ChartAreaPageStateNamespace';
import type { Chart } from '../../model/state/Chart';

type OwnProps = $Exact<{ chart: Chart, pageStateNamespace: ChartAreaPageStateNamespace }>;
const mapAppStateToComponentProps = () => ({});

const createController = (dispatch: Dispatch, { pageStateNamespace }: OwnProps) =>
  new ChartControllerFactory(dispatch, pageStateNamespace).createController();

type Controller = $Call<typeof createController, Dispatch, OwnProps>;
type Props = $Exact<{ ...OwnProps, ...Controller }>;

const StatisticChartView = ({ chart, selectChart, pageStateNamespace }: Props): Element<any> => (
  <div key={chart.id} className={statisticGroup} onClick={() => selectChart(chart)}>
    {chart.createChartView(0, 0, pageStateNamespace)}
  </div>
);

export default connect<Props, OwnProps, _, _, _, _>(
  mapAppStateToComponentProps,
  createController
)(StatisticChartView);
