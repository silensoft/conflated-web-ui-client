// @flow

import React from 'react';
import type { Element } from 'react';
import { smallText, uiHints, uiHintsContainer } from './ChartConfigHintsView.module.scss';
import type { Chart } from '../../model/state/Chart';

type Props = $Exact<{
  chart: Chart
}>;

const ChartConfigHintsView = ({ chart }: Props): Element<any> | null => {
  const chartConfigHintTitle = chart.getChartConfigHintTitle();
  const chartConfigHintSubtitle = chart.getChartConfigHintSubtitle();

  if (chartConfigHintTitle) {
    return (
      <div className={uiHintsContainer}>
        <div className={uiHints}>{chartConfigHintTitle}</div>
        {chartConfigHintSubtitle ? <div className={smallText}>{chartConfigHintSubtitle}</div> : undefined}
      </div>
    );
  }

  return null;
};

export default ChartConfigHintsView;
