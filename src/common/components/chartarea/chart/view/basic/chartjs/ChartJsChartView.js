// @flow

import _ from 'lodash';
import classNames from 'classnames';
import 'chartjs-chart-box-and-violin-plot';
import type { Element } from 'react';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import { default as ChartJsChart } from 'chart.js';
import { connect } from 'react-redux';
import type { Dispatch } from 'oo-redux-utils';
import {
  chartCanvas,
  chartSubtitle,
  chartTitle,
  chartTitles,
  drillDownTitles
} from './ChartJsChartView.module.scss';
import type { ChartAreaPageStateNamespace } from '../../../../model/state/namespace/ChartAreaPageStateNamespace';
import type { Chart } from '../../../model/state/Chart';
import ChartControllerFactory from '../../../controller/ChartControllerFactory';
import ChartJsChartBaseOptionsFactory from './model/factories/ChartJsChartBaseOptionsFactory';

type OwnProps = $Exact<{
  chart: Chart,
  pageStateNamespace: ChartAreaPageStateNamespace
}>;

const mapAppStateToComponentProps = () => ({});
const createController = (dispatch: Dispatch, { pageStateNamespace }: OwnProps) =>
  new ChartControllerFactory(dispatch, pageStateNamespace).createController();

type Controller = $Call<typeof createController, Dispatch, OwnProps>;
type Props = $Exact<{ ...OwnProps, ...Controller }>;

const ChartJsChartView = ({ chart, pageStateNamespace, ...actions }: Props): Element<any> => {
  const chartCanvasRef: { current: any } = useRef(null);
  const [chartJsChart, setChartJsChart] = useState(null);

  useEffect(() => {
    updateChart();
  });

  const updateChart = useCallback(() => {
    const data = chart.getChartJsDataSetsAndLabels();
    ChartJsChart.defaults.global.defaultFontFamily = 'Arimo';

    const options = {
      ...ChartJsChartBaseOptionsFactory.createBaseOptions(),
      onClick: (event: any, activeElements: Object[]) =>
        chart.handleChartJsClick(event, activeElements, data, pageStateNamespace, actions)
    };

    if (chartJsChart) {
      chartJsChart.update(options);
    } else {
      const context = chartCanvasRef.current.getContext('2d');

      setChartJsChart(
        new ChartJsChart(context, {
          type: chart.chartType,
          data,
          options
        })
      );
    }
  });

  const chartTitlesClassName = classNames(chartTitles, { [drillDownTitles]: chart.drillDowns.length > 0 });

  return (
    <React.Fragment>
      <canvas className={chartCanvas} ref={chartCanvasRef} />
      <div className={chartTitlesClassName}>
        <div className={chartTitle}>{chart.getTitleText()}</div>
        <div className={chartSubtitle}>{chart.getSubtitleText()}</div>
      </div>
    </React.Fragment>
  );
};

export default connect<Props, OwnProps, _, _, _, _>(
  mapAppStateToComponentProps,
  createController
)(ChartJsChartView);
