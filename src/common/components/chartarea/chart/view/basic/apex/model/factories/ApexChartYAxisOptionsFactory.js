// @flow

import type { Chart } from '../../../../../model/state/Chart';

export default class ApexChartYAxisOptionsFactory {
  static createYAxisOptions(chart: Chart): Object {
    return {
      show: chart.hasData(),
      tooltip: {
        enabled: chart.shouldShowYAxisTooltip()
      }
    };
  }
}
