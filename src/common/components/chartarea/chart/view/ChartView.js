// @flow

import _ from 'lodash';
import type { Element } from 'react';
import React from 'react';
import { connect } from 'react-redux';
import type { Dispatch } from 'oo-redux-utils';
import classNames from 'classnames';
import type { ChartAreaPageStateNamespace } from '../../model/state/namespace/ChartAreaPageStateNamespace';
import type { AppState } from '../../../../../store/AppState';
import ChartControllerFactory from '../controller/ChartControllerFactory';
import { menu, scrollableChart, scrollbar, selectedChart } from './ChartView.module.scss';
import type { Chart } from '../model/state/Chart';
import ChartMenuView from '../menu/view/ChartMenuView';
import ChartConfigHintsView from './confighints/ChartConfigHintsView';
import ChartScrollbarView from '../scrollbar/view/ChartScrollbarView';
import DrillUpIconView from '../drillupicon/view/DrillUpIconView';

type OwnProps = $Exact<{
  chart: Chart,
  height: number,
  isSelectedChart: boolean,
  pageStateNamespace: ChartAreaPageStateNamespace,
  width: number
}>;

const mapAppStateToComponentProps = () => ({});
const createController = (dispatch: Dispatch, { pageStateNamespace }: OwnProps) =>
  new ChartControllerFactory(dispatch, pageStateNamespace).createController();

type MappedState = $Call<typeof mapAppStateToComponentProps, AppState>;
type Controller = $Call<typeof createController, Dispatch, OwnProps>;
type Props = $Exact<{ ...OwnProps, ...MappedState, ...Controller }>;

const ChartView = ({ chart, height, selectChart, pageStateNamespace, width }: Props): Element<any> => {
  const className = classNames(scrollableChart, { [selectedChart]: selectedChart === chart });
  const chartView = chart.createChartView(width, height, pageStateNamespace);

  return (
    <div className={className} onClick={() => selectChart(chart)}>
      {chartView}
      <ChartMenuView chart={chart} className={menu} pageStateNamespace={pageStateNamespace} />
      <ChartScrollbarView chart={chart} className={scrollbar} pageStateNamespace={pageStateNamespace} />
      <DrillUpIconView chart={chart} pageStateNamespace={pageStateNamespace} />
      <ChartConfigHintsView chart={chart} />
    </div>
  );
};

export default connect<Props, OwnProps, _, _, _, _>(
  mapAppStateToComponentProps,
  createController
)(ChartView);
