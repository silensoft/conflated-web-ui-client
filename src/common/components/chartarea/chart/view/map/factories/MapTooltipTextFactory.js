// @flow

import React from 'react';
import type { Element } from 'react';
import type { SelectedMeasure } from '../../../model/state/selectedmeasure/SelectedMeasure';
import type { Chart } from '../../../model/state/Chart';
import type { SelectedDimension } from '../../../model/state/selecteddimension/SelectedDimension';

export default class MapTooltipTextFactory {
  static getRadiusTypeMeasureTooltipText = (
    radiusTypeSelectedMeasure: ?SelectedMeasure,
    chart: Chart,
    index: number
  ): Element<any> | null => {
    if (radiusTypeSelectedMeasure != null) {
      const measureValues = chart.chartData.getForSelectedMeasure(radiusTypeSelectedMeasure);

      if (measureValues.length > 0) {
        return (
          <React.Fragment>
            <br />
            {`${radiusTypeSelectedMeasure.measure.name}: ${measureValues[index]}`}
          </React.Fragment>
        );
      }
    }

    return null;
  };

  static getTooltipTypeMeasureTooltipTexts = (
    wantedColor: string,
    chart: Chart,
    index: number
  ): Array<Element<any> | null> =>
    chart.selectedMeasures
      .filter(
        ({ visualizationColor, visualizationType }: SelectedMeasure) =>
          visualizationType === 'tooltip' && visualizationColor === wantedColor
      )
      .map(
        (selectedMeasure: SelectedMeasure): Element<any> | null => {
          const measureValues = chart.chartData.getForSelectedMeasure(selectedMeasure);
          if (measureValues.length > 0) {
            return (
              <React.Fragment>
                <br />
                {`${selectedMeasure.measure.name}: ${measureValues[index]}`}
              </React.Fragment>
            );
          }
          return null;
        }
      );

  static getTooltipTypeDimensionTooltipTexts = (chart: Chart, index: number): Array<Element<any> | null> =>
    chart.selectedDimensions
      .filter(({ visualizationType }: SelectedDimension) => visualizationType === 'Tooltip')
      .map(
        (selectedDimension: SelectedDimension): Element<any> | null => {
          const dimensionValues = chart.chartData.getForSelectedDimension(selectedDimension);
          if (dimensionValues.length > 0) {
            return (
              <React.Fragment>
                <br />
                {`${selectedDimension.dimension.name}: `}
                {`${dimensionValues[index]}`}
              </React.Fragment>
            );
          }
          return null;
        }
      );
}
