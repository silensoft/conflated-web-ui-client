// @flow

import selectorWithDefaultActionsStateNamespaces from '../../../../selectorwithdefaultactions/model/state/namespace/SelectorWithDefaultActionsStateNamespace';

const selectorStateNamespaces = {
  ...selectorWithDefaultActionsStateNamespaces,
  layoutSelector: 'layoutSelector',
  chartTypeSelector: 'chartTypeSelector',
  dataExplorerPageDataPointsCountSelector: 'dataExplorerPageDataPointsCountSelector',
  dashboardsPageDataPointsCountSelector: 'dashboardsPageDataPointsCountSelector'
};

export type SelectorStateNamespace = $Keys<typeof selectorStateNamespaces>;

export default selectorStateNamespaces;
