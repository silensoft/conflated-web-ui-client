// @flow

export type SelectorState = $Exact<{
  +isSelectorOpen: boolean
}>;
