// @flow

import React, { useCallback } from 'react';
import type { Element } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import type { Dispatch } from 'oo-redux-utils';
import { Accordion, Icon } from 'semantic-ui-react';
import { maximizedSelector, title, titleSpan } from './SelectorView.module.scss';
import type { AppState } from '../../../../store/AppState';
import SelectorControllerFactory from '../controller/SelectorControllerFactory';
import type { SelectorStateNamespace } from '../model/state/namespace/SelectorStateNamespace';

type OwnProps = $Exact<{
  additionalContent?: Element<any> | void,
  id: string,
  isSelectorMaximized?: boolean,
  selectorContentClassName?: string,
  selectorContent: Element<any>,
  selectorStateNamespace: SelectorStateNamespace,
  titleText: string,
  titleContent?: Element<any> | void
}>;

const mapAppStateToComponentProps = (appState: AppState, { selectorStateNamespace }: OwnProps) =>
  appState.common.selectorStates[selectorStateNamespace];

const createController = (dispatch: Dispatch, { selectorStateNamespace }: OwnProps) =>
  new SelectorControllerFactory(dispatch, selectorStateNamespace).createController();

type MappedState = $Call<typeof mapAppStateToComponentProps, AppState, OwnProps>;
type Controller = $Call<typeof createController, Dispatch, OwnProps>;
type Props = $Exact<{ ...OwnProps, ...MappedState, ...Controller }>;

const SelectorView = ({
  additionalContent,
  id,
  isSelectorOpen,
  isSelectorMaximized,
  selectorContent,
  selectorContentClassName,
  titleContent,
  titleText,
  toggleSelectorOpen
}: Props): Element<any> => {
  const handleAccordionTitleClick = useCallback(() => {
    toggleSelectorOpen();
  }, []);

  return (
    <section id={id}>
      <Accordion className={isSelectorMaximized && isSelectorOpen ? maximizedSelector : ''}>
        <Accordion.Title
          className={title}
          active={isSelectorOpen}
          index={0}
          onClick={handleAccordionTitleClick}
        >
          <Icon name="dropdown" />
          <span className={titleSpan}>{titleText}</span>
          {titleContent}
        </Accordion.Title>
        <Accordion.Content className={selectorContentClassName} active={isSelectorOpen}>
          <div id={`${id}Content`}>{selectorContent}</div>
        </Accordion.Content>
      </Accordion>
      {additionalContent}
    </section>
  );
};

export default connect<Props, OwnProps, _, _, _, _>(
  mapAppStateToComponentProps,
  createController
)(SelectorView);
