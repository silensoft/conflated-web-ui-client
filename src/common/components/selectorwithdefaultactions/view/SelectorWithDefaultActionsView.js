// @flow

import type { Element } from 'react';
import React, { useCallback } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import type { Dispatch } from 'oo-redux-utils';
import { content, searchInput } from './SelectorWithDefaultActionsView.module.scss';
import { actionIcon } from '../../selector/view/SelectorView.module.scss';
import SearchInputView from '../../../view/searchinput/SearchInputView';
import TitleDefaultActionsView from './titledefaultactions/TitleDefaultActionsView';
import type { AppState } from '../../../../store/AppState';
import SelectorWithDefaultActionsControllerFactory from '../controller/SelectorWithDefaultActionsControllerFactory';
import SelectorView from '../../selector/view/SelectorView';
import type { SelectorWithDefaultActionsStateNamespace } from '../model/state/namespace/SelectorWithDefaultActionsStateNamespace';

type OwnProps = $Exact<{
  additionalContent?: Element<any> | void,
  handleMaximizeIconClick: (event: SyntheticEvent<HTMLElement>) => void,
  handlePinIconClick?: ?(event: SyntheticEvent<HTMLElement>) => void,
  id: string,
  isPinned?: ?boolean,
  listItemsContent: Element<any> | Array<Element<any>> | void,
  selectedListItemsContent?: Element<any> | void,
  selectorStateNamespace: SelectorWithDefaultActionsStateNamespace,
  titleText: string
}>;

const mapAppStateToComponentProps = (appState: AppState, { selectorStateNamespace }: OwnProps) => ({
  ...appState.common.selectorWithDefaultActionsStates[selectorStateNamespace]
});

const createController = (dispatch: Dispatch, { selectorStateNamespace }: OwnProps) =>
  new SelectorWithDefaultActionsControllerFactory(dispatch, selectorStateNamespace).createController();

type MappedState = $Call<typeof mapAppStateToComponentProps, AppState, OwnProps>;
type Controller = $Call<typeof createController, Dispatch, OwnProps>;
type Props = $Exact<{ ...OwnProps, ...MappedState, ...Controller }>;

const SelectorWithDefaultActionsView = ({
  additionalContent,
  changeSelectorSearchedValue,
  handleMaximizeIconClick,
  handlePinIconClick,
  id,
  isPinned,
  isSearchInputShown,
  isSelectorMaximized,
  listItemsContent,
  selectedListItemsContent,
  selectorStateNamespace,
  titleText,
  toggleShowSearchInput
}: Props): Element<any> => {
  const handleSearchIconClick = useCallback((event: SyntheticEvent<HTMLElement>) => {
    event.stopPropagation();
    toggleShowSearchInput();
  }, []);

  const scrollSelectedListItemsIntoView = useCallback(() => {
    const selectedItemsDiv = document.getElementById(`${id}SelectedItems`);
    if (selectedItemsDiv != null) {
      selectedItemsDiv.scrollTop = 0;
    }
  }, []);

  const titleContent = (
    <TitleDefaultActionsView
      iconClassName={actionIcon}
      toggleShowSearchInput={handleSearchIconClick}
      toggleMaximizeAccordion={handleMaximizeIconClick}
      shouldShowPinIcon={isPinned !== undefined}
      isPinned={isPinned}
      togglePinPane={handlePinIconClick}
    />
  );

  const selectorContent = (
    <React.Fragment>
      <div id={`${id}SelectedItems`}>{selectedListItemsContent}</div>
      <SearchInputView
        className={searchInput}
        isShown={isSearchInputShown}
        onChange={(searchedValue: string) => changeSelectorSearchedValue(searchedValue)}
      />
      <div onClick={scrollSelectedListItemsIntoView}>{listItemsContent}</div>
    </React.Fragment>
  );

  return (
    <SelectorView
      id={id}
      isSelectorMaximized={isSelectorMaximized}
      titleText={titleText}
      titleContent={titleContent}
      selectorContent={selectorContent}
      selectorContentClassName={content}
      additionalContent={additionalContent}
      selectorStateNamespace={selectorStateNamespace}
    />
  );
};

export default connect<Props, OwnProps, _, _, _, _>(
  mapAppStateToComponentProps,
  createController
)(SelectorWithDefaultActionsView);
