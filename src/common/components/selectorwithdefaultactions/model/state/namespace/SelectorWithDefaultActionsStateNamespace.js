// @flow

const selectorWithDefaultActionsStateNamespaces = {
  dashboardGroupSelector: 'dashboardGroupSelector',
  dashboardSelector: 'dashboardSelector',
  dataSourceSelector: 'dataSourceSelector',
  measureSelector: 'measureSelector',
  dimensionSelector: 'dimensionSelector',
  dashboardsPageFilterSelector: 'dashboardsPageFilterSelector',
  dashboardsPageSortBySelector: 'dashboardsPageSortBySelector',
  dataExplorerPageFilterSelector: 'dataExplorerPageFilterSelector',
  dataExplorerPageSortBySelector: 'dataExplorerPageSortBySelector',
  alertsPageTriggerDataSourceSelector: 'alertsPageTriggerDataSourceSelector',
  alertsPageTriggerGroupSelector: 'alertsPageTriggerGroupSelector',
  alertsPageTriggerSelector: 'alertsPageTriggerSelector',
  goalsPageTriggerDataSourceSelector: 'goalsPageTriggerDataSourceSelector',
  goalsPageTriggerGroupSelector: 'goalsPageTriggerGroupSelector',
  goalsPageTriggerSelector: 'goalsPageTriggerSelector'
};

// eslint-disable-next-line flowtype/generic-spacing
export type SelectorWithDefaultActionsStateNamespace = $Keys<
  typeof selectorWithDefaultActionsStateNamespaces
>;

export default selectorWithDefaultActionsStateNamespaces;
