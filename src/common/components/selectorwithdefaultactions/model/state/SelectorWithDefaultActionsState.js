// @flow

export type SelectorWithDefaultActionsState = $Exact<{
  +isSelectorMaximized: boolean,
  +isSearchInputShown: boolean,
  +searchedValue: string
}>;
