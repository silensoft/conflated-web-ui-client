import React from 'react';
import { shallow as renderShallow } from 'enzyme/build';
import SortBySelector from './SortBySelectorView';

test('SortBySelector should render correctly', () => {
  const renderedSortBySelector = renderShallow(<SortBySelector />);
  expect(renderedSortBySelector).toMatchSnapshot();
});
