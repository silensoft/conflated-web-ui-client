// @flow

import type { TimeSortOption } from '../../../chartarea/chart/model/state/selectedsortbys/selectedsortby/types/TimeSortOption';
import type { SortDirection } from '../../../chartarea/chart/model/state/selectedsortbys/selectedsortby/types/SortDirection';

export type SortBySelectorState = $Exact<{
  +timeSortOptions: TimeSortOption[],
  +lastUsedSortDirection: SortDirection,
  +areSelectedSortBysShown: boolean
}>;
