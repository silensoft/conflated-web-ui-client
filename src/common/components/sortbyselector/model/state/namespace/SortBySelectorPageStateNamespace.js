// @flow

export type SortBySelectorPageStateNamespace = 'dataExplorerPage' | 'dashboardsPage';
