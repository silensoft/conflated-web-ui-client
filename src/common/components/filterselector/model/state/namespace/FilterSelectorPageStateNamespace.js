// @flow

export type FilterSelectorPageStateNamespace = 'dataExplorerPage' | 'dashboardsPage';
