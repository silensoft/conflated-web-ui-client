// @flow

import type { Element } from 'react';
import React, { useCallback } from 'react';
import SliderView from '../../../../../../view/slider/SliderView';
import type { ChartData } from '../../../../../chartarea/chart/model/state/chartdata/ChartData';
import type { SelectedFilter } from '../../../../../chartarea/chart/model/state/selectedfilters/selectedfilter/SelectedFilter';
import NumberRangesParser from '../../../../../chartarea/chart/model/state/selectedfilters/selectedfilter/numberrange/NumberRangesParser';

type Props = $Exact<{
  changeFilterExpression: string => void,
  chartData: ChartData,
  className: string,
  selectedFilter: SelectedFilter
}>;

const SliderFilterInputView = ({
  changeFilterExpression,
  chartData,
  className,
  selectedFilter
}: Props): Element<any> => {
  const changeSliderValues = useCallback((newSelectedMinValue: number, newSelectedMaxValue: number) => {
    const newFilterExpression = `${newSelectedMinValue}-${newSelectedMaxValue}`;
    changeFilterExpression(newFilterExpression);
  });

  const [selectedMinValue, selectedMaxValue] = NumberRangesParser.parseNumberRange(
    selectedFilter.filterExpression
  );

  const [sliderMinValue, sliderMaxValue] = chartData.getMinAndMaxValueForSelectedFilter(selectedFilter);

  return (
    <SliderView
      className={className}
      sliderMinValue={sliderMinValue}
      sliderMaxValue={sliderMaxValue}
      selectedMinValue={selectedMinValue}
      selectedMaxValue={selectedMaxValue}
      onSliderValuesChange={changeSliderValues}
    />
  );
};

export default SliderFilterInputView;
