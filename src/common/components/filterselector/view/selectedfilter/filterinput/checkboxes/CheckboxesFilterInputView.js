// @flow

import _ from 'lodash';
import type { Element } from 'react';
import React, { useCallback } from 'react';
import { Checkbox } from 'semantic-ui-react';
import { checkboxesSelector } from './CheckboxesFilterInputView.module.scss';
import type { ChartData } from '../../../../../chartarea/chart/model/state/chartdata/ChartData';
import type { SelectedFilter } from '../../../../../chartarea/chart/model/state/selectedfilters/selectedfilter/SelectedFilter';

type Props = $Exact<{
  chartData: ChartData,
  className: string,
  selectedFilter: SelectedFilter,
  changeFilterExpression: string => void
}>;

const CheckboxesFilterInputView = ({
  chartData,
  className,
  selectedFilter,
  selectedFilter: { filterExpression },
  changeFilterExpression
}: Props): Element<any> => {
  const changeCheckboxState = useCallback(
    (event: SyntheticEvent<HTMLElement>, { label: filterItem, checked }: Object) => {
      let selectedFilterItems = filterExpression ? JSON.parse(filterExpression) : [];

      if (checked && !selectedFilterItems.includes(filterItem)) {
        selectedFilterItems.push(filterItem);
      } else if (!checked && selectedFilterItems.includes(filterItem)) {
        selectedFilterItems = _.without(selectedFilterItems, filterItem);
      }

      changeFilterExpression(JSON.stringify(selectedFilterItems));
    },
    [filterExpression]
  );

  const checkboxItems = chartData.getForSelectedFilter(selectedFilter);
  const checkedItems = filterExpression ? JSON.parse(filterExpression) : [];

  const checkboxes = checkboxItems.map((item: any) => (
    <Checkbox
      key={item}
      label={item}
      checked={checkedItems.includes(item)}
      style={{ display: 'block' }}
      onChange={changeCheckboxState}
    />
  ));

  return <div className={`${className} ${checkboxesSelector} small-checkboxes`}>{checkboxes}</div>;
};

export default CheckboxesFilterInputView;
