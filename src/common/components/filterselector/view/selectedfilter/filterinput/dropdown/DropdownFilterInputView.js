// @flow

import type { Element } from 'react';
import React from 'react';
import { Dropdown } from 'semantic-ui-react';
import type { ChartData } from '../../../../../chartarea/chart/model/state/chartdata/ChartData';
import type { SelectedFilter } from '../../../../../chartarea/chart/model/state/selectedfilters/selectedfilter/SelectedFilter';

type Props = $Exact<{
  changeFilterExpression: string => void,
  chartData: ChartData,
  className: string,
  selectedFilter: SelectedFilter
}>;

const DropdownFilterInputView = ({
  changeFilterExpression,
  chartData,
  className,
  selectedFilter
}: Props): Element<any> => {
  const dropdownItems = chartData.getForSelectedFilter(selectedFilter);

  const dropdownOptions = dropdownItems.map((value: any) => ({
    key: value,
    text: value,
    value
  }));

  return (
    <Dropdown
      className={className}
      placeholder="Enter filter value..."
      fluid
      multiple
      selection
      search
      options={dropdownOptions}
      value={selectedFilter.filterExpression ? JSON.parse(selectedFilter.filterExpression) : []}
      onChange={(event: SyntheticEvent<HTMLElement>, { value }: Object) =>
        changeFilterExpression(JSON.stringify(value))
      }
    />
  );
};

export default DropdownFilterInputView;
