// @flow

import type { Element } from 'react';
import React, { useCallback } from 'react';
import { DateTimeInput } from 'semantic-ui-calendar-react';
import {
  dateTimeInput,
  label,
  labelledDateTimeInput,
  timestampRangeSelector
} from './TimestampRangeFilterInputView.module.scss';

type Props = $Exact<{
  changeFilterExpression: string => void,
  className: string,
  filterExpression: string
}>;

const TimestampRangeFilterInputView = ({
  changeFilterExpression,
  className,
  filterExpression
}: Props): Element<any> => {
  const timestamps = filterExpression.split(';');

  const changeStartTimestamp = useCallback(
    (event: SyntheticEvent<HTMLElement>, { value: startTimestamp }: Object) => {
      if (timestamps.length === 2) {
        changeFilterExpression(`${startTimestamp};${timestamps[1]}`);
      } else {
        changeFilterExpression(`${startTimestamp};`);
      }
    },
    [filterExpression]
  );

  const changeEndTimestamp = useCallback(
    (event: SyntheticEvent<HTMLElement>, { value: endTimestamp }: Object) => {
      if (timestamps.length === 2) {
        changeFilterExpression(`${timestamps[0]};${endTimestamp}`);
      } else {
        changeFilterExpression(`;${endTimestamp}`);
      }
    },
    [filterExpression]
  );

  const fromTimestamp = timestamps.length > 0 ? timestamps[0] : '';
  const toTimestamp = timestamps.length === 2 ? timestamps[1] : '';

  return (
    <div className={`${className} ${timestampRangeSelector}`}>
      <div className={labelledDateTimeInput}>
        <span className={label}>From</span>
        <div className={dateTimeInput}>
          <DateTimeInput
            localization="fi"
            closable
            name="from"
            value={fromTimestamp}
            iconPosition="left"
            popupPosition="bottom left"
            onChange={changeStartTimestamp}
          />
        </div>
      </div>
      <div className={labelledDateTimeInput}>
        <span className={label}>To</span>
        <div className={dateTimeInput}>
          <DateTimeInput
            localization="fi"
            closable
            name="to"
            value={toTimestamp}
            iconPosition="left"
            popupPosition="bottom left"
            onChange={changeEndTimestamp}
          />
        </div>
      </div>
    </div>
  );
};

export default TimestampRangeFilterInputView;
