// @flow

import type { Element } from 'react';
import React from 'react';
import { Input } from 'semantic-ui-react';

type Props = $Exact<{
  changeFilterExpression: string => void,
  className: string,
  filterExpression: string,
  isSelectionFilter: ?boolean
}>;

const InputFilterInputView = ({
  changeFilterExpression,
  className,
  filterExpression,
  isSelectionFilter
}: Props): Element<any> => (
  <Input
    className={className}
    disabled={isSelectionFilter}
    placeholder="Enter filter, e.g. 1, 5, 10-15"
    onChange={({ currentTarget: { value } }: SyntheticInputEvent<HTMLInputElement>) =>
      changeFilterExpression(value)
    }
    value={filterExpression}
  />
);

export default InputFilterInputView;
