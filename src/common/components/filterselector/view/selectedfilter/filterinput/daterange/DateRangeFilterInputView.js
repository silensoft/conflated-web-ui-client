// @flow

import type { Element } from 'react';
import React from 'react';
import { DatesRangeInput } from 'semantic-ui-calendar-react';

type Props = $Exact<{
  className: string,
  filterExpression: string,
  changeFilterExpression: string => void
}>;

const DateRangeFilterInputView = ({
  className,
  filterExpression,
  changeFilterExpression
}: Props): Element<any> => (
  <div className={`${className} dates-range-input`}>
    <DatesRangeInput
      localization="fi"
      closable
      name="datesRange"
      placeholder="From date - to date"
      value={filterExpression}
      iconPosition="left"
      popupPosition="bottom left"
      onChange={(event: SyntheticEvent<HTMLElement>, { value: datesRange }: Object) =>
        changeFilterExpression(datesRange)
      }
    />
  </div>
);

export default DateRangeFilterInputView;
