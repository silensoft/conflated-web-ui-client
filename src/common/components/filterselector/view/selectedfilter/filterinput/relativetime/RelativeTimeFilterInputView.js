// @flow

import _ from 'lodash';
import type { Element } from 'react';
import React, { useCallback } from 'react';
import { Dropdown, Input } from 'semantic-ui-react';
import {
  relativeTimeSelector,
  relativeTimeUnitDropdown,
  relativeTimeValueInput
} from './RelativeTimeFilterInputView.module.scss';

type Props = $Exact<{
  changeFilterExpression: string => void,
  filterExpression: string
}>;

const RelativeTimeFilterInputView = ({ changeFilterExpression, filterExpression }: Props): Element<any> => {
  const changeRelativeTimeValue = useCallback(
    ({ currentTarget: { value: relativeTimeValueStr } }: SyntheticEvent<HTMLInputElement>) => {
      const relativeTimeUnit = filterExpression.split(' ')[1];
      const relativeTimeValue = parseInt(relativeTimeValueStr, 10);

      if (relativeTimeValueStr === '' || _.isFinite(relativeTimeValue)) {
        changeFilterExpression(
          `${relativeTimeValueStr ? relativeTimeValue : relativeTimeValueStr} ${relativeTimeUnit}`
        );
      }
    },
    [filterExpression]
  );

  const changeRelativeTimeUnit = useCallback(
    (event: SyntheticEvent<HTMLElement>, { value: relativeTimeUnit }: Object) => {
      const relativeTimeValueStr = filterExpression.split(' ')[0];
      changeFilterExpression(`${relativeTimeValueStr} ${relativeTimeUnit}`);
    },
    [filterExpression]
  );

  const relativeTimeUnitOptions = [
    {
      key: 'Seconds',
      text: 'Seconds',
      value: 'Seconds'
    },
    {
      key: 'Minute',
      text: 'Minutes',
      value: 'Minutes'
    },
    {
      key: 'Hours',
      text: 'Hours',
      value: 'Hours'
    },
    {
      key: 'Days',
      text: 'Days',
      value: 'Days'
    },
    {
      key: 'Months',
      text: 'Months',
      value: 'Months'
    },
    {
      key: 'Years',
      text: 'Years',
      value: 'Years'
    }
  ];

  return (
    <div className={relativeTimeSelector}>
      <span>Last</span>
      <Input
        className={relativeTimeValueInput}
        value={filterExpression.split(' ')[0]}
        maxLength={2}
        onChange={changeRelativeTimeValue}
      />
      <Dropdown
        className={relativeTimeUnitDropdown}
        text={filterExpression.split(' ')[1]}
        inline
        options={relativeTimeUnitOptions}
        onChange={changeRelativeTimeUnit}
      />
    </div>
  );
};

export default RelativeTimeFilterInputView;
