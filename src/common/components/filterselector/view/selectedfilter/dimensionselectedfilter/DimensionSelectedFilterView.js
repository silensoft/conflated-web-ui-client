// @flow

import type { Element } from 'react';
import React from 'react';
import { Dropdown, Icon, List } from 'semantic-ui-react';
import { filterInput, icon, listItem, measureOrDimensionName } from '../SelectedFilterView.module.scss';
import type { FilterInputType } from '../../../../chartarea/chart/model/state/selectedfilters/selectedfilter/types/FilterInputType';
import type { DataScopeType } from '../../../../../model/state/types/DataScopeType';
import DataScopePickerView from '../../../../../view/datascopepicker/DataScopePickerView';
import type { ChartData } from '../../../../chartarea/chart/model/state/chartdata/ChartData';
import type { SelectedFilter } from '../../../../chartarea/chart/model/state/selectedfilters/selectedfilter/SelectedFilter';

type Props = $Exact<{
  changeSelectedFilterExpression: (expression: string) => void,
  changeSelectedFilterInputType: (filterInputType: FilterInputType) => void,
  changeSelectedFilterDataScopeType: (dataScopeType: DataScopeType) => void,
  chartData: ChartData,
  removeSelectedFilter: () => void,
  selectedFilter: SelectedFilter
}>;

// noinspection FunctionWithMoreThanThreeNegationsJS
const DimensionSelectedFilterView = ({
  changeSelectedFilterDataScopeType,
  changeSelectedFilterExpression,
  changeSelectedFilterInputType,
  chartData,
  removeSelectedFilter,
  selectedFilter
}: Props): Element<any> => {
  let dimensionFilterInputTypeDropdownItems;
  let filterRemoveIcon;

  if (!selectedFilter.isDrillDownFilter && !selectedFilter.isSelectionFilter) {
    dimensionFilterInputTypeDropdownItems = selectedFilter.allowedDimensionFilterInputTypes.map(
      (filterInputType: FilterInputType) => (
        <Dropdown.Item
          text={filterInputType}
          value={filterInputType}
          onClick={() => changeSelectedFilterInputType(filterInputType)}
        />
      )
    );

    filterRemoveIcon = <Icon className={icon} name="close" onClick={() => removeSelectedFilter()} />;
  }

  return (
    <List.Item className={listItem} key={selectedFilter.measureOrDimension.name}>
      <div className={measureOrDimensionName}>{selectedFilter.measureOrDimension.name}</div>
      <DataScopePickerView
        changeDataScopeType={changeSelectedFilterDataScopeType}
        selectedDataScopeType={selectedFilter.dataScopeType}
      />
      <Dropdown className={icon} icon="setting">
        <Dropdown.Menu direction="left">{dimensionFilterInputTypeDropdownItems}</Dropdown.Menu>
      </Dropdown>
      {filterRemoveIcon}
      {selectedFilter.getFilterInputView(filterInput, chartData, changeSelectedFilterExpression)}
    </List.Item>
  );
};

export default DimensionSelectedFilterView;
