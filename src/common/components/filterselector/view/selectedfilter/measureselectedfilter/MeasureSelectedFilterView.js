// @flow

import type { Element } from 'react';
import React from 'react';
import { Dropdown, Icon, List } from 'semantic-ui-react';
import { filterInput, icon, listItem, measureOrDimensionName } from '../SelectedFilterView.module.scss';
import type { AggregationFunction } from '../../../../chartarea/chart/model/state/selectedmeasure/types/AggregationFunction';
import type { FilterInputType } from '../../../../chartarea/chart/model/state/selectedfilters/selectedfilter/types/FilterInputType';
import type { DataScopeType } from '../../../../../model/state/types/DataScopeType';
import AggregationFunctionPickerView from '../../../../../view/aggregationfunctionpicker/AggregationFunctionPickerView';
import type { Chart } from '../../../../chartarea/chart/model/state/Chart';
import DataScopePickerView from '../../../../../view/datascopepicker/DataScopePickerView';
import type { SelectedFilter } from '../../../../chartarea/chart/model/state/selectedfilters/selectedfilter/SelectedFilter';

type Props = $Exact<{
  changeSelectedFilterAggregationFunction: (aggregationFunction: AggregationFunction) => void,
  changeSelectedFilterExpression: (expression: string) => void,
  changeSelectedFilterInputType: (filterInputType: FilterInputType) => void,
  changeSelectedFilterDataScopeType: (dataScopeType: DataScopeType) => void,
  chart: Chart,
  removeSelectedFilter: () => void,
  selectedFilter: SelectedFilter
}>;

const MeasureSelectedFilterView = ({
  changeSelectedFilterAggregationFunction,
  changeSelectedFilterExpression,
  changeSelectedFilterInputType,
  changeSelectedFilterDataScopeType,
  chart,
  removeSelectedFilter,
  selectedFilter
}: Props): Element<any> => (
  <List.Item className={listItem} key={selectedFilter.measureOrDimension.name}>
    <AggregationFunctionPickerView
      aggregationFunctions={chart.getSupportedAggregationFunctions()}
      changeAggregationFunction={changeSelectedFilterAggregationFunction}
      selectedAggregationFunction={selectedFilter.aggregationFunction}
    />
    <div className={measureOrDimensionName}>{selectedFilter.measureOrDimension.name}</div>
    <DataScopePickerView
      changeDataScopeType={changeSelectedFilterDataScopeType}
      selectedDataScopeType={selectedFilter.dataScopeType}
    />
    <Dropdown className={icon} icon="setting">
      <Dropdown.Menu direction="left">
        <Dropdown.Item
          text="Input filter"
          value="Input filter"
          onClick={() => changeSelectedFilterInputType('Input filter')}
        />
        <Dropdown.Item
          text="Range filter"
          value="Range filter"
          onClick={() => changeSelectedFilterInputType('Range filter')}
        />
      </Dropdown.Menu>
    </Dropdown>
    <Icon className={icon} name="close" onClick={() => removeSelectedFilter()} />
    {selectedFilter.getFilterInputView(filterInput, chart.chartData, changeSelectedFilterExpression)}
  </List.Item>
);

export default MeasureSelectedFilterView;
