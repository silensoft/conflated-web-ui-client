import React from 'react';
import { shallow as renderShallow } from 'enzyme/build';
import FilterSelector from './FilterSelectorView';

test('FilterSelector should render correctly', () => {
  const renderedFilterSelector = renderShallow(<FilterSelector />);
  expect(renderedFilterSelector).toMatchSnapshot();
});
