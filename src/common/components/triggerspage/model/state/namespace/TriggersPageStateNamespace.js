// @flow

export type TriggersPageStateNamespace = 'alertsPage' | 'goalsPage';
