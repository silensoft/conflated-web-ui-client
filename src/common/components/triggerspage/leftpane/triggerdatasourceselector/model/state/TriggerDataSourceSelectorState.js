// @flow

import type { DataSource } from '../../../../../../model/state/datasource/DataSource';

export type TriggerDataSourceSelectorState = $Exact<{
  +selectedDataSources: DataSource[],
  +dataSources: DataSource[],
  +isFetchingDataSources: boolean
}>;
