// @flow

export type TriggerSelectorState = $Exact<{
  +selectedTriggers: string[]
}>;
