// @flow

import _ from 'lodash';
import type { Element } from 'react';
import React, { useCallback, useMemo } from 'react';
import { connect } from 'react-redux';
import type { Dispatch } from 'oo-redux-utils';
import OOReduxUtils from 'oo-redux-utils';
import TriggerGroupListItemView from './triggergrouplistitem/TriggerGroupListItemView';
import type { AppState } from '../../../../../../store/AppState';
import type { TriggersPageStateNamespace } from '../../../model/state/namespace/TriggersPageStateNamespace';
import SelectorWithDefaultActionsView from '../../../../selectorwithdefaultactions/view/SelectorWithDefaultActionsView';
import TriggerGroupSelectorControllerFactory from '../controller/TriggerGroupSelectorControllerFactory';
import selectorStateNamespaces from '../../../../selector/model/state/namespace/SelectorStateNamespace';
import selectorWithDefaultActionsStateNamespaces from '../../../../selectorwithdefaultactions/model/state/namespace/SelectorWithDefaultActionsStateNamespace';
import AllAndFavoritesTabView from '../../../../../view/allandfavoritestabview/AllAndFavoritesTabView';
import createTriggerGroupsSelector from '../model/state/selector/createTriggerGroupsSelector';
import type { TriggerGroup } from '../model/state/triggergroup/TriggerGroup';
import SelectorWithDefaultActionsControllerFactory from '../../../../selectorwithdefaultactions/controller/SelectorWithDefaultActionsControllerFactory';

type OwnProps = $Exact<{ pageStateNamespace: TriggersPageStateNamespace }>;

const mapAppStateToComponentProps = (appState: AppState, { pageStateNamespace }: OwnProps) =>
  OOReduxUtils.mergeOwnAndForeignState(appState[pageStateNamespace].triggerGroupSelectorState, {
    triggerGroups: createTriggerGroupsSelector(pageStateNamespace)(appState),

    isTriggerDataSourceSelectorOpen:
      appState.common.selectorStates[
        selectorStateNamespaces[`${pageStateNamespace}TriggerDataSourceSelector`]
      ].isSelectorOpen,

    isTriggerSelectorOpen:
      appState.common.selectorStates[selectorStateNamespaces[`${pageStateNamespace}TriggerSelector`]]
        .isSelectorOpen
  });

const createController = (dispatch: Dispatch, { pageStateNamespace }: OwnProps) => ({
  toggleMaximizeSelector: new SelectorWithDefaultActionsControllerFactory(
    dispatch,
    selectorWithDefaultActionsStateNamespaces[`${pageStateNamespace}TriggerGroupSelector`]
  ).createController().toggleMaximizeSelector,

  ...new TriggerGroupSelectorControllerFactory(dispatch, pageStateNamespace).createController()
});

type MappedState = $Call<typeof mapAppStateToComponentProps, AppState, OwnProps>;
type Controller = $Call<typeof createController, Dispatch, OwnProps>;
type Props = $Exact<{ ...OwnProps, ...MappedState, ...Controller }>;

const TriggerGroupSelectorView = ({
  isTriggerDataSourceSelectorOpen,
  isTriggerSelectorOpen,
  pageStateNamespace,
  selectedTriggerGroups,
  selectTriggerGroup,
  toggleMaximizeSelector,
  triggerGroups
}: Props): Element<any> => {
  const handleMaximizeIconClick = useCallback(
    (event: SyntheticEvent<HTMLElement>) => {
      event.stopPropagation();

      toggleMaximizeSelector([
        {
          isOpen: isTriggerDataSourceSelectorOpen,
          stateNamespace:
            selectorWithDefaultActionsStateNamespaces[`${pageStateNamespace}TriggerDataSourceSelector`]
        },
        {
          isOpen: isTriggerSelectorOpen,
          stateNamespace: selectorWithDefaultActionsStateNamespaces[`${pageStateNamespace}TriggerSelector`]
        }
      ]);
    },
    [isTriggerSelectorOpen, isTriggerDataSourceSelectorOpen]
  );

  const triggerGroupListItems = useMemo(
    (): Array<Element<any>> =>
      triggerGroups.map((triggerGroup: TriggerGroup) => (
        <TriggerGroupListItemView
          key={triggerGroup.name}
          triggerGroup={triggerGroup.name}
          selectedTriggerGroups={selectedTriggerGroups}
          selectTriggerGroup={(triggerGroupName: string) => selectTriggerGroup(triggerGroupName)}
          worstTriggerCount={triggerGroup.worstTriggerCount}
          intermediateTriggerCount={triggerGroup.intermediateTriggerCount}
          bestTriggerCount={triggerGroup.bestTriggerCount}
          pageStateNamespace={pageStateNamespace}
        />
      )),
    [triggerGroups, selectedTriggerGroups]
  );

  const selectorStateNamespace = `${pageStateNamespace}TriggerGroupSelector`;

  return (
    <SelectorWithDefaultActionsView
      id={selectorStateNamespace}
      titleText="ALERT GROUP"
      listItemsContent={
        <AllAndFavoritesTabView firstTabPaneListItems={triggerGroupListItems} secondTabPaneListItems={[]} />
      }
      handleMaximizeIconClick={handleMaximizeIconClick}
      selectorStateNamespace={selectorWithDefaultActionsStateNamespaces[selectorStateNamespace]}
    />
  );
};

export default connect<Props, OwnProps, _, _, _, _>(
  mapAppStateToComponentProps,
  createController
)(TriggerGroupSelectorView);
