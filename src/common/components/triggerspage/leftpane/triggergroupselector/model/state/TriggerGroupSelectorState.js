// @flow

export type TriggerGroupSelectorState = $Exact<{
  +selectedTriggerGroups: string[]
}>;
