// @flow

import AbstractPageAction from '../AbstractPageAction';
import type { PageState } from '../../state/PageState';
import type { Pane } from '../../state/types/Pane';
import type { PageStateNamespace } from '../../state/namespace/PageStateNamespace';

export default class StartPaneGutterDragAction extends AbstractPageAction {
  +pagePaneGutterPositionOnDragStart: number;

  +pane: Pane;

  constructor(stateNamespace: PageStateNamespace, pane: Pane, pagePaneGutterPositionOnDragStart: number) {
    super(stateNamespace);
    this.pane = pane;
    this.pagePaneGutterPositionOnDragStart = pagePaneGutterPositionOnDragStart;
  }

  performActionAndReturnNewState(currentState: PageState): PageState {
    return {
      ...currentState,
      pagePaneGutterPositionOnDragStart: {
        ...currentState.pagePaneGutterPositionOnDragStart,
        // $FlowFixMe
        [this.pane]: currentState.pagePaneGutterPositionOnDragStart[this.pane] === -1
          ? this.pagePaneGutterPositionOnDragStart
          : currentState.pagePaneGutterPositionOnDragStart[this.pane]
      }
    };
  }
}
