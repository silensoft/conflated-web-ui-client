// @flow

import AbstractPageAction from '../AbstractPageAction';
import type { PageState } from '../../state/PageState';
import type { Pane } from '../../state/types/Pane';
import type { PageStateNamespace } from '../../state/namespace/PageStateNamespace';

export default class PagePaneGutterDragAction extends AbstractPageAction {
  +pagePaneGutterPosition: number;

  +pane: Pane;

  constructor(stateNamespace: PageStateNamespace, pane: Pane, pagePaneGutterPosition: number) {
    super(stateNamespace);
    this.pane = pane;
    this.pagePaneGutterPosition = pagePaneGutterPosition;
  }

  performActionAndReturnNewState(currentState: PageState): PageState {
    return {
      ...currentState,
      pagePaneGutterOffset: {
        ...currentState.pagePaneGutterOffset,
        // $FlowFixMe
        [this.pane]: this.pagePaneGutterPosition - currentState.pagePaneGutterPositionOnDragStart[this.pane]
      }
    };
  }
}
