// @flow

import AbstractPageAction from '../AbstractPageAction';
import type { Pane } from '../../state/types/Pane';
import type { PageState } from '../../state/PageState';
import type { PageStateNamespace } from '../../state/namespace/PageStateNamespace';

export default class HidePagePaneAction extends AbstractPageAction {
  +pane: Pane;

  constructor(stateNamespace: PageStateNamespace, pane: Pane) {
    super(stateNamespace);
    this.pane = pane;
  }

  performActionAndReturnNewState(currentState: PageState): PageState {
    return {
      ...currentState,
      shouldShowPagePane: {
        ...currentState.shouldShowPagePane,
        // $FlowFixMe
        [this.pane]: false
      }
    };
  }
}
