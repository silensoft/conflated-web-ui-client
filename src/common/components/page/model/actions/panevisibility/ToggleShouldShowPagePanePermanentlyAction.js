// @flow

import type { PageState } from '../../state/PageState';
import type { Pane } from '../../state/types/Pane';
import AbstractPageAction from '../AbstractPageAction';
import type { PageStateNamespace } from '../../state/namespace/PageStateNamespace';

export default class ToggleShouldShowPagePanePermanentlyAction extends AbstractPageAction {
  +pane: Pane;

  constructor(stateNamespace: PageStateNamespace, pane: Pane) {
    super(stateNamespace);
    this.pane = pane;
  }

  performActionAndReturnNewState(currentState: PageState): PageState {
    return {
      ...currentState,
      shouldShowPagePanePermanently: {
        ...currentState.shouldShowPagePanePermanently,
        // $FlowFixMe
        [this.pane]: currentState.shouldShowPagePanePermanently[this.pane]
      }
    };
  }
}
