// @flow

import type { Pane } from './types/Pane';

type PaneStates = { [Pane]: boolean };
type PaneValues = { [Pane]: number };

export type PageState = $Exact<{
  +shouldShowPagePaneActivatorHint: PaneStates,
  +shouldShowPagePane: PaneStates,
  +shouldShowPagePanePermanently: PaneStates,
  +pagePaneGutterOffset: PaneValues,
  +pagePaneGutterPositionOnDragStart: PaneValues
}>;
