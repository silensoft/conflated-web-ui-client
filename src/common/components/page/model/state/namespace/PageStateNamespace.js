// @flow

const pageStateNamespaces = {
  dashboardsPage: 'dashboardsPage',
  dataExplorerPage: 'dataExplorerPage',
  alertsPage: 'alertsPage',
  goalsPage: 'goalsPage'
};

export type PageStateNamespace = $Keys<typeof pageStateNamespaces>;

export default pageStateNamespaces;
