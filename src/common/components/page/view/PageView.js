// @flow

import _ from 'lodash';
import type { Element } from 'react';
import React, { useCallback, useEffect } from 'react';
import { connect } from 'react-redux';
import type { Dispatch } from 'oo-redux-utils';
import classNames from 'classnames';
import {
  draggableGutter,
  leftPaneActivator,
  leftPaneActivatorHint,
  page,
  rightPaneActivator,
  rightPaneActivatorHint,
  visible
} from './PageView.module.scss';
import type { AppState } from '../../../../store/AppState';
import PageControllerFactory from '../controller/PageControllerFactory';
import type { Pane } from '../model/state/types/Pane';
import type { PageStateNamespace } from '../model/state/namespace/PageStateNamespace';

type OwnProps = $Exact<{
  className?: string,
  header?: Element<any>,
  leftPane: Element<any>,
  middlePane: Element<any>,
  pageStateNamespace: PageStateNamespace,
  rightPane?: Element<any>,
  showPaneActivatorHintsOnComponentMount?: boolean
}>;

const mapAppStateToComponentProps = (appState: AppState, { pageStateNamespace }: OwnProps) =>
  appState.common.pageStates[pageStateNamespace];

const createController = (dispatch: Dispatch, { pageStateNamespace }: OwnProps) =>
  new PageControllerFactory(dispatch, pageStateNamespace).createController();

type MappedState = $Call<typeof mapAppStateToComponentProps, AppState, OwnProps>;
type Controller = $Call<typeof createController, Dispatch, OwnProps>;
type Props = $Exact<{ ...OwnProps, ...MappedState, ...Controller }>;

const PageView = ({
  className,
  dragPagePaneGutter,
  flashBrieflyPaneActivatorHints,
  header,
  leftPane,
  middlePane,
  rightPane,
  shouldShowPagePane,
  shouldShowPagePaneActivatorHint,
  shouldShowPagePanePermanently,
  showPane,
  showPaneActivatorHintsOnComponentMount,
  startPaneGutterDrag
}: Props): Element<any> => {
  useEffect(() => {
    if (showPaneActivatorHintsOnComponentMount) {
      flashBrieflyPaneActivatorHints();
    }
  }, []);

  const handlePaneGutterDragStart = useCallback((event: SyntheticDragEvent<HTMLDivElement>, pane: Pane) => {
    event.persist();
    startPaneGutterDrag(pane, event.pageX);
  }, []);

  const handlePaneGutterDrag = _.throttle(useCallback((event: SyntheticDragEvent<HTMLDivElement>, pane: Pane) => {
    event.persist();
    if (event.pageX !== null) {
      dragPagePaneGutter(pane, event.pageX);
    }
  }, []), 25);

  const getDraggableGutter = useCallback(
    (pane: Pane): Element<any> | void => {
      if (shouldShowPagePanePermanently[pane]) {
        return (
          <div
            className={draggableGutter}
            draggable
            onDragStart={(event: SyntheticDragEvent<HTMLDivElement>) =>
              handlePaneGutterDragStart(event, pane)
            }
            onDrag={(event: SyntheticDragEvent<HTMLDivElement>) => handlePaneGutterDrag(event, pane)}
          />
        );
      }

      return undefined;
    },
    [shouldShowPagePanePermanently]
  );

  const getPaneActivator = useCallback(
    (pane: Pane): Element<any> | void => {
      if (shouldShowPagePane[pane] || shouldShowPagePanePermanently[pane]) {
        return undefined;
      } else {
        return (
          <div
            className={pane === 'leftPane' ? leftPaneActivator : rightPaneActivator}
            onMouseOver={() => showPane(pane)}
            onFocus={() => showPane(pane)}
          />
        );
      }
    },
    [shouldShowPagePane, shouldShowPagePanePermanently]
  );

  const leftPaneActivatorClassName = classNames(leftPaneActivatorHint, {
    [visible]: shouldShowPagePaneActivatorHint.leftPane
  });

  const rightPaneActivatorHintClassName = classNames(rightPaneActivatorHint, {
    [visible]: shouldShowPagePaneActivatorHint.rightPane
  });

  return (
    <div className={`${page} ${className ?? ''}`}>
      {header}
      {leftPane}
      {getDraggableGutter('leftPane')}
      {middlePane}
      {getDraggableGutter('rightPane')}
      {rightPane}
      {getPaneActivator('leftPane')}
      {getPaneActivator('rightPane')}
      <div className={leftPaneActivatorClassName} />
      <div className={rightPaneActivatorHintClassName} />
    </div>
  );
};

PageView.defaultProps = {
  className: ''
};

export default connect<Props, OwnProps, _, _, _, _>(
  mapAppStateToComponentProps,
  createController
)(PageView);
